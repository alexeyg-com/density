import lib.simulator

# Some code to export initiation rates for Eftychia
def write_output_file(result, comment = None, output_filename = 'eftychia.txt') :
    fout = open(output_filename, 'wb')
    fout.write('# COMMENT\n')
    if comment is not None :
        fout.write('# %s\n' % comment)
    genes = result.initiation.keys()
    for gene in genes :
        initiation = result.initiation[gene]
        initiation = lib.simulator.RateFunction.sigmoid(initiation)
        fout.write('%s\t%.20f\n' % (gene, initiation))
    fout.close()
