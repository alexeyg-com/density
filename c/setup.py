#!/bin/env python
from distutils.core import setup, Extension
from os import environ

gillespie = Extension('gillespie',
    sources = ['gillespie.c'],
    #libraries = ['gsl', 'gslcblas', 'profiler'],
    libraries = ['gsl', 'gslcblas'],
    library_dirs = ['/home/nfs/alexeygritsenk/env/sys_enhance/lib64/'])

setup(name = 'Gillespie',
      version = '1.0',
      description = 'A C extension for TASEP Monte-Carlo simulations.',
      author = 'Alexey A. Gritsenko',
      author_email = 'a.gritsenko@tudelft.nl',
      long_description = 'A C implementation of the Gillespie algorithm for continuous-time Monte Carlo simulation of the Totally Asymmetric Exclusion Process (TASEP)',
      #include_dirs = ['/opt/insy/env/sys_enhance/include/python2.7/', '/opt/insy/env/sys_enhance/lib64/python2.7/site-packages/numpy/core/include/numpy/', '/opt/insy/env/sys_enhance/include/'],
      include_dirs = ['/home/nfs/alexeygritsenk/env/sys_enhance/include/python2.7/', '/home/nfs/alexeygritsenk/env/sys_enhance/lib64/python2.7/site-packages/numpy/core/include/numpy/', '/home/nfs/alexeygritsenk/env/sys_enhance/include/'],
      ext_modules = [gillespie])
