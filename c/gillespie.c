#include <Python.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>
#include <limits.h>
#include "arrayobject.h"

#define ZERO_RATE (1e-8)

/* .... C vector utility functions ..................*/
PyArrayObject *pyvector(PyObject *objin);
double *pyvector_to_Carrayptrs(PyArrayObject *arrayin);
int  not_doublevector(PyArrayObject *vec);

/* Vector Utility functions */
 
/* ==== Make a Python Array Obj. from a PyObject, ================
generates a double vector w/ contiguous memory which may be a new allocation if
the original was not a double type or contiguous 
!! Must DECREF the object returned from this routine unless it is returned to the
caller of this routines caller using return PyArray_Return(obj) or
PyArray_BuildValue with the "N" construct   !!!
*/
PyArrayObject *pyvector(PyObject *objin) 
{
    return (PyArrayObject *) PyArray_ContiguousFromObject(objin, NPY_DOUBLE, 1, 1);
}

/* ==== Create 1D Carray from PyArray ======================
Assumes PyArray is contiguous in memory.             */
double *pyvector_to_Carrayptrs(PyArrayObject *arrayin) 
{
    return (double *) arrayin->data;  /* pointer to arrayin data as double */
}

/* ==== Check that PyArrayObject is a double (Float) type and a vector ==============
return 1 if an error and raise exception */ 
int  not_doublevector(PyArrayObject *vec)
{
    if (vec->descr->type_num != NPY_DOUBLE || vec->nd != 1)
    {
        PyErr_SetString(PyExc_ValueError, "In not_doublevector: array must be of type Float and 1 dimensional (n).");
        return 1;
    }
    return 0;
}

static gsl_rng *random_generator = 0;

inline double uniform(void)
{
    return gsl_rng_uniform(random_generator);
}

inline double exponential(double scale)
{
    return gsl_ran_exponential(random_generator, scale);
}

inline unsigned int geometric(double p)
{
    return gsl_ran_geometric(random_generator, p);
}

inline double erlang(unsigned int n, double beta)
{
    return gsl_ran_gamma(random_generator, n, beta);
}

void lprint(int *list, int s, int f, int max_len)
{
    for (; s != f; s = (s + 1) & max_len)
        printf("%d ", list[s]);
    printf("\n");
}

inline int lget_length(int s, int f, int max_len)
{
    if (s <= f)
        return f - s;
    else
        return max_len - s + f + 1;
}

inline void lappend_left(int *list, int *s, int *f, int max_len, int what)
{
    *s = *s - 1;
    if (*s < 0)
        *s = *s + max_len + 1;
    list[*s] = what;
}

inline void lpop_right(int *list, int *s, int *f, int max_len)
{
    *f = *f - 1;
    if (*f < 0)
        *f = *f + max_len + 1;
}

inline void lremove(int *list, int *s, int *f, int max_len, int where)
{
    *f = *f - 1;
    if (*f < 0)
        *f = *f + max_len + 1;
    list[where] = list[*f];
}

char check_positions(int *list, int s, int f, int max_len, int L, int n)
{
    int i, j;
    for (i = s, j = (i + 1) & max_len; j != f && i != f; i = j,  j = (j + 1) & max_len)
        if (list[j] - list[i] < L)
        {
            printf("s = %d, f = %d, max_len = %d, i = %d, j = %d, list[i] = %d, list[j] = %d\n", s, f, max_len, i, j, list[i], list[j]);
            printf("Distance: %d (should be %d)\n", list[j] - list[i], L);
            return 0;
        }
        
    for (i = s; i != f; i = (i + 1) & max_len)
        if (list[i] < 0 || list[i] >= n)
        {
            printf("Location: %d (max %d)\n", list[i], n - 1);
            return 0;
        }
    return 1;
}

double determine_rate(int *list, int s, int f, int max_len, int L, double init, double *rate)
{
    double res = 0.0;
    int i, j;
    if (s == f)
        return init;
    if (list[s] >= L)
        res += init;
    for (i = s, j = (i + 1) & max_len; j != f; i = j,  j = (j + 1) & max_len)
        if (list[j] - list[i] > L)
            res += rate[list[i]];
    i = f - 1;
    if (i < 0)
        i = i + max_len + 1;
    res += rate[list[i]];
    return res;
}

char estimates_converged(double *a, double *b, int n, double eps)
{
    char result = 1;
    int i;
    for (i = 0; i < n; i++)
        if (fabs(a[i] - b[i]) >= eps)
        {
            result = 0;
            break;
        }
    return result;
};

void obtain_estimates(double *density, double total_time, PyObject *segments, int n_segments, double *measurements)
{
    PyObject *tuple, *left_object, *right_object;
    int k;
    long left, right, p;
    for (k = 0; k < n_segments; k++)
    {
        tuple = PyList_GetItem(segments, k);
        left_object = PyTuple_GetItem(tuple, 0);
        right_object = PyTuple_GetItem(tuple, 1);
        left = PyInt_AsLong(left_object);
        right = PyInt_AsLong(right_object);
        double sum = 0.0;
        for (p = left; p <= right; p++)
            sum += density[p];
        sum /= total_time;
        sum /= right - left + 1;
        measurements[k] = sum;
    }
};

static PyObject *gillespie_srand(PyObject *self, PyObject *args)
{
    int value;
    if (!PyArg_ParseTuple(args, "i", &value)) 
        return NULL;
    gsl_rng_set(random_generator, value);
    Py_RETURN_NONE;
}

static PyObject *gillespie_rand(PyObject *self, PyObject *args)
{
    unsigned int value = gsl_rng_get(random_generator);
    return Py_BuildValue("I", value);
}

static PyObject *gillespie_simulate_naive(PyObject *self, PyObject *args)
{
    PyArrayObject *rate_np, *density_np;
    double *rate, *density;
    double init;
    int burn, total_steps, L;
    int dims[2];

    if (!PyArg_ParseTuple(args, "dO!iii", &init, &PyArray_Type, &rate_np, &L, &burn, &total_steps)) 
        return NULL;
    if (rate_np == NULL)
        return NULL;
    if (not_doublevector(rate_np))
        return NULL;

    rate = pyvector_to_Carrayptrs(rate_np);
    int n = rate_np->dimensions[0];
    dims[0] = n;
    density_np = (PyArrayObject *) PyArray_FromDims(1, dims, NPY_DOUBLE);
    density = pyvector_to_Carrayptrs(density_np);

    int min_required = n / L + 1;
    int max_len = 1;
    while (max_len <= min_required)
        max_len = max_len << 1;
    memset(density, 0, n * sizeof(double));
    int *list = malloc((size_t) max_len * sizeof(int));
    max_len = max_len - 1;
    int s = max_len;
    int f = max_len;
    int simulation_time = 0;
    int termination_events = 0;
    double p;
    int step, i;
    int event_type;
    for (step = 0; step < total_steps + burn; ++step)
    {
        /*if (!check_positions(list, s, f, max_len, L, n))
        {
            printf("Incorrect ribosome positioning!\n");
            break;
        }*/
        if (step >= burn) // Record the state BEFORE we move
        {
            simulation_time += 1;
            for (i = s; i != f; i = (i + 1) & max_len)
                density[list[i]] += 1.0;
        }
        p = uniform();
        event_type = gsl_rng_get(random_generator) % (lget_length(s, f, max_len) + 1);
        if (event_type == 0)
        {
            if (lget_length(s, f, max_len) == 0 || list[s] >= L)
            {
                if (p < init)
                    lappend_left(list, &s, &f, max_len, 0);
            }
        }
        else
        {
            event_type--;
            i = (s + event_type) & max_len;
            int id = list[i];
            int j = (i + 1) & max_len;
            if (p < rate[id])
            {
                if (j == f)
                {
                    if (id == n - 1)
                    {
                        lpop_right(list, &s, &f, max_len);
                        if (step >= burn)
                           termination_events++;
                    }
                    else
                    {
                        list[i]++;
                    }
                }
                else
                {
                    if (list[j] - id > L)
                    {
                        list[i]++;
                    }
                }
            }
        }
    }

    for (i = 0; i < n; i++) 
        density[i] /= (double)simulation_time;
    double J = termination_events / (double)simulation_time;
    free(list);
    return Py_BuildValue("fN", J, density_np);
}

inline void flush_density(double *density, double *arrival_time, double simulation_time, int *list, int s, int f, int max_len)
{
    int i;
    for (i = s; i != f; i = (i + 1) & max_len)
    {
        density[list[i]] += simulation_time - arrival_time[list[i]];
        arrival_time[list[i]] = simulation_time;
    }
}

static PyObject *gillespie_simulate(PyObject *self, PyObject *args)
{
    PyArrayObject *rate_np, *density_np;
    PyObject *segments;
    double *rate, *density;
    double eps;
    double init;
    int burn, burn_max_steps, increment, max_steps, L;
    int n_segments;
    int dims[2];

    if (!PyArg_ParseTuple(args, "dO!iOdiiii", &init, &PyArray_Type, &rate_np, &L, &segments, &eps, &burn, &burn_max_steps, &increment, &max_steps)) 
        return NULL;
    if (rate_np == NULL)
        return NULL;
    if (not_doublevector(rate_np))
        return NULL;
    n_segments = PyList_Size(segments);

    rate = pyvector_to_Carrayptrs(rate_np);
    int n = rate_np->dimensions[0];
    dims[0] = n;
    density_np = (PyArrayObject *) PyArray_FromDims(1, dims, NPY_DOUBLE);
    density = pyvector_to_Carrayptrs(density_np);

    int min_required = n / L + 1;
    int max_len = 1;
    while (max_len <= min_required)
        max_len = max_len << 1;
    memset(density, 0, n * sizeof(double));
    memset(density, 0, n * sizeof(double));
    int *list = malloc((size_t) max_len * sizeof(int));
    max_len = max_len - 1;
    int s = max_len;
    int f = max_len;
    double event_rate = init;
    double simulation_time = 0.0;
    int termination_events = 0;
    double event_time, p;
    
    int half_increment = increment / 2;
    int current_max_steps = increment;
    double *old_estimates = malloc((size_t) n_segments * sizeof(double));
    double *new_estimates = malloc((size_t) n_segments * sizeof(double));
    double *arrival_time = malloc((size_t) n * sizeof(double));
    char success = 0;

    int i, step = 0;
    int burn_steps = 0;
    while (1)
    {
        if (step > max_steps + burn_steps && burn_steps > 0) // over the budget
        {
            if (n_segments > 0)
            {
                flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
                obtain_estimates(density, simulation_time, segments, n_segments, new_estimates);
                success = estimates_converged(old_estimates, new_estimates, n_segments, eps);
            }
            else
                success = 1;
            break;
        }
        if (n_segments > 0 && step == half_increment + burn_steps + 1 && burn_steps > 0) // first estimation
        {
            flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
            obtain_estimates(density, simulation_time, segments, n_segments, new_estimates);
        }
        if (n_segments > 0 && step == current_max_steps + burn_steps + 1 && burn_steps > 0) // reached current maximum
        {
            memcpy(old_estimates, new_estimates, (size_t) n_segments * sizeof(double));
            flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
            obtain_estimates(density, simulation_time, segments, n_segments, new_estimates);
            success = estimates_converged(old_estimates, new_estimates, n_segments, eps);
            if (success)
                break;
            current_max_steps += increment;
        }
        if (event_rate < ZERO_RATE)
            break;
        
        //if (step % 1000000 == 0)
        //    printf("Step %d : term %d, event %.5f\n", step, termination_events, event_rate);
        
        event_time = exponential(1.0 / event_rate);
        if ((termination_events == burn && burn_steps == 0) || (burn_steps == 0 && step == burn_max_steps))
        {
            burn_steps = step;
            for (i = s; i != f; i = (i + 1) & max_len)
                arrival_time[list[i]] = 0.0;
        }
        if (termination_events >= burn) // Record the time BEFORE we move
            simulation_time += event_time;
        
        p = uniform() * event_rate;
        if (p < init)
        {
            if (lget_length(s, f, max_len) == 0 || list[s] >= L)
            {
                lappend_left(list, &s, &f, max_len, 0);
                arrival_time[0] = simulation_time;
                event_rate += rate[0];
            }
        }
        else
        {
            p -= init;
            for (i = s; i != f && p > rate[list[i]]; i = (i + 1) & max_len)
                p -= rate[list[i]];
            if (i == f)
            {
                i = i - 1;
                if (i < 0)
                    i = i + max_len + 1;
            }
            int id = list[i];
            int j = (i + 1) & max_len;
            if (j == f)
            {
                if (id == n - 1)
                {
                    event_rate -= rate[id];
                    lpop_right(list, &s, &f, max_len);
                    density[id] += simulation_time - arrival_time[id];
                    termination_events++;
                }
                else
                {
                    list[i]++;
                    event_rate += rate[id + 1] - rate[id];
                    arrival_time[id + 1] = simulation_time;
                    density[id] += simulation_time - arrival_time[id];
                }
            }
            else
            {
                if (list[j] - id > L)
                {
                    list[i]++;
                    event_rate += rate[id + 1] - rate[id];
                    arrival_time[id + 1] = simulation_time;
                    density[id] += simulation_time - arrival_time[id];
                }
            }
        }
        ++step;
    }

    flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
    for (i = 0; i < n; i++) 
        density[i] /= simulation_time;
    double J = (termination_events - burn) / simulation_time;
    free(list);
    free(old_estimates);
    free(new_estimates);
    free(arrival_time);

    return Py_BuildValue("fNib", J, density_np, step, success);
};

static PyObject *gillespie_simulate_nobump(PyObject *self, PyObject *args)
{
    PyArrayObject *rate_np, *density_np;
    PyObject *segments;
    double *rate, *density;
    double eps;
    double init;
    int burn, burn_max_steps, increment, max_steps, L;
    int n_segments;
    int dims[2];

    if (!PyArg_ParseTuple(args, "dO!iOdiiii", &init, &PyArray_Type, &rate_np, &L, &segments, &eps, &burn, &burn_max_steps, &increment, &max_steps)) 
        return NULL;
    if (rate_np == NULL)
        return NULL;
    if (not_doublevector(rate_np))
        return NULL;
    n_segments = PyList_Size(segments);

    rate = pyvector_to_Carrayptrs(rate_np);
    int n = rate_np->dimensions[0];
    dims[0] = n;
    density_np = (PyArrayObject *) PyArray_FromDims(1, dims, NPY_DOUBLE);
    density = pyvector_to_Carrayptrs(density_np);

    int min_required = n / L + 2;
    int max_len = 1;
    while (max_len <= min_required)
        max_len = max_len << 1;
    memset(density, 0, n * sizeof(double));
    int *list = malloc((size_t) max_len * sizeof(int));
    int *pos = malloc((size_t) max_len * sizeof(int));
    max_len = max_len - 1;
    int s = max_len;
    int f = max_len;
    int s_pos = max_len;
    int f_pos = max_len;
    int cur;
    double event_rate = init, block_event_rate = 0.0;
    double lambda;
    char can_initiate = 1;
    double simulation_time = 0.0;
    int termination_events = 0;
    int bumps = 0;
    double event_time, p;
    unsigned int event_count;

    int half_increment = increment / 2;
    int current_max_steps = increment;
    double *old_estimates = malloc((size_t) n_segments * sizeof(double));
    double *new_estimates = malloc((size_t) n_segments * sizeof(double));
    double *arrival_time = malloc((size_t) n * sizeof(double));
    char success = 0;

    int i, step = 0;
    int burn_steps = 0;
    while (1)
    {
        //printf("Step %d, s = %d, f = %d, s_pos = %d, f_pos = %d, max_len = %d\n", step, s, f, s_pos, f_pos, max_len);
        //lprint(list, s, f, max_len);
        //lprint(pos, s_pos, f_pos, max_len);
        if (step > max_steps + burn_steps && burn_steps > 0) // over the budget
        {
            if (n_segments > 0)
            {
                flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
                obtain_estimates(density, simulation_time, segments, n_segments, new_estimates);
                success = estimates_converged(old_estimates, new_estimates, n_segments, eps);
            }
            else
                success = 1;
            break;
        }
        if (n_segments > 0 && step == half_increment + burn_steps + 1 && burn_steps > 0) // first estimation
        {
            flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
            obtain_estimates(density, simulation_time, segments, n_segments, new_estimates);
        }
        if (n_segments > 0 && step == current_max_steps + burn_steps + 1 && burn_steps > 0) // reached current maximum
        {
            memcpy(old_estimates, new_estimates, (size_t) n_segments * sizeof(double));
            flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
            obtain_estimates(density, simulation_time, segments, n_segments, new_estimates);
            success = estimates_converged(old_estimates, new_estimates, n_segments, eps);
            if (success)
                break;
            current_max_steps += increment;
        }

        if (event_rate < ZERO_RATE)
            break;

        if (event_rate < 0.0) // safety guard
            event_rate = 0.0;
        if (block_event_rate < 0.0) // safety guard
            block_event_rate = 0.0;
        lambda = 1.0 / (event_rate + block_event_rate);
        p = event_rate * lambda;
        if (p > 1.0) // safety guard
            p = 1.0;
        event_count = geometric(p);
        if (event_count == 1)
            event_time = exponential(lambda);
        else if (event_count == 2)
            event_time = exponential(lambda) + exponential(lambda);
        else
            event_time = erlang(event_count, lambda);
        if ((termination_events == burn && burn_steps == 0) || (burn_steps == 0 && step == burn_max_steps))
        {
            burn_steps = step;
            for (i = s; i != f; i = (i + 1) & max_len)
                arrival_time[list[i]] = 0.0;
        }
        if (termination_events >= burn) // Record time BEFORE we move
        {
            simulation_time += event_time;
            bumps += event_count - 1;
        }
        
        p = uniform() * event_rate;
        //if (step >= 1259104 - 100)
        //    printf("p = %.30f, event rate = %.30f, init = %.30f\n", p, event_rate, init);
        if (can_initiate && ((p < init) || (s_pos == f_pos)))
        {
            lappend_left(list, &s, &f, max_len, 0);
            arrival_time[0] = simulation_time;
            can_initiate = 0;
            event_rate -= init;
            block_event_rate += init;
            if (lget_length(s, f, max_len) == 1 || list[(s + 1) & max_len] > L) // Check if the newly bound ribosome has space to move forward (if we are the only ribosome or if there is enough space before the next ribosome)
            {
                lappend_left(pos, &s_pos, &f_pos, max_len, s);
                event_rate += rate[0];
            }
            else
                block_event_rate += rate[0];
        }
        else
        {
            if (can_initiate)
                p -= init;
            for (cur = s_pos; cur != f_pos && p > rate[list[pos[cur]]]; cur = (cur + 1) & max_len)
                p -= rate[list[pos[cur]]];
            if (cur == f_pos)
            {
                cur = cur - 1;
                if (cur < 0)
                    cur = cur + max_len + 1;
            }
            i = pos[cur];
            int id = list[i]; 
            int j = (i + 1) & max_len;
            int k = i - 1;
            if (k < 0)
                k = k + max_len + 1;
            //printf("i = %d id = %d (s_pos = %d f_pos = %d - max_len = %d | cur = %d)\n", i, id, s_pos, f_pos, max_len, cur);
            if (j == f) // We are the last ribosome on the mRNA
            {
                if (id == n - 1) // We are also finishing translation
                {
                    if (lget_length(s, f, max_len) == 1) // We are the only ribosome
                    {
                        if (!can_initiate) // We could not previously initiate, so NOW it should be possible
                        {
                            event_rate += init;
                            block_event_rate -= init;
                            can_initiate = 1;
                        }
                    }
                    else
                    {
                        if (i != s && id - list[k] == L) // If there is a ribosome behind us, check if it can NOW move.
                        {
                            event_rate += rate[list[k]];
                            block_event_rate -= rate[list[k]];
                            lappend_left(pos, &s_pos, &f_pos, max_len, k);
                        }
                    }
                    event_rate -= rate[id]; 
                    lpop_right(list, &s, &f, max_len);
                    lremove(pos, &s_pos, &f_pos, max_len, cur);
                    density[id] += simulation_time - arrival_time[id];
                    termination_events++;
                }
                else
                {
                    if (i != s && id - list[k] == L) // The previous ribosome can NOW move
                    {
                        event_rate += rate[list[k]];
                        block_event_rate -= rate[list[k]];
                        lappend_left(pos, &s_pos, &f_pos, max_len, k);
                    }
                    list[i]++;
                    event_rate += rate[id + 1] - rate[id];
                    arrival_time[id + 1] = simulation_time;
                    density[id] += simulation_time - arrival_time[id];
                    if (id == L - 1) // We can now initiate translation again.
                    {
                        event_rate += init;
                        block_event_rate -= init;
                        can_initiate = 1;
                    }
                }
            }
            else
            {
                if (i != s) // We are NOT the first ribosome
                {
                    if (id - list[k] == L) // The previous ribosome can NOW move
                    {
                        event_rate += rate[list[k]];
                        block_event_rate -= rate[list[k]];
                        lappend_left(pos, &s_pos, &f_pos, max_len, k);
                    }
                }
                else if (id == L - 1) // We can now initiate translation again.
                {
                    event_rate += init;
                    block_event_rate -= init;
                    can_initiate = 1;
                }
                //printf("i = %d id = %d (s_pos = %d f_pos = %d - max_len = %d | cur = %d)\n", i, id, s_pos, f_pos, max_len, cur);
                list[i]++;
                event_rate -= rate[id];
                arrival_time[id + 1] = simulation_time;
                density[id] += simulation_time - arrival_time[id];
                if (list[j] - list[i] > L) // We can still move forward
                    event_rate += rate[id + 1];
                else
                {
                    block_event_rate += rate[id + 1];
                    lremove(pos, &s_pos, &f_pos, max_len, cur);
                }
            }
        }
        ++step;
    }

    flush_density(density, arrival_time, simulation_time, list, s, f, max_len);
    for (i = 0; i < n; i++) 
        density[i] /= simulation_time;
    double J = (termination_events - burn) / simulation_time;
    double queueing = bumps / simulation_time;
    free(list);
    free(pos);
    free(old_estimates);
    free(new_estimates);
    free(arrival_time);
    //printf("Missed time: %.5f, total time: %.5f, fraction %.5f\n", missed_time, simulation_time, missed_time / simulation_time);
    //printf("Missed events: %i, avg. missed time: %.5f\n", missed_events, missed_time / missed_events);
    return Py_BuildValue("ffNib", J, queueing, density_np, step, success);
};

static PyMethodDef GillespieMethods[] = {
    {"srand",  gillespie_srand, METH_VARARGS, "Set random seed for the random number generator."}, 
    {"rand",  gillespie_rand, METH_VARARGS, "Return a random number from the random number generator."}, 
    {"simulate",  gillespie_simulate, METH_VARARGS, "Run a Monte-Carlo simulation of the TASEP model."}, 
    {"simulate_naive",  gillespie_simulate_naive, METH_VARARGS, "Run a Monte-Carlo simulation of the TASEP model (naive version)."}, 
    {"simulate_nobump",  gillespie_simulate_nobump, METH_VARARGS, "Run a Monte-Carlo simulation of the TASEP model (non-bumping version with Erlang distribution)."}, 
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

void cleanup_gillespie(void)
{
    gsl_rng_free(random_generator);
}

PyMODINIT_FUNC initgillespie(void)
{
    (void) Py_InitModule("gillespie", GillespieMethods);
    import_array(); // NumPy
    
    random_generator = gsl_rng_alloc(gsl_rng_taus);
    Py_AtExit(cleanup_gillespie);
}
