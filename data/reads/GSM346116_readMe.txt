All feature names and reference sequences are taken from the Saccharomyces Genome Database, 2008-06-22.  The UTR annoations used in the quantification are from Nagalakshmi et al., Science 2008.

A per-gene quantification of transcription (mRNA) or translation
(Footprints) is in the quant file, with column 1 as the gene name and column 2 as the expression level.

Alignment files (chr_best):
1: tag sequence
2: tag count
3: alignment score = # of matches, versus reference plus poly-A tail; # mismatches = length($1) - $3.
4: reference sequence name
5: reference sequence start position
6: reference sequence end position
7: maximum reference alignment length
8: length ambiguity in match, due to poly-A tailing; minimum reference alignment length is $7 - $8

Quantification files (quant):
1: feature name
2: normalized read density [rpkM] = $4 / (($5 / 1000) * ($6 / 1000000))
3: read density [rpk]
4: read count
5: feature target length [nt]
6: total CDS-aligned reads
