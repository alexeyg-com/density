#!/usr/bin/env python

from Bio import SeqIO

filename = 'S288C_reference_sequence_R64-1-1_20110203.fsa'
postfix = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'mt']

fin = open(filename, 'rb')
records = list(SeqIO.parse(fin, 'fasta'))
fin.close()

for i in range(len(records)) :
    filename = 'chr' + postfix[i] + '.fasta'
    fout = open(filename, 'wb')
    SeqIO.write([records[i]], fout, 'fasta')
    fout.close()
