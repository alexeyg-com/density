import lib.persistent
import lib.evaluator
import numpy as np
from statlib import stats
#import scipy.stats as stats

def read_belle_halflives(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[6:]
    for line in lines :
        args = line.strip().split('\t')
        if len(args) < 4 :
            continue
        value = args[3]
        if value != '' :
            result[args[0]] = float(value)
    return result

def read_ghaemmaghami_abundances(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    for line in lines :
        args = line.strip().split(',')
        value = args[2]
        if value != '' and value != '#' and value != '-' and value != '%' :
            result[args[0]] = float(value)
    return result

def read_newman_yepd_abundances(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[4:]
    for line in lines :
        args = line.strip().split(',')
        value = args[3]
        if value != '' :
            result[args[0]] = float(value)
    return result

def read_newman_sd_abundances(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[4:]
    for line in lines :
        args = line.strip().split(',')
        value = args[5]
        if value != '' :
            result[args[0]] = float(value)
    return result

def read_shah_inits(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    for line in lines :
        args = line.strip().split()
        result[args[0]] = float(args[1])
    return result

def read_shah_prod(filename_names, filename_times) :
    num2name = {}
    result = {}
    fin = open(filename_names, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    cnt = 0
    for line in lines :
        args = line.strip().split()
        num2name[cnt] = args[0]
        cnt += 1
    fin = open(filename_times, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    for line in lines :
        args = line.strip().split()
        num = int(args[0])
        time = float(args[2])
        if time > 0 :
            result[num2name[num]] = 1.0 / time
    return result

def read_siwiak_inits(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    for line in lines :
        args = line.strip().split(',')
        result[args[0]] = float(args[7])
    return result

def read_siwiak_prod(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    for line in lines :
        args = line.strip().split(',')
        try :
            result[args[0]] = float(args[4])
        except :
            continue
    return result

def read_ciandrini_inits(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[0].split('\r')
    lines = lines[1:]
    for line in lines :
        args = line.strip().split(';')
        try :
            result[args[0]] = float(args[6].replace(',', '.'))
        except :
            continue
    return result

def read_ciandrini_prod(filename) :
    result = {}
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[0].split('\r')
    lines = lines[1:]
    for line in lines :
        args = line.strip().split(';')
        try :
            result[args[0]] = float(args[7].replace(',', '.'))
        except :
            continue
    return result

def read_utr_lengths(filenames) :
    five_prime = {}
def read_utr_lengths(filenames) :
    five_prime = {}
    three_prime = {}
    for filename in filenames :
        fin = open(filename, 'rb')
        lines = fin.readlines()
        fin.close()
        lines = lines[3:]
        for line in lines :
            args = line.strip().split()
            type = args[2]
            start, stop = float(args[3]), float(args[4])
            length = abs(start - stop) + 1
            meta = args[8].split(';')
            name = meta[0].split('=')[1][:-5]
            if type == 'five_prime_UTR' :
                if not name in five_prime :
                    five_prime[name] = length
                else :
                    five_prime[name] = (length + five_prime[name]) / 2.0
            elif type == 'three_prime_UTR' :
                if not name in three_prime :
                    three_prime[name] = length
                else :
                    three_prime[name] = (length + three_prime[name]) / 2.0
    return (five_prime, three_prime)

def partial_correlation(x, y, z, type = 'pearson') :
    from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage as STAP
    from rpy2 import robjects

    x = robjects.FloatVector(x)
    y = robjects.FloatVector(y)
    z = robjects.FloatVector(z)
    #type = robjects.StrVector(type)
    func_str = None
    with open('../R/pcor.R') as f :
        func_str = ''.join(f.readlines())

    pcor = STAP(func_str, 'pcor')
    result = pcor.pcor_test(x, y, z, method = type)
    return (result[0][0], result[1][0])

def prod(a, b) :
    '''
    Compute product of two variables.
    :param a : first variable.
    :param b : second variable.
    '''
    if a is None or b is None :
        return None

    c = {}
    genes = list(set(a.keys()) & set(b.keys()))
    for gene in genes :
        c[gene] = a[gene] * b[gene]
    return c

def correlate2(a, b, a_name, b_name, do_print = True) :
    '''
    Compute correlation between two variables.
    :param a : first dictionary of values.
    :param b : second dictionary of values.
    :param a_name : name of the first variable.
    :param b_name : name of the second variable.
    :param do_print : if True the output is printed.
    '''
    global _added_lines

    if a is None or b is None :
        return

    genes = list(set(a.keys()) & set(b.keys()))
    a_list, b_list = [], []
    for gene in genes :
        a_list.append(a[gene])
        b_list.append(b[gene])
    a = np.array(a_list)
    b = np.array(b_list)
    if do_print :
        print '[i] %s vs. %s (%d)' % (a_name, b_name, len(genes))
        print '    [+] Pearson:  %.2f (p-value %g)' % stats.pearsonr(a, b)
        print '    [+] Spearman: %.2f (p-value %g)' % stats.spearmanr(a, b)
        print ''
        print '[i] Log(%s) vs. Log(%s) (%d)' % (a_name, b_name, len(genes))
        print '    [+] Pearson:  %.2f (p-value %g)' % stats.pearsonr(np.log(a), np.log(b))
        print '    [+] Spearman: %.2f (p-value %g)' % stats.spearmanr(np.log(a), np.log(b))
        print ''
        _added_lines = True
    else :
        pearson = stats.pearsonr(a, b)
        spearman = stats.spearmanr(a, b)
        pearson_log = stats.pearsonr(np.log(a), np.log(b))
        spearman_log = stats.spearmanr(np.log(a), np.log(b))
        return (pearson, spearman, pearson_log, spearman_log)

def correlate3(a, b, c, a_name, b_name, c_name) :
    '''
    Compute partial correlation between two variables.
    :param a : first dictionary of values.
    :param b : second dictionary of values.
    :param c : third dictionary of values.
    :param a_name : name of the first variable.
    :param b_name : name of the second variable.
    '''
    global _added_lines

    if a is None or b is None or c is None :
        return

    genes = list(set(a.keys()) & set(b.keys()) & set(c.keys()))
    a_list, b_list, c_list = [], [], []
    for gene in genes :
        a_list.append(a[gene])
        b_list.append(b[gene])
        c_list.append(c[gene])
    a = np.array(a_list)
    b = np.array(b_list)
    c = np.array(c_list)
    print '[i] %s vs. %s given %s (%d)' % (a_name, b_name, c_name, len(genes))
    print '    [+] Pearson:  %.2f (p-value %g)' % partial_correlation(a, b, c, type = 'pearson')
    print '    [+] Spearman: %.2f (p-value %g)' % partial_correlation(a, b, c, type = 'spearman')
    print ''
    print '[i] Log(%s) vs. Log(%s) given Log(%s) (%d)' % (a_name, b_name, c_name, len(genes))
    print '    [+] Pearson:  %.2f (p-value %g)' % partial_correlation(np.log(a), np.log(b), np.log(c), type = 'pearson')  
    print '    [+] Spearman: %.2f (p-value %g)' % partial_correlation(np.log(a), np.log(b), np.log(c), type = 'spearman')
    print ''
    _added_lines = True

def print_split() :
    '''
    Print a section splitter if a new section has been added.
    '''
    global _added_lines
    if _added_lines :
        print '---------------------------'
        print ''
        _added_lines = False

def correlate_initiation(initiation, other_initiation, utr5, utr3, length, other_initiation_name) :
    '''
    Correlate initiation with other other initiation and other variables.
    :param initiation : original initiations.
    :param other_initiation : other initiations.
    :param utr5 : 5-prime UTRs.
    :param utr3 : 3-prime UTRs.
    :param length : CDS lengths.
    :param other_initiation_name : name of the other initiations.
    '''
    print_split()
    correlate2(initiation, other_initiation, 'Initiation', other_initiation_name)
    correlate2(other_initiation, length, other_initiation_name, 'Length')

    print_split()
    correlate2(utr5, other_initiation, '5-prime UTR', other_initiation_name)
    correlate3(utr5, other_initiation, length, '5-prime UTR', other_initiation_name, 'Length')

    print_split()
    correlate2(utr3, other_initiation, '3-prime UTR', other_initiation_name)
    correlate3(utr3, other_initiation, length, '3-prime UTR', other_initiation_name, 'Length')

def correct_protein_levels(abundance, halflives, V = 2 ** (1.0 / 90.0)) :
    '''
    Correct protein abundance for protein half lives.
    :param abundance : protein abundance variable.
    :param halflives : protein half life variable.
    :param V : dilution factor due to growth.
    '''
    if abundance is None or halflives is None :
        return None
    result = {}
    genes = list(set(abundance.keys()) & set(halflives.keys()))
    for gene in genes :
        a = abundance[gene]
        h = halflives[gene]
        d = 1.0 / h
        p = a * (d + V)
        result[gene] = p
    return result

def correlate_abundance(abundance, length, mrna, halflives, forest_density, abundance_name) :
    '''
    Correlation abundance with length (given mRNA levels).
    :param abundance : protein abundance variable.
    :param length : CDS length variable.
    :param mrna : mRNA level variable.
    :param halflives : protein halflife variable.
    :param forest_density : measured density variable.
    :param abundance_name : name of the abundance variable.
    '''
    print_split()
    correlate2(abundance, length, abundance_name, 'Length')
    correlate2(abundance, forest_density, abundance_name, 'Forest Density')
    correlate3(abundance, forest_density, mrna, abundance_name, 'Forest Density', 'mRNA')
    correlate3(abundance, length, mrna, abundance_name, 'Length', 'mRNA')
    
    corrected_abundance = correct_protein_levels(abundance, halflives)
    correlate2(corrected_abundance, length, abundance_name + ' Corrected', 'Length')
    correlate2(corrected_abundance, forest_density, abundance_name + ' Corrected', 'Forest Density')
    correlate3(corrected_abundance, forest_density, mrna, abundance_name + ' Corrected', 'Forest Density', 'mRNA')
    correlate3(corrected_abundance, length, mrna, abundance_name + ' Corrected', 'Length', 'mRNA')

def correlate_model_with_abundance(initiation, abundance, J, mrna, halflives, initiation_name, J_name, abundance_name) :
    '''
    Correlate model with with abundances.
    :param abundance : protein abundance variable.
    :param J : J (production rate) variable.
    :param mrna : mRNA level variable.
    :param halflives : protein halflife variable.
    :param initiation_name : name of the initiation variable.
    :param J_name : name of the J variable.
    :param abundance_name : name of the abundance variable.
    '''
    print_split()
    correlate2(initiation, abundance, initiation_name, abundance_name)
    correlate2(J, abundance, J_name, abundance_name)
    correlate2(prod(J, mrna), abundance, '%s * mRNA' % J_name, abundance_name)
    correlate3(J, abundance, mrna, J_name, abundance_name, 'mRNA')
    
    corrected_abundance = correct_protein_levels(abundance, halflives)
    correlate2(initiation, corrected_abundance, initiation_name, abundance_name + ' Corrected')
    correlate2(J, corrected_abundance, J_name, abundance_name + ' Corrected')
    correlate2(prod(J, mrna), corrected_abundance, '%s * mRNA' % (J_name + ' Corrected' ), abundance_name)
    correlate3(J, corrected_abundance, mrna, J_name, abundance_name + ' Corrected', 'mRNA')

def print_rp(r, p, names) :
    '''
    Print r and p of a correlation matrix.
    :param r : correlation coefficient matrix.
    :param p : p-value coefficient matrix.
    :param names : variable names.
    '''
    n = len(names)
    for i in range(n) :
        for j in range(i, n) :
            r[j][i] = r[i][j]
            p[j][i] = p[i][j]

    print '%10s ' % '',
    for i in range(n) :
        print '%10s' % names[i],
    print '%10s ' % '',
    for i in range(n) :
        print '%10s' % names[i],
    print ''
    for i in range(n) :
        print '%10s ' % names[i],
        for j in range(n) :
            print '%10g ' % r[i][j],
        print '%10s ' % '',
        for j in range(n) :
            print '%10g ' % p[i][j],
        print ''
    print ''

def cross_correlate(vars, names) :
    '''
    Cross-correlate variables.
    :param vars : variables to correlate.
    :param names : variable names.
    '''
    n = len(vars)
    pearson_r, pearson_p = np.zeros((n, n)), np.zeros((n, n))
    spearman_r, spearman_p = np.zeros((n, n)), np.zeros((n, n))
    pearson_r_log, pearson_p_log = np.zeros((n, n)), np.zeros((n, n))
    spearman_r_log, spearman_p_log = np.zeros((n, n)), np.zeros((n, n))
    for i in range(n) :
        for j in range(i, n) :
            pearson, spearman, pearson_log, spearman_log = correlate2(vars[i], vars[j], names[i], names[j], do_print = False)
            pearson_r[i][j], pearson_p[i][j] = pearson[0], float(pearson[1])
            pearson_r_log[i][j], pearson_p_log[i][j] = pearson_log[0], float(pearson_log[1])
            spearman_r[i][j], spearman_p[i][j] = spearman[0], float(spearman_log[1])
            spearman_r_log[i][j], spearman_p_log[i][j] = spearman_log[0], float(spearman_log[1])

    print '[i] Cross-correlation (Pearson)'
    print_rp(pearson_r, pearson_p, names)
    print '[i] Cross-correlation (Spearman)'
    print_rp(spearman_r, spearman_p, names)
    print '[i] Cross-correlation (Pearson Log)'
    print_rp(pearson_r_log, pearson_p_log, names)
    print '[i] Cross-correlation (Spearman Log)'
    print_rp(spearman_r_log, spearman_p_log, names)

def look_for_correlations(results, forest, mrna = None, other_results = None, shah_results = None, shah_inits = None, siwiak_inits = None, ciandrini_inits = None, shah_prod = None, siwiak_prod = None, ciandrini_prod = None, newman_yepd_abundances = None, newman_sd_abundances = None, ghaemmaghami_abundances = None, belle_halflives = None, five_primes = None, three_primes = None) :
    genes = results.names
    initiation, density, J, length = {}, {}, {}, {}
    mrna = {}
    forest_density = {}
    for gene in genes :
        init = lib.simulator.RateFunction.sigmoid(results.initiation[gene])
        length_cur = forest[gene].length_codons
        length[gene] = length_cur
        initiation[gene] = init
        J[gene] = results.J[gene]
        density[gene] = np.mean(results.density[gene])
        node = forest[gene].tree[0]
        if node[0] == 0 and node[1] == length_cur - 1 :
            mrna[gene] = 2 ** node[4]
            forest_density[gene] = 2 ** node[2]

    other_initiation, other_density, other_J, other_length = None, None, None, None
    if other_results is not None :
        other_initiation, other_density, other_J = {}, {}, {}
        genes = other_results.names
        for gene in genes :
            init = lib.simulator.RateFunction.sigmoid(other_results.initiation[gene])
            other_initiation[gene] = init
            other_J[gene] = other_results.J[gene]
            other_density[gene] = np.mean(other_results.density[gene])

    shah_density = None
    if shah_results is not None :
        shah_density = {}
        for gene in shah_results.density.keys() :
            shah_density[gene] = np.mean(shah_results.density[gene])

    all_inits, all_inits_names = [initiation], ['Initiation']
    if other_initiation is not None :
        all_inits.append(other_initiation)
        all_inits_names.append('Other')
    if shah_inits is not None :
        all_inits.append(shah_inits)
        all_inits_names.append('Shah')
    if ciandrini_inits is not None :
        all_inits.append(ciandrini_inits)
        all_inits_names.append('Ciandrini')
    if siwiak_inits is not None :
        all_inits.append(siwiak_inits)
        all_inits_names.append('Siwiak')

    all_J, all_J_names = [J], ['J']
    if other_J is not None :
        all_J.append(other_J)
        all_J_names.append('Other')
    if shah_prod is not None :
        all_J.append(shah_prod)
        all_J_names.append('Shah')
    if ciandrini_prod is not None :
        all_J.append(ciandrini_prod)
        all_J_names.append('Ciandrini')

    print '[i] Initiation cross-correlation.'
    cross_correlate(all_inits, all_inits_names)
    
    print_split()
    print '[i] J cross-correlation.'
    cross_correlate(all_J, all_J_names)

    correlate2(initiation, length, 'Initiation', 'Length')
    correlate2(initiation, density, 'Initiation', 'Density')
    correlate2(initiation, J, 'Initiation', 'J')
    correlate2(initiation, J, 'Initiation', 'J')
    correlate2(J, length, 'J', 'Length')
    correlate2(J, density, 'J', 'Density')
    correlate2(density, length, 'Density', 'Length')
    correlate2(forest_density, length, 'Forest Density', 'Length')
    
    print_split()
    correlate2(mrna, length, 'mRNA', 'Length')
    
    print_split()
    correlate2(five_primes, initiation, '5-prime UTR', 'Initiation')
    correlate3(five_primes, initiation, length, '5-prime UTR', 'Initiation', 'Length')
    correlate2(five_primes, J, '5-prime UTR', 'J')
    correlate2(five_primes, density, '5-prime UTR', 'Density')
    correlate2(five_primes, length, '5-prime UTR', 'Length')
    
    print_split()
    correlate2(three_primes, initiation, '3-prime UTR', 'Initiation')
    correlate3(three_primes, initiation, length, '3-prime UTR', 'Initiation', 'Length')
    correlate2(three_primes, J, '3-prime UTR', 'J')
    correlate2(three_primes, density, '3-prime UTR', 'Density')
    correlate2(three_primes, length, '3-prime UTR', 'Length')
    
    print_split()
    correlate2(initiation, other_initiation, 'Initiation', 'Initiation other')
    correlate2(J, other_J, 'J', 'J other')
    correlate2(other_density, density, 'Density other', 'Density')
    correlate2(other_density, length, 'Density other', 'Length')
    
    print_split()
    correlate2(shah_density, density, 'Shah Density', 'Density')
    correlate2(shah_density, length, 'Shah Density', 'Length')
    
    print_split()
    correlate2(shah_inits, shah_density, 'Shah Initiation', 'Shah Density')
   
    correlate_initiation(initiation, shah_inits, five_primes, three_primes, length, 'Shah Initiation')
    correlate_initiation(initiation, ciandrini_inits, five_primes, three_primes, length, 'Ciandrini Initiation')
    correlate_initiation(initiation, siwiak_inits, five_primes, three_primes, length, 'Siwiak Initiation')
    
    correlate_abundance(newman_yepd_abundances, length, mrna, belle_halflives, forest_density, 'Newman YEPD abundance')
    correlate_abundance(newman_sd_abundances, length, mrna, belle_halflives, forest_density, 'Newman SD abundance')
    correlate_abundance(ghaemmaghami_abundances, length, mrna, belle_halflives, forest_density, 'Ghaemmaghami abundance')

    correlate_model_with_abundance(initiation, newman_yepd_abundances, J, mrna, belle_halflives, 'Initiation', 'J', 'Newman YEPD abundances')
    correlate_model_with_abundance(initiation, newman_sd_abundances, J, mrna, belle_halflives, 'Initiation', 'J', 'Newman SD abundances')
    correlate_model_with_abundance(initiation, ghaemmaghami_abundances, J, mrna, belle_halflives, 'Initiation', 'J', 'Ghaemmaghami abundances')
    
    correlate_model_with_abundance(other_initiation, newman_yepd_abundances, other_J, mrna, belle_halflives, 'Initiation other', 'J other', 'Newman YEPD abundances')
    correlate_model_with_abundance(other_initiation, newman_sd_abundances, other_J, mrna, belle_halflives, 'Initiation other', 'J other', 'Newman SD abundances')
    correlate_model_with_abundance(other_initiation, ghaemmaghami_abundances, other_J, mrna, belle_halflives, 'Initiation other', 'J other', 'Ghaemmaghami abundances')
    
    correlate_model_with_abundance(ciandrini_inits, newman_yepd_abundances, ciandrini_prod, mrna, belle_halflives, 'Ciandrini Initiation', 'Ciandrini J', 'Newman YEPD abundances')
    correlate_model_with_abundance(ciandrini_inits, newman_sd_abundances, ciandrini_prod, mrna, belle_halflives, 'Ciandrini Initiation', 'Ciandrini J', 'Newman SD abundances')
    correlate_model_with_abundance(ciandrini_inits, ghaemmaghami_abundances, ciandrini_prod, mrna, belle_halflives, 'Ciandrini Initiation', 'Ciandrini J', 'Ghaemmaghami abundances')
    
    correlate_model_with_abundance(siwiak_inits, newman_yepd_abundances, None, mrna, belle_halflives, 'Siwiak Initiation', 'Siwiak J', 'Newman YEPD abundances')
    correlate2(siwiak_prod, newman_yepd_abundances, 'Siwiak J * mRNA', 'Newman YEPD abundances')
    correlate_model_with_abundance(siwiak_inits, newman_sd_abundances, None, mrna, belle_halflives, 'Siwiak Initiation', 'Siwiak J', 'Newman SD abundances')
    correlate2(siwiak_prod, newman_sd_abundances, 'Siwiak J * mRNA', 'Newman SD abundances')
    correlate_model_with_abundance(siwiak_inits, ghaemmaghami_abundances, None, mrna, belle_halflives, 'Siwiak Initiation', 'Siwiak J', 'Ghaemmaghami abundances')
    correlate2(siwiak_prod, ghaemmaghami_abundances, 'Siwiak J * mRNA', 'Newman SD abundances')
    
    correlate_model_with_abundance(shah_inits, newman_yepd_abundances, shah_prod, mrna, belle_halflives, 'Shah Initiation', 'Shah J', 'Newman YEPD abundances')
    correlate_model_with_abundance(shah_inits, newman_sd_abundances, shah_prod, mrna, belle_halflives, 'Shah Initiation', 'Shah J', 'Newman SD abundances')
    correlate_model_with_abundance(shah_inits, ghaemmaghami_abundances, shah_prod, mrna, belle_halflives, 'Shah Initiation', 'Shah J', 'Ghaemmaghami abundances')
    
def look_for_correlations_wrapper(results, other_results = None) :
    forest = lib.persistent.forest
    newman_yepd = read_newman_yepd_abundances('../support/newman.csv')
    print '[+] Read Newman YEPD abundances'
    newman_sd = read_newman_sd_abundances('../support/newman.csv')
    print '[+] Read Newman SD abundances'
    ghaemmaghami = read_ghaemmaghami_abundances('../support/ghaemmaghami.csv')
    print '[+] Read Ghaemmaghami abundances'
    siwiak_inits = read_siwiak_inits('../support/siwiak.csv')
    print '[+] Read Siwiak initiation rates'
    siwiak_prod = read_siwiak_prod('../support/siwiak.csv')
    print '[+] Read Siwiak production rates'
    shah_inits = read_shah_inits('../support/S.cer.mRNA.abndc.ini.tsv')
    print '[+] Read Shah initiation rates'
    shah_prod = read_shah_prod('../support/S.cer.mRNA.abndc.ini.tsv', '../support/output_gene_totetimes.out')
    print '[+] Read Shah production rates'
    ciandrini_inits = read_ciandrini_inits('../support/ciandrini.csv')
    print '[+] Read Ciandrini initiation rates'
    ciandrini_prod = read_ciandrini_prod('../support/ciandrini.csv')
    print '[+] Read Ciandrini production rates'
    belle_halflives = read_belle_halflives('../support/belle.csv')
    print '[+] Read Bell half lives.'
    utr5, utr3 = read_utr_lengths(['../data/gff/R64/Nagalakshmi_2008_UTRs.gff3', '../data/gff/R64/Yassour_2009_UTRs.gff3'])
    print '[+] Read 5- and 3-prime UTR lengths'
    shah = lib.evaluator.read_shah_output('../support/S.cer.mRNA.abndc.ini.tsv', '../../../data/shah/mod/SMoPT/output2/shah-time-2.4mil-seed-1413_ribo_pos_2400000', 2400000)
    shah = lib.evaluator.evaluate_profiles(shah, forest)
    print '[+] Read Shah density'

    print ''
    look_for_correlations(results, forest, other_results = other_results, shah_results = shah, shah_inits = shah_inits, siwiak_inits = siwiak_inits, ciandrini_inits = ciandrini_inits, shah_prod = shah_prod, siwiak_prod = siwiak_prod, ciandrini_prod = ciandrini_prod, newman_yepd_abundances = newman_yepd, newman_sd_abundances = newman_sd, ghaemmaghami_abundances = ghaemmaghami, belle_halflives = belle_halflives, five_primes = utr5, three_primes = utr3)

def _initialize_module() :
    global _added_lines
    _added_lines = False

_initialize_module()
