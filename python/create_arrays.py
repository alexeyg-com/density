#!/bin/env python

import lib.reader
import lib.writer

def get_records() :
    '''
    Return read GFF records array.
    '''
    records = lib.reader.read_features(['../data/gff/R64/SGD_Everything.gff3'])
    print '[+] Read GFF annotations.'

    return records

def get_reference() :
    '''
    Return read reference sequences.
    '''
    reference = lib.reader.read_references(['../data/sequences/R64/chrI.fasta', '../data/sequences/R64/chrII.fasta', '../data/sequences/R64/chrIII.fasta', '../data/sequences/R64/chrIV.fasta', '../data/sequences/R64/chrV.fasta', '../data/sequences/R64/chrVI.fasta', '../data/sequences/R64/chrVII.fasta', '../data/sequences/R64/chrVIII.fasta', '../data/sequences/R64/chrIX.fasta', '../data/sequences/R64/chrX.fasta', '../data/sequences/R64/chrXI.fasta', '../data/sequences/R64/chrXII.fasta', '../data/sequences/R64/chrXIII.fasta', '../data/sequences/R64/chrXIV.fasta', '../data/sequences/R64/chrXV.fasta', '../data/sequences/R64/chrXVI.fasta', '../data/sequences/R64/chrmt.fasta'])
    print '[+] Read reference.'

    return reference

def get_changes() :
    '''
    Return read changes file.
    '''
    changes = lib.reader.read_changes_file('../data/all_chromosome_sequence_changes.tab')
    print '[+] Read changes file.'

    return changes

def update_alignments(input = None, output = None) :
    '''
    Update alignment to the latest genome release.
    :param input: a list of input filenames.
    :param output: a list of output filenames.
    '''
    if input is None :
        input = ['../data/reads/GSM346111_fp_rich1_chr_best.txt', '../data/reads/GSM346114_fp_rich2_chr_best.txt', '../data/reads/GSM346117_mrna_rich1_chr_best.txt', '../data/reads/GSM346118_mrna_rich2_chr_best.txt']
    if output is None :
        output = ['../data/reads/rich1_rib.out', '../data/reads/rich2_rib.out', '../data/reads/rich1_mrna.out', '../data/reads/rich2_mrna.out']

    changes = get_changes()
    reference = get_reference()
    for i in range(len(input)) :
        alignment = lib.reader.read_alignment(input[i])
        print '[+] Read alignment: %s' % input[i]
        alignment = lib.reader.apply_changes_to_alignment(reference, changes, alignment, 59)
        print '[+] Updated alignment.'
        lib.writer.write_pickle(alignment, output[i])
        print '[+] Wrote alignment (pickle): %s' % output[i]

def get_ribosome_alignment(selection = -1) :
    '''
    Return ribosome alignment.
    :param selection: which replica to return. -1 for both.
    '''
    rich1_rib, rich2_rib = [], []
    if selection == 0 or selection < 0 :
        rich1_rib = lib.reader.read_pickle('../data/reads/rich1_rib.out')
    if selection == 1 or selection < 0 :
        rich2_rib = lib.reader.read_pickle('../data/reads/rich2_rib.out')
    alignment = rich1_rib + rich2_rib
    print '[+] Read ribosome alignment: %s alignment groups.' % format(len(alignment), ',d')

    return alignment

def get_mrna_alignment(selection = -1) :
    '''
    Return mRNA alignment.
    :param selection: which replica to return. -1 for both.
    '''
    rich1_mrna, rich2_mrna = [], []
    if selection == 0 or selection < 0 :
        rich1_mrna = lib.reader.read_pickle('../data/reads/rich1_mrna.out')
    if selection == 1 or selection < 0 :
        rich2_mrna = lib.reader.read_pickle('../data/reads/rich2_mrna.out')
    alignment = rich1_mrna + rich2_mrna
    print '[+] Read mRNA alignment: %s alignment groups.' % format(len(alignment), ',d')

    return alignment

def filter_alignment(alignment, mismatches = 2, min_length = 22, max_length = 32, max_nucA = 18, unique = False) :
    '''
    Filters alignments.
    :param alignment: alignment to filter.
    :param mismatches: maximum number of mismatches allowed.
    :param min_length: minimum allowed alignment length.
    :param max_length: maximum allowed alignment length.
    :param max_nucA: maximum number of A nucleotides allowed at the end of the read.
    :param unique: if true, only unambiguously aligned reads are kept.
    '''
    filtered_alignment, removed_groups, removed_alg = lib.reader.filter_alignment(alignment, mismatches = mismatches, min_length = min_length, max_length = max_length, max_nucA = max_nucA)

    print '[+] Filtered alignments'
    print '\t[i] Initial groups: %s' % format(len(alignment), ',d')
    print '\t[i] Left groups: %s' % format(len(filtered_alignment), ',d')
    print '\t[i] Removed groups %s' % format(removed_groups, ',d')
    print '\t[i] Removed alignments: %s' % format(removed_alg, ',d')

    return filtered_alignment, removed_groups, removed_alg

def check_alignment(alignment_reader, selection = -1, should_filter = False) :
    reference = get_reference() 
    alignment = alignment_reader(selection = selection)
    if should_filter :
        alignment, removed_groups, removed_alg = filter_alignment(alignment)
    lib.reader.check_alignment(reference, alignment)

def create_coverage_records(kernel = None, assignment_kernel = None, output = None, selection = -1, mismatches = 2, min_length = 22, max_length = 32, unique = False) :
    input = [get_ribosome_alignment, get_mrna_alignment]
    if kernel is None :
        kernel = [lib.reader.ribosome_coverage_kernel, lib.reader.mrna_coverage_kernel]
    if assignment_kernel is None :
        assignment_kernel = [lib.reader.read_start_kernel, lib.reader.read_start_kernel]
    if output is None :
        output = ['../results/rib_coverage.out', '../results/mrna_coverage.out']

    reference = get_reference()
    records = get_records()
    genes = lib.reader.group_gff_records_by_name(records)
    genes = lib.reader.unify_gff_record_groups(genes)
    print '[+] Grouped features (CDS)'

    for i in range(len(input)) :
        print '[i] Processing: %s' % output[i]
        alignment_reader = input[i]
        alignment = alignment_reader(selection = selection)
        
        filtered_alignment, removed_groups, removed_alg = filter_alignment(alignment, mismatches = mismatches, min_length = min_length, max_length = max_length, unique = unique)
        positions = lib.reader.map_alignment(reference, filtered_alignment)
        print '\t[+] Mapped reads to positions.'

        positions = lib.reader.cover_positions(genes, positions, assignment_kernel[i], start_prefix = 16, end_prefix = 0)
        print '\t[+] Covered alignments with features.'

        records = lib.reader.convert_to_mrna_array(reference, genes, positions, kernel[i], start_prefix = 16, end_prefix = 0, assignment_kernel = assignment_kernel[i])
        print '\t[+] Created records.'

        lib.writer.write_pickle(records, output[i])

def create_read_start_records(output = None, selection = -1, mismatches = 2, min_length = 22, max_length = 32, unique = False) :
    input = [get_ribosome_alignment, get_mrna_alignment]
    kernel = [lib.reader.read_start_kernel, lib.reader.read_start_kernel]
    if output is None :
        output = ['../results/rib_read_start.out', '../results/mrna_read_start.out']

    reference = get_reference()
    records = get_records()
    genes = lib.reader.group_gff_records_by_name(records)
    genes = lib.reader.unify_gff_record_groups(genes)
    print '[+] Grouped features (CDS, UTR3, UTR5)'

    for i in range(len(input)) :
        print '[i] Processing: %s' % output[i]
        alignment_reader = input[i]
        alignment = alignment_reader(selection = selection)

        filtered_alignment, removed_groups, removed_alg = filter_alignment(alignment, mismatches = mismatches, min_length = min_length, max_length = max_length, unique = unique)
        positions = lib.reader.map_alignment(reference, filtered_alignment)
        print '\t[+] Mapped reads to positions.'

        positions = lib.reader.cover_positions(genes, positions, kernel[i], start_prefix = 16, end_prefix = 14)
        print '\t[+] Covered alignments with features.'

        records = lib.reader.convert_to_mrna_array(reference, genes, positions, kernel[i], start_prefix = 16, end_prefix = 14)
        print '\t[+] Created records.'

        lib.writer.write_pickle(records, output[i])

def process_alignments() :
    print '[i] Updating alignments (mRNA and Rib, replicates 1 and 2)'
    update_alignments()
    print '\n',
    
    print '[i] mRNA alignment (replicates 1 and 2)'
    check_alignment(get_mrna_alignment)
    print '\n',

    print '[i] Rib alignment (replicates 1 and 2)'
    check_alignment(get_ribosome_alignment)
    print '\n',

def create_records() :
    create_read_start_records(output = ['../results/rib_read_start_1.out', '../results/mrna_read_start_1.out'], selection = 0)
    create_read_start_records(output = ['../results/rib_read_start_2.out', '../results/mrna_read_start_2.out'], selection = 1)

def create_profiles_records() :
    create_coverage_records(output = ['../results/rib_coverage_1.out', '../results/mrna_coverage_1.out'], selection = 0)
    create_coverage_records(output = ['../results/rib_coverage_2.out', '../results/mrna_coverage_2.out'], selection = 1)
    
    create_coverage_records(output = ['../results/rib_coverage_unique_1.out', '../results/mrna_coverage_unique_1.out'], selection = 0, mismatches = 1, unique = True)
    create_coverage_records(output = ['../results/rib_coverage_unique_2.out', '../results/mrna_coverage_unique_2.out'], selection = 1, mismatches = 1, unique = True)

def main() :
    #process_alignments()
    #create_records()
    create_profiles_records()

if __name__ == "__main__":
    main()
