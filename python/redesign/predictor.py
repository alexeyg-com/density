# Routines for redesigning genes.
# Alexey Gritsenko
# June 2014

import gillespie
import lib.simulator
import lib.reader
from lib.SimpleNamespace import SimpleNamespace

import numpy as np

class OptimizedSequence(object) :
    '''
    A class for holding sequences that are being optimized.
    '''

    _DEFAULT_WINDOW = 0
    '''
    Default recompute window for the sequence.
    '''
    
    Eps = 1e-3
    '''
    Simulation precision. This setting ultimately determines the number of Monte-Carlo steps, for which the system is simulated.
    '''

    L = 10
    '''
    Ribosome length in codons.
    '''

    def __init__(self, sequence, feature_computer = None, rate_function = None) :
        '''
        Constructor function.
        :param sequence : sequence to be used as a start for optimization.
        :param feature_computer : feature computer for the sequence. If None, the default one is loaded.
        :param rate_function : rate function to be used in optimization. If None, the default one is loaded.
        '''
        self.window = self._DEFAULT_WINDOW
        self.sequence = sequence
        self.feature_computer = feature_computer
        self.rate_function = rate_function
        self._precompute()

    def get_production_rate(self, initiation, srand = None) :
        '''
        Predict protein production rate.
        :param initiation : translation initiation rate to be used.
        :param srand: seed of the random number generator to be used in simulation.
        '''
        res = self._simulate(initiation, srand = srand)
        return res.J
   
    def _simulate(self, initiation, eps = None, burn = 1000, total_burn_steps = 10000000, increment = 1000000, total_steps = 10000000, srand = None) :
        '''
        Simulates the movement of ribosomes along the mRNA molecule. Used to obtain a steady-state occupancy (density) profile.
        :param initiation: initiation rate to be used for the simulation.
        :param eps: Epsilon value used to call estimates reliable.
        :param burn: number of translation termination events that are not recorded (burn-in steps that are not recorded)
        :param total_burn_steps: maximum allowed duration of the burn phase.
        :param increment: the increment of the number of simulation steps.
        :param total_steps: the maximum allowed number of steps.
        :param srand: seed of the random number generator to be used in simulation.
        '''
        if eps is None :
            eps = self.Eps
        if srand is not None :
            gillespie.srand(srand)
        J, Q, density, iter, success = gillespie.simulate_nobump(initiation, self._rate, self.L, [], eps, burn, total_burn_steps, increment, total_steps)
        res = SimpleNamespace()
        res.J, res.Q, res.density, res.success, res.iter = J, Q, density, success, iter
        self.solution = res
        return res

    def _precompute(self) :
        '''
        Used to precompute codon translation rates (uses the rate function).
        '''
        self.length_codons = len(self.sequence) / 3
        self._features = []
        for i in range(self.length_codons) :
            feat = self.feature_computer.compute(self.sequence, i)
            self._features.append(feat)
            
        self._compute_rates()
    
    def _compute_rates(self) :
        '''
        (Re-)compute codon translation rates based on the pre-computed sequence features.
        '''
        rate = np.zeros((self.length_codons,))
        for i in range(self.length_codons) :
            rate[i] = self.rate_function.evaluate(self._features[i])
        
        self._rate = rate

    def _recompute_rates(self, pos) :
        '''
        Recompute rates after a codon at position pos was changed.
        :param pos: position at which the codon was changed.
        '''
        start, stop = max(pos - self.window, 0), min(pos + self.window, self.length_codons)
        for i in range(stat, stop + 1) :
            feat = self.feature_computer.compute(self.sequence, i)
            self._features[i] = feat
            self._rate[i] = self.rate_function.evaluate(self._features[i])

def predict_J(init, seq) :
    '''
    Predict protein production rate for a gene given its CDS and initiation rate.
    :param init : translation initiation rate.
    :param seq : coding sequence.
    '''
    pass
