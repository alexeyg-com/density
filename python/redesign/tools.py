# A module containing additional tools used within the optimizer module.
# Alexey Gritsenko
# June 2014

import create_arrays
import lib.reader
import lib.forest

import Bio

def feature_cmp(a, b) :
    '''
    A feature comparator used to sort features prior to merging them.
    '''
    if a.strand != b.strand :
        raise ValueError()

    strand = a.strand
    if strand == '+' :
        if a.start < b.start :
            return -1
        elif a.start > b.start :
            return +1
        elif a.stop > b.stop :
            return -1
        elif a.stop < b.stop :
            return +1
    else :
        if a.stop > b.stop : 
            return -1
        elif a.stop < b.stop :
            return +1
        elif a.start < b.start :
            return -1
        elif a.start > b.start :
            return +1
    return 0

def obtain_sequences_from_records(records, references) :
    ''' 
    Extract nucleotide CDS sequences from gene records.
    :param records : a dictionary of GFF record groups.
    :param references : dictionary of reference sequences.
    ''' 
    missing_reference = set()
    constructed = {}
    genes = records.keys()
    for gene in genes :
        features = records[gene].cds
        features = [feature for feature in features if feature.type == 'CDS']
        features.sort(feature_cmp)
        record = features[0]
        ref_name = record.reference
        if not references.has_key(ref_name) :
            missing_reference.add(ref_name)
            continue
        
        gene_seq = ''
        ref_seq = references[ref_name].seq
        for feature in features :
            if feature.type == 'CDS' :
                sub_seq = ref_seq[feature.start : feature.stop]
                if record.strand == '-' :
                    sub_seq = Bio.Seq.Seq(sub_seq).reverse_complement().tostring()
                gene_seq += sub_seq
        
        constructed[gene] = gene_seq
    print 'Missing references: %s' % (', '.join(missing_reference))
    return constructed

def obtain_gene_sequences() :
    '''
    A shortcut for obtaining a dictionary with gene sequences.
    '''
    references = create_arrays.get_reference()
    records = create_arrays.get_records()
    records = lib.reader.group_gff_records_by_name(records)
    sequences = obtain_sequences_from_records(records, references)
    sequences = lib.forest.filter_dubious(sequences)
    sequences = lib.forest.filter_mitochondrial(sequences)
    return sequences

