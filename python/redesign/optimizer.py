# A module for optimizing gene coding sequences
# Alexey Gritsenko
# June 2014

import inspyred
import numpy as np
import random
import copy
import sys
import scipy.spatial.distance
import predictor
import lib.tools
from lib.SimpleNamespace import SimpleNamespace

def parallel_simulate(param) :
    import predictor
    import lib.simulator
    
    seq, initiation, beta, srand = param
    feature_computer = lib.simulator.FeatureComputer()
    rate_function = lib.simulator.RateFunction(feature_computer.dims)
    rate_function.beta[:] = beta[:]
    opt = predictor.OptimizedSequence(seq, feature_computer, rate_function)
    result = opt.get_production_rate(initiation, srand = srand)

    return result

class CodonPairDistance(object) :
    '''
    A class for implementing the computation of codon pair frequency distance between a sequence and a set of sequences.
    '''

    def __init__(self, cloud) :
        '''
        Class constructor.
        :param cloud: a list of sequences the distance to which should be determined.
        '''
        self._compute_mean(cloud)

    def _compute_mean(self, cloud) :
        '''
        A routine for computing the average of a "cloud" (i.e. a list of sequences).
        :param cloud: a list of sequence to compute the mean/center of.
        '''
        array = []
        for seq in cloud :
            freq = CodonPairDistance.compute_frequency(seq)
            array.append(freq)
        array = np.array(array)

        average = np.zeros((64 ** 2,))
        variance = np.zeros((64 ** 2,))
        for i in range(64 ** 2) :
            column = array[:, i]
            ind = np.logical_not(np.isnan(column))
            average[i] = np.mean(column[ind])
            variance[i] = np.var(column[ind])
        self._average = average
        self._inverse_variance = 1.0 / variance
        usable_ind = np.logical_not(np.logical_or(np.isnan(self._inverse_variance), np.isinf(self._inverse_variance)))
        self._average = average
        self._usable_ind = usable_ind
    
    @staticmethod
    def compute_frequency(seq) :
        '''
        Compute codon frequency array for a sequence.
        :param seq: sequence for which the codon frequency profile should be determined.
        '''
        count = {}
        for codon1 in lib.tools.codon2int :
            amino1 = lib.tools.codon2amino[codon1]
            for codon2 in lib.tools.codon2int :
                amino2 = lib.tools.codon2amino[codon2]
                count[codon1 + codon2] = 0
                count[amino1 + amino2] = 0

        length = len(seq) / 3
        for i in range(length - 1) :
            codon1 = seq[3 * i : 3 * i + 3]
            codon2 = seq[3 * i + 3 : 3 * i + 3 + 3]
            amino1 = lib.tools.codon2amino[codon1]
            amino2 = lib.tools.codon2amino[codon2]
            count[codon1 + codon2] = count[codon1 + codon2] + 1
            count[amino1 + amino2] = count[amino1 + amino2] + 1

        freq = np.zeros((64 ** 2,))
        for codon1 in lib.tools.codon2int :
            amino1 = lib.tools.codon2amino[codon1]
            for codon2 in lib.tools.codon2int :
                amino2 = lib.tools.codon2amino[codon2]
                codon_freq = count[codon1 + codon2] / float(count[amino1 + amino2]) if count[amino1 + amino2] > 0 else float('Nan')
                ind1 = lib.tools.codon2int[codon1]
                ind2 = lib.tools.codon2int[codon2]
                ind = ind1 * 64 + ind2
                freq[ind] = codon_freq
        
        return freq

    def compute_distance(self, seq) :
        '''
        Compute distance between sequence and the cloud.
        :param seq: sequence to compute the distance to.
        '''
        freq = CodonPairDistance.compute_frequency(seq)
        ind = np.logical_and(np.logical_not(np.isnan(freq)), self._usable_ind)
        dist = scipy.spatial.distance.mahalanobis(freq[ind], self._average[ind], np.diag(self._inverse_variance[ind]))
        return dist

class CodonDistance(object) :
    '''
    A class for implementing the computation of codon frequency distance between a sequence and a set of sequences.
    '''

    def __init__(self, cloud) :
        '''
        Class constructor.
        :param cloud: a list of sequences the distance to which should be determined.
        '''
        self._compute_mean(cloud)

    def _compute_mean(self, cloud) :
        '''
        A routine for computing the average of a "cloud" (i.e. a list of sequences).
        :param cloud: a list of sequence to compute the mean/center of.
        '''
        array = []
        for seq in cloud :
            freq = CodonDistance.compute_frequency(seq)
            array.append(freq)
        array = np.array(array)
        
        average = np.zeros((64,))
        variance = np.zeros((64,))
        for i in range(64) :
            column = array[:, i]
            ind = np.logical_not(np.isnan(column))
            average[i] = np.mean(column[ind])
            variance[i] = np.var(column[ind])
        self._average = average
        self._inverse_variance = 1.0 / variance
        usable_ind = np.logical_not(np.logical_or(np.isnan(self._inverse_variance), np.isinf(self._inverse_variance)))
        self._usable_ind = usable_ind

    @staticmethod
    def compute_frequency(seq) :
        '''
        Compute codon frequency array for a sequence.
        :param seq: sequence for which the codon frequency profile should be determined.
        '''
        count = {}
        for codon in lib.tools.codon2int :
            amino = lib.tools.codon2amino[codon]
            count[codon] = 0
            count[amino] = 0

        length = len(seq) / 3
        for i in range(length) :
            codon = seq[3 * i : 3 * i + 3]
            amino = lib.tools.codon2amino[codon]
            count[codon] = count[codon] + 1
            count[amino] = count[amino] + 1

        freq = np.zeros((64,))
        for codon in lib.tools.codon2int :
            amino = lib.tools.codon2amino[codon]
            codon_freq = count[codon] / float(count[amino]) if count[amino] > 0 else float('Nan')
            freq[lib.tools.codon2int[codon]] = codon_freq
        
        return freq

    def compute_distance(self, seq) :
        '''
        Compute distance between sequence and the cloud.
        :param seq: sequence to compute the distance to.
        '''
        freq = CodonDistance.compute_frequency(seq)
        ind = np.logical_and(np.logical_not(np.isnan(freq)), self._usable_ind)
        dist = scipy.spatial.distance.mahalanobis(freq[ind], self._average[ind], np.diag(self._inverse_variance[ind]))
        return dist

class OptimizerNSGA(object) :
    '''
    Class for the NSGA-II algorithm for optimizing gene CDSes.
    '''

    def __init__(self, sequence, initiation, cloud = None, feature_computer = None, rate_function = None, n_repetitions = 10, random_seed = 73575339) :
        '''
        Optimizer constructor.
        :param sequence: sequence to be optimized.
        :param initiation : translation initiation rate to be used.
        :param cloud : the cloud of points to which the distance should be computed as additional objective(s). Default: None.
        :param feature_computer : feature computer for the sequence. If None, the default one is loaded.
        :param rate_function : rate function to be used in optimization. If None, the default one is loaded.
        :param n_repetitions : number of times to repeat the simulation with different random seeds.
        :param random_seed: random see for the optimization.
        '''
        if feature_computer is None :
            feature_computer = lib.simulator.FeatureComputer()
        if rate_function is None :
            results_filename = '../results/paper/result-rate_fitting-folds_all.out'
            res = lib.reader.read_pickle(results_filename)
            rate_function = lib.simulator.RateFunction(feature_computer.dims)
            rate_function.beta[:] = res.vector[:]
        self.feature_computer = feature_computer
        self.rate_function = rate_function

        self.random = random.Random()
        ea = inspyred.ec.emo.NSGA2(self.random)
        ea.variator = [inspyred.ec.variators.uniform_crossover, self._reset_mutator]
        ea.terminator = inspyred.ec.terminators.generation_termination
        ea.observer = self._observer
        self.ea = ea
        self.sequence = sequence
        self.initiation = initiation
        self.random_seed = random_seed
        self.n_repetitions = n_repetitions
        self._maximize = True
        self._client = None
        self._precompute(cloud)

    def _initialize_random(self) :
        '''
        Initialize necessary randomization stuff.
        '''
        if self.random_seed is not None :
            self.random.seed(self.random_seed)
        self._simulation_seeds = np.zeros((self.n_repetitions,), dtype = np.int)
        for i in range(self.n_repetitions) :
            self._simulation_seeds[i] = self.random.randint(0, 2 ** 16)

    def optimize(self, pop_size, max_generations, output_filename = None, client = None, retries = 50) :
        '''
        Optimize the gene sequence.
        :param pop_size : population size to be used in the optimization.
        :param max_generations : maximum number of generations to optimize for.
        :param output_filename : a file to which intermediate results should be written. Default: None.
        :param client : ipython client that should be used to parallelize objective function evaluation. Default: None.
        :param retries : number of retries for the parallel evaluation.
        '''
        self._initialize_random()
        self._generated_initial = 0
        self.history = []
        self._output_filename = output_filename
        self._client = client
        self._view = client.load_balanced_view() if client is not None else None
        if self._view is not None :
            self._view.retries = retries
        population = self.ea.evolve(generator = self._generator,
                                    evaluator = self._evaluator,
                                    pop_size = pop_size,
                                    maximize = self._maximize,
                                    bounder = self._bounder,
                                    max_generations = max_generations)
        
        return self._construct_result(population)
    
    def _construct_naive_sequence(self) :
        '''
        Construct naively "optimal" sequence by using fastest codons only.
        '''
        int2codon = {}
        for codon, codon_int in lib.tools.codon2int.iteritems() :
            int2codon[codon_int] = codon

        codon2rate = {}
        for i in range(64) :
            codon = int2codon[i]
            rate = self.rate_function.sigmoid(self.rate_function.beta[i])
            codon2rate[codon] = rate

        sequence = np.zeros((self.n_codons,), dtype = np.int)
        for i in range(self.n_codons) :
            synonymous = self._synonymous[i]
            max_j, max_rate = 0, -1
            for j in range(self._n_synonymous[i]) :
                rate = codon2rate[synonymous[j]]
                if rate > max_rate :
                    max_j, max_rate = j, rate
            sequence[i] = int(max_j)
        return sequence

    def predict_production_rate_naive(self) :
        '''
        Predict protein production rate (and other objectives) for a naively "optimized" sequence
        obtained by using fastest codon for each position.
        '''
        sequence = self._construct_naive_sequence()
        self._initialize_random()
        eval = self._evaluator([sequence], {})
        eval = eval[0]
        print '[i] Naive optimization: ',
        print eval

    def _construct_result(self, population) :
        '''
        A function for constructing the optimization result structure.
        :param population : GA population after the optimization. 
        '''
        result = SimpleNamespace()
        result.initiation = self.initiation
        result.srand = self.random_seed
        result.n_repetitions = self.n_repetitions
        result.original_sequence = self.sequence
        result.simulation_seeds = self._simulation_seeds
        
        ga_population = []
        for candidate in population :
            wrapped = SimpleNamespace()
            wrapped.fitness = candidate.fitness
            wrapped.int_sequence = candidate.candidate
            wrapped.sequence = self._construct_sequence(candidate.candidate)
            wrapped.birthdate = candidate.birthdate
            ga_population.append(wrapped)
        
        ga_archive = []
        for candidate in self.ea.archive :
            wrapped = SimpleNamespace()
            wrapped.fitness = candidate.fitness
            wrapped.int_sequence = candidate.candidate
            wrapped.sequence = self._construct_sequence(candidate.candidate)
            wrapped.birthdate = candidate.birthdate
            ga_archive.append(wrapped)
        
        result.ga_population = ga_population
        result.ga_archive = ga_archive
        return result

    def _precompute(self, cloud) :
        '''
        A routine for precomputing some of the necessary internal structures.
        :param cloud : the cloud of points to which the distance should be computed as additional objective(s). Default: None.
        '''
        self.n_codons = len(self.sequence) / 3
        aminos = np.chararray((self.n_codons,))
        synonymous = np.zeros((self.n_codons,), dtype = np.object)
        n_synonymous = np.zeros((self.n_codons,), dtype = np.int)
        for i in range(self.n_codons) :
            codon = self.sequence[3 * i : 3 * i + 3]
            amino = lib.tools.codon2amino[codon]
            aminos[i] = amino
            synonymous[i] = np.array(lib.tools.amino2codon[amino], copy = True)
            n_synonymous[i] = len(synonymous[i])

        sequence_int = np.zeros((self.n_codons,), dtype = np.int)
        for i in range(self.n_codons) :
            codon = self.sequence[3 * i : 3 * i + 3]
            indices = np.where(synonymous[i] == codon)
            index = indices[0][0]
            sequence_int[i] = index
        self._sequence_int = sequence_int

        self._aminos = aminos
        self._synonymous = synonymous
        self._n_synonymous = n_synonymous
        
        self._codon_distance = None
        self._codon_pair_distance = None
        if cloud is not None :
            self._codon_distance = CodonDistance(cloud)
            self._codon_pair_distance = CodonPairDistance(cloud)
    
    def _construct_sequence(self, solution) :
        '''
        Construct character (nucleotide) sequence out of an integer sequence.
        :param solution : solution that needs to be converted into a character representation.
        '''
        codons = []
        for i in range(self.n_codons) :
            codons.append(self._synonymous[i][solution[i]])
        seq = ''.join(codons)
        return seq

    def _evaluator(self, candidates, args) :
        '''
        Evaluator function.
        :param candidates: a list of candidates.
        :param args: additional arguments.
        '''
        seqs = []
        for solution in candidates :
            seq = self._construct_sequence(solution)
            seqs.append(seq)
        
        fitness = []
        if self._client is None :
            for sequence in seqs :
                opt = predictor.OptimizedSequence(sequence, self.feature_computer, self.rate_function)
                repeated_fits = np.zeros((self.n_repetitions,))
                for i in range(self.n_repetitions) :
                    repeated_fits[i] = opt.get_production_rate(self.initiation, srand = self._simulation_seeds[i])
               
                fit = [np.min(repeated_fits)]
                if self._codon_distance is not None :
                    fit.append(-self._codon_distance.compute_distance(sequence))
                if self._codon_pair_distance is not None :
                    fit.append(-self._codon_pair_distance.compute_distance(sequence))
                fit = inspyred.ec.emo.Pareto(fit)
                fitness.append(fit)
        else :
            params = []
            n_sequences = len(seqs)
            for i in range(n_sequences) :
                sequence = seqs[i]
                for j in range(self.n_repetitions) :
                    param = (sequence, self.initiation, self.rate_function.beta, self._simulation_seeds[j])
                    params.append(param)
            results = self._view.map(parallel_simulate, params, block = True)
            k = 0
            for i in range(n_sequences) :
                repeated_fits = []
                for j in range(self.n_repetitions) :
                    result = results[k]
                    repeated_fits.append(result)
                    k += 1
               
                sequence = seqs[i]
                fit = [np.min(repeated_fits)]
                if self._codon_distance is not None :
                    fit.append(-self._codon_distance.compute_distance(sequence))
                if self._codon_pair_distance is not None :
                    fit.append(-self._codon_pair_distance.compute_distance(sequence))
                fit = inspyred.ec.emo.Pareto(fit)
                fitness.append(fit)

        return fitness

    def _generator(self, random, args) :
        '''
        Generator function used to generate new solutions.
        :param random: random number generations.
        :param args: additional arguments.
        '''
        if self._generated_initial == 0 :
            solution = np.copy(self._sequence_int)
            self._generated_initial += 1
        elif self._generated_initial == 1 :
            solution = self._construct_naive_sequence()
            self._generated_initial += 1
        else :
            solution = np.zeros((self.n_codons,), dtype = np.int)
            for i in range(self.n_codons) :
                solution[i] = int(random.randint(0, self._n_synonymous[i] - 1))
        return solution.tolist()

    def _bounder(self, candidate, args) :
        '''
        Bounding function for candidate solutions.
        :param candidate: candidate solution to be bound.
        :param args: additional arguments.
        '''
        for i in range(self.n_codons) :
            candidate[i] = int(max(0, min(candidate[i], self._n_synonymous[i] - 1)))
        return candidate
    
    def _reset_mutator(self, random, candidates, args) :
        '''
        The mutation moves through a candidate solution and, with rate equal to the *mutation_rate*, randomly chooses
        a value from the set of allowed values to be used in that location.
        Note that this value may be the same as the original value.
        :param random: the random number generator object
        :param candidates: the candidate solution
        :param args: a dictionary of keyword arguments
        '''
        rate = args.setdefault('mutation_rate', 0.1)
        mutants = []
        for candidate in candidates :
            mutant = copy.copy(candidate)
            for i in range(self.n_codons) :
                if random.random() < rate :
                    mutant[i] = int(random.randint(0, self._n_synonymous[i] - 1))
            mutants.append(mutant)
        return mutants
    
    def _observer(self, population, num_generations, num_evaluations, args) :
        '''
        Observer function (called at every iteration of the GA).
        :param population: population of the genetic algorithm.
        :param num_generations: number of generations passed after optimization.
        :param num_evaluation: number of evaluations passed after optimization.
        :param args: additional arguments.
        '''
        self.history.append(copy.deepcopy(population))
        if self._output_filename is not None :
            data = SimpleNamespace()
            data.population = population
            data.history = self.history
            data.num_generations = num_generations
            data.num_evaluation = num_evaluations
            lib.writer.write_pickle(data, self._output_filename)

        print '[i] Generation %d (%d evaluations) : ' % (num_generations, num_evaluations),
        fits = []
        for ind in population :
            fits.append(np.array(ind.fitness))
        n_objectives = len(fits[0])
        for i in range(n_objectives) :
            fits = sorted(fits, key = lambda x : x[i], reverse = True)
            print fits[0],
        print ''

