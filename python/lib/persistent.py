import numpy as np
import simulator
from reader import read_pickle

def initialize_rate_functions() :
    '''
    Initializes a rate function for each gene in the forest.
    '''
    global forest
    global rate_functions

    rate_functions = {}
    genes = forest.keys()
    for gene in genes :
        rate_functions[gene] = simulator.RateFunction(dims = feature_computer.dims)

def initialize_simulators() :
    '''
    Initialized a simulator for each gene in the forest.
    '''
    global forest
    global feature_computer
    global rate_functions
    global simulators

    simulators = {}
    genes = forest.keys()
    for gene in genes :
        simulators[gene] = simulator.MonteCarloSimulator(forest[gene], 1.0, rate_functions[gene], feature_computer)

def initialize_persistent(forest_filename = './forest.out', type = 'single') :
   '''
   Initialize the persistent simulator module.
   '''
   global forest
   global feature_computer
   global rate_function

   forest = read_pickle(forest_filename)
   feature_computer = simulator.FeatureComputer(type = type)
   initialize_rate_functions()
   initialize_simulators()

def simulate_gene(gene, initiation, rates, srand) :
    '''
    Constructs an obtained solution by returning a simulated profile along with its evaluation.
    :param gene: name of the gene to be evaluated.
    :param initiation: initiation rate of the gene.
    :param rates: vector beta of the rate function.
    :param srand: random seed for the Monte Carlo simulation.
    '''
    global forest
    global simulators
    global rate_functions

    simulator, rate_function, tree = simulators[gene], rate_functions[gene], forest[gene]
    rate_function.beta[:] = rates
    simulator.initiation = initiation
    simulator.compute_rates()
    result = simulator.simulate(srand = srand)
    coef = tree.evaluate_scalefree(result.density)
    return (coef, result.density, result.J, result.Q)

def evaluate_fit(gene, initiation, rates, srand) :
    '''
    Evaluates a fit (scale-free evaluation) of a simulated gene.
    :param gene: name of the gene to be evaluated.
    :param initiation: initiation rate of the gene.
    :param rates: vector beta of the rate function.
    :param srand: random seed for the Monte Carlo simulation.
    '''
    global forest
    global simulators
    global rate_functions

    simulator, rate_function, tree = simulators[gene], rate_functions[gene], forest[gene]
    rate_function.beta[:] = rates
    simulator.initiation = initiation
    simulator.compute_rates()
    result = simulator.simulate(srand = srand)
    coef = tree.evaluate_scalefree(result.density)
    return coef

initialize_persistent()
