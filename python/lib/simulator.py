'''
This module contains simulation routines.

Alexey Gritsenko
31-10-2013
'''

import numpy as np
import time

import gillespie

import tools
from SimpleNamespace import SimpleNamespace

class RateFunction(object) :
    '''
    A linear sigmoid rate function class to be used in optimization and ribosome movement simulation.
    '''
    
    beta = np.array([])
    '''
    A vector defining the linear sigmoid predictor of codon translation rates.
    '''

    def __init__(self, dims) :
        '''
        Rate function constructor.
        :param dims: the length of the feature vector this rate function works with.
        '''
        self.beta = np.zeros((dims, ))

    def evaluate(self, vector) :
        '''
        Calculate rate function value for the provided feature vector (not sequence).
        I.e. compute the sigmoid of a vector dot product.
        :param vector: the feature vector to compute the rate function for.
        '''
        return RateFunction.sigmoid(np.dot(self.beta, vector))

    @staticmethod
    def logit(x) :
        '''
        Computes the logit function (reverse sigmoid):
            logit = log(x) - log(1.0 - x)
        '''
        return np.log(x) - np.log(1.0 - x)

    @staticmethod
    def sigmoid(x) :
        '''
        Computes the sigmoid function:
            sigmoid = 1.0 / (1.0 + exp(-x))
        :param x: argument of the sigmoid function.
        '''
        return 1.0 / (1.0 + np.exp(-x))

class FeatureComputer(object) :
    '''
    A class used for computing features for predicting codon translation rates.
    '''

    dims = 0
    '''
    Number of dimensions (length) of the feature vector.
    '''
    
    type = ''
    '''
    Type of the feature computer (i.e. what features does it compute?)
    '''

    def __init__(self, type = 'single', dims = None) :
        '''
        FeatureComputer constructor.
        :param type: type of the feature computer. Can be 'single' (default).
        :param dims: dimensionality of the feature vector.
        '''
        if isinstance(type, int) :
            dims = type
            type = 'position'

        type = type.lower()
        if type == 'single' :
            self.dims = 4 * 4 * 4 if dims is None else dims
        elif type == 'position' :
            self.dims = dims
        else :
            raise ValueError('Invalid type of FeatureComputer: %s' % type)
        self.type = type

    def compute(self, record, position) :
        '''
        Returns a feature vector computed for a particular position in the sequence.
        :param record: sequence to compute the feature vector on or a tree record.
        :param position: (codon) position for which the feature vector should be computed.
        '''
        if isinstance(record, basestring) :
            seq = record
            start_prefix = 0
        else :
            seq = record.seq
            start_prefix = record.start_prefix
        length = len(seq) - start_prefix
        length_codons = length / 3
        
        vector = np.zeros((self.dims,))
        if self.type == 'single' :
            start = start_prefix + position * 3
            codon = seq[start:start + 3].upper()
            codon_int = tools.codon2int[codon]
            vector[codon_int] = 1.0
        else :
            raise ValueError('FeatureComputer type not implemented: %s' % self.type)
        
        return vector

class SimulatorBase(object) :
    '''
    A base class for ribosome movement simulators.
    '''

    L = 10
    '''
    Ribosome length in codons.
    '''

    Eps = 1e-6
    '''
    Simulation precision.
    '''

    def __init__(self, record, initiation, rate_function, feature_computer) :
        '''
        Class constructor.
        :param record: gene record.
        :param double initiation: initiation rate for the mRNA.
        :param rate_function: an instance of the RateFunction class used to compute (predict) codon translation rates.
        :param feature_computer: an instance of the FeatureComputer class used to compute feature for predicting the translation rates.
        '''
        self.record = record
        self.initiation = initiation
        self.rate_function = rate_function
        self.feature_computer = feature_computer

        self._precompute()

    def _precompute(self) :
        '''
        Used to precompute codon translation rates (uses the rate function).
        '''
        self.length_codons = self.record.length_codons
        self._features = []
        for i in range(self.length_codons) :
            feat = self.feature_computer.compute(self.record, i)
            self._features.append(feat)
        self.compute_rates()

    def compute_rates(self) :
        '''
        (Re-)compute codon translation rates based on the pre-computed sequence features.
        '''
        rate = np.zeros((self.length_codons,))
        for i in range(self.length_codons) :
            rate[i] = self.rate_function.evaluate(self._features[i])
        
        self._rate = rate

class MonteCarloSimulator(SimulatorBase) :
    '''
    A simple gene simulator that performs Monte Carlo simulations to compute occupation profiles.
    '''
    
    Eps = 1e-3
    '''
    Simulation precision. This setting ultimately determines the number of Monte-Carlo steps, for which the system is simulated.
    '''

    def __init__(self, record, initiation, rate_function, feature_computer) :
        '''
        Class constructor.
        :param record: tree record.
        :param double initiation: initiation rate for the mRNA.
        :param rate_function: a function used to compute (predict) codon translation rates.
        :param feature_computer: an instance of the FeatureComputer class used to compute feature for predicting the translation rates.
        '''
        SimulatorBase.__init__(self, record, initiation, rate_function, feature_computer)
        self._construct_segment_list()

    def _construct_segment_list(self) :
        '''
        Constructs a segment list to be fed to the MC simulators (used as a stopping criterion).
        '''
        segment_list = []
        for node in self.record.tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            segment_list.append((left, right))
        self._segment_list = segment_list
   
    def simulate(self, eps = None, burn = 1000, total_burn_steps = 10000000, increment = 1000000, total_steps = 10000000, srand = None, verbose = False) :
        '''
        Simulates the movement of ribosomes along the mRNA molecule. Used to obtain a steady-state occupancy (density) profile.
        :param eps: Epsilon value used to call estimates reliable.
        :param burn: number of translation termination events that are not recorded (burn-in steps that are not recorded)
        :param total_burn_steps: maximum allowed duration of the burn phase.
        :param increment: the increment of the number of simulation steps.
        :param total_steps: the maximum allowed number of steps.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: a boolean flag determining whether verbose output should be produced.
        '''
        if eps is None :
            eps = self.Eps
        start_time = time.time()
        if srand is not None :
            gillespie.srand(srand)
        J, Q, density, iter, success = gillespie.simulate_nobump(self.initiation, self._rate, self.L, self._segment_list, eps, burn, total_burn_steps, increment, total_steps)
        time_elapsed = time.time() - start_time
        if verbose :
            print 'Done : %15.10g (%4f sec) | iterations = %d, success = %d' % (J, time_elapsed, iter, success)
        res = SimpleNamespace()
        res.J, res.Q, res.density, res.success, res.iter = J, Q, density, success, iter
        self.solution = res
        return res

