'''
Contains routines for data input and processing.

Alexey Gritsenko
31-10-2013
'''

from collections import namedtuple
from sets import Set
from SimpleNamespace import *
import types
import re
import os
import Bio.SeqIO
import Bio.Seq
import datetime
import tools
import cPickle
import marshal
import numpy as np
import warnings

def make_feature_record(seq, array, array_overlapping, array_ambiguous, feature_length, overlapping, ambiguous, total) :
    rec = SimpleNamespace()
    rec.seq = seq
    rec.array = array
    rec.array_overlapping = array_overlapping
    rec.array_ambiguous = array_ambiguous
    rec.feature_length = feature_length
    rec.overlapping = overlapping
    rec.ambiguous = ambiguous
    rec.total = total
    
    return rec

def read_list(filename) :
    '''
    Reads a list of lines (e.g. gene names) from a file.
    :param filename: name of the file to read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    res = []
    for line in lines :
        res.append(line.strip())
    return res

def read_alignment(filename) :
    fin = open(filename, 'rb')

    alignments = []
    group = []
    for line in fin :
        line = line.strip()
        length = len(line)
        # Check if we hit a comment line
        if length > 0 and line[0] == '#' :
            continue
        # We hit a group separator
        if length == 0 :
            group_size = len(group)
            for i in range(group_size) :
                item = group[i].split()
                reference = item[3]
                if reference == 'chrMito' :
                    reference = 'chrmt'
                alg = SimpleNamespace()
                alg.seq = item[0]
                alg.count = int(item[1])
                alg.score = int(item[2])
                alg.reference = reference
                alg.start = int(item[4])
                alg.stop = int(item[5])
                alg.maximum_length = int(item[6])
                alg.ambiguity_length = int(item[7])
                group[i] = alg

            alignment = [group[0].seq, group[0].count, []]
            for item in group :
                alg = SimpleNamespace()
                alg.score = item.score
                alg.reference = item.reference
                alg.start = item.start - 1
                alg.stop = item.stop - 1
                alg.maximum_length = item.maximum_length
                alg.ambiguity_length = item.ambiguity_length
                alignment[2].append(alg)

            alignments.append(alignment)

            group = []
        else :
            group.append(line)
    fin.close()

    return alignments

def changes_cmp(a, b) :
    a = a.release.split('-')
    b = b.release.split('-')
    a = int(a[0])
    b = int(b[0])

    return a - b

def read_changes_file(filename) :
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    res = []
    for line in lines :
        line = line.split()
        date = line[0].split('-')
        date = datetime.date(int(date[0]), int(date[1]), int(date[2]))
        old_seq = line[6]
        new_seq = ''
        if len(line) >= 8 :
            new_seq = line[7]
        elif line[3] == 'Insertion' :
            new_seq = old_seq
            old_seq = ''

        change_rec = SimpleNamespace()
        change_rec.data = date
        change_rec.chromosome = int(line[1])
        change_rec.release = line[2]
        change_rec.type = line[3]
        change_rec.start = int(line[4]) - 1
        change_rec.stop = int(line[5]) - 1
        change_rec.old_seq = old_seq
        change_rec.new_seq = new_seq
        res.append(change_rec)
    return res

def group_changes(changes) :
    changes = changes[:]
    changes.sort(changes_cmp)
    groups = []
    i = 0
    j = 0
    while i < len(changes) :
        while j < len(changes) and changes[i].release == changes[j].release :
            j = j + 1
        groups.append(changes[i:j])
        i = j

    return groups

def compute_alignment_score(seq, ref_seq) :
    score = 0
    len_ref_seq = len(ref_seq)
    for i in range(len_ref_seq) :
        if ref_seq[i] == seq[i] :
            score += 1
    for i in range(len_ref_seq, len(seq)) :
        if seq[i] == 'A' :
            score += 1
    return score

def get_ref_seq_for_alignment(alignment, seqs, diff = 0) :
    if alignment.start < alignment.stop :
        ref_seq = seqs[alignment.reference].seq[alignment.start + diff:alignment.stop + diff + 1]
    else :
        ref_seq = seqs[alignment.reference].seq[alignment.stop + diff:alignment.start + diff + 1]
        ref_seq = Bio.Seq.Seq(ref_seq).reverse_complement().tostring()
    
    return ref_seq

def apply_changes_to_alignment(seqs, changes, groups, start_release) :
    changes = group_changes(changes)
    for group in groups :
        for alignment in group[2] :
            diff = 0
            alignment_reference = tools.chromosome2int(alignment.reference)
            start_pos = min(alignment.start, alignment.stop)
            for change_group in changes :
                for change in change_group :
                    change_release = change.release.split('-')
                    change_release = int(change_release[0])
                    if change_release < start_release :
                        continue
                    if alignment_reference == change.chromosome :
                        if change.type == 'Insertion' :
                            if change.start <= start_pos :
                                diff = diff + len(change.new_seq)
                        elif change.type == 'Deletion' :
                            if change.start <= start_pos :
                                diff = diff + -len(change.old_seq)
                        elif change.type == 'Substitution' :
                            if change.start <= start_pos :
                                diff = diff - len(change.old_seq) + len(change.new_seq)
            
            score = 0
            if seqs.has_key(alignment.reference) :
                ref_seq = get_ref_seq_for_alignment(alignment, seqs, diff)
                alignment.score = compute_alignment_score(group[0], ref_seq)
            alignment.start += diff
            alignment.stop += diff
           
    return groups

def get_gff_attribute(string, attribute) :
    m = re.search(('(^|;)%s=([^;]+)($|;)') % attribute, string)
    found = None
    if m is not None :
        found = m.group(2)
    return found

def read_features(filename) :
    if not isinstance(filename, list) :
        filename = [filename]

    records = []
    for file in filename :
        fin = open(file, 'rb')
        for line in fin :
            line = line.strip()
            length = len(line)
            if length > 0 and line[0] == '#' :
                continue
            if length > 0 :
                item = line.split('\t')
                if len(item) != 9 :
                    continue
                description = item[8]
                reference = item[0]
                if reference == 'chrMito' :
                    reference = 'chrmt'
                record = SimpleNamespace()
                record.name = get_gff_attribute(description, 'Name')
                record.type = item[2]
                record.reference = reference
                record.start = int(item[3]) - 1
                record.stop = int(item[4])
                record.strand = item[6]
                record.phase = item[7]
                record.source = item[1]
                record.description = description
                records.append(record)

    return records

def read_references(filename) :
    if not isinstance(filename, list) :
        filename = [filename]

    ref = {}
    for file in filename :
        name = os.path.basename(file).split('.')
        name = name[0]
        fin = open(file, 'rb')
        records = list(Bio.SeqIO.parse(fin, 'fasta'))
        fin.close()
        record = records[0]
        ref_record = SimpleNamespace()
        ref_record.seq = record.seq.tostring()
        ref_record.description = record.description
        ref[name] = ref_record

    return ref

def group_gff_records_by_name(records) :
    genes = {}
    for record in records :
        if record.type == 'CDS' :
            name = record.name
            if genes.has_key(name) :
                genes[name].cds.append(record)
            else :
                gene_record = SimpleNamespace()
                gene_record.cds = [record]
                genes[name] = gene_record
    
    return genes

def unify_gff_record_groups(genes, verbose = True) :
    for gene in genes.values() :
        gene.cds.sort(feature_cmp)

    return genes

def check_alignment(seqs, alignments) :
    set = Set([])
    counts = {}
    total_counts = {}
    for alg_group in alignments :
        for alg in alg_group[2] :
            if not seqs.has_key(alg.reference) :
                set.add(alg.reference)
                continue
            if alg.start < alg.stop :
                ref_seq = seqs[alg.reference].seq[alg.start:alg.stop + 1]
            else :
                ref_seq = seqs[alg.reference].seq[alg.stop:alg.start + 1]
                ref_seq = Bio.Seq.Seq(ref_seq).reverse_complement().tostring()
            
            len_ref_seq = len(ref_seq)
            cnt = 0
            for i in range(len_ref_seq) :
                if ref_seq[i] != alg_group[0][i] :
                    cnt = cnt + 1
            if not total_counts.has_key(alg.reference) :
                total_counts[alg.reference] = 0
            total_counts[alg.reference] = total_counts[alg.reference] + 1

            if cnt > 4 :
                print 'Mismatches %d:\n%s\n%s\n%s\n' % (cnt, alg_group[0], ref_seq, alg.reference)
                if not counts.has_key(alg.reference):
                    counts[alg.reference] = 0
                counts[alg.reference] = counts[alg.reference] + 1
    for ref in set :
        print '[-] No reference found for %s' % ref

    for key in counts.keys() :
        print '%10s : %4d (%.4f)' % (key, counts[key], (counts[key] + 0.0) / total_counts[key])

def read_marshal(filename) :
    fin = open(filename, 'rb')
    loaded = marshal.load(fin)
    fin.close()

    return loaded

def read_pickle(filename) :
    fin = open(filename, 'rb')
    loaded = cPickle.load(fin)
    fin.close()

    return loaded

def filter_alignment(alignments, mismatches = 2, min_length = 22, max_length = 32, max_nucA = 18, unique = False) :
    good = []
    filtered = 0
    filtered_alg = 0
    for alg_group in alignments :
        n_alignments = len(alg_group[2])
        if unique and n_alignments > 1 :
            filtered += 1
            filtered_alg += n_alignments
            continue
        group_good = []
        for alg in alg_group[2] :
            # Too many mismatches
            if len(alg_group[0]) - alg.score > mismatches :
                filtered_alg = filtered_alg + 1
                continue

            # Too many As in the beginning of the read
            cnt_nucA = 0
            for nuc in alg_group[0][:24] :
                if nuc == 'A' :
                    cnt_nucA = cnt_nucA + 1
            if cnt_nucA > max_nucA :
                filtered_alg = filtered_alg + 1
                continue

            # Read potentially too long
            if alg.maximum_length > max_length :
                filtered_alg = filtered_alg + 1
                continue

            # Read potentially too short
            if alg.maximum_length - alg.ambiguity_length < min_length :
                filtered_alg = filtered_alg + 1
                continue
                
            group_good.append(alg)
        alg_group[2] = group_good
        if len(group_good) > 0 :
            good.append(alg_group)
        else :
            filtered = filtered + 1

    return (good, filtered, filtered_alg)

def map_alignment(seqs, alignments) :
    positions_for = {}
    positions_rev = {}
    for key in seqs.keys() :
        positions_for[key] = []
        positions_rev[key] = []
        length = len(seqs[key].seq)
        for l in range(length) :
            positions_for[key].append([])
            positions_rev[key].append([])

    for alg_group in alignments :
        for alg_id in range(len(alg_group[2])) :
            alg = alg_group[2][alg_id]
            if not seqs.has_key(alg.reference) :
                continue
            map = SimpleNamespace()
            map.group = alg_group
            map.alignment_id = alg_id
            map.num_covering_features = 0
            if alg.start < alg.stop :
                positions_for[alg.reference][alg.start].append(map)
            else :
                positions_rev[alg.reference][alg.start].append(map)

    return (positions_for, positions_rev)

def cover_alignment_array(feature, start_prefix, end_prefix, alg, kernel, is_overlapping = False, is_ambiguous = False, array = None, array_overlapping = None, array_ambiguous = None, weight = 1.0, scale = 1.0, simulate = False) :
    alignment_length = abs(alg.stop - alg.start) + 1
    weight /= float(scale)
    kernel = kernel(alg.start, alg.stop, weight)
    covered = 0
    start, stop = alg.start, alg.stop + 1
    feature_start, feature_stop = feature.start, feature.stop
    if feature.strand == '+' :
        feature_start -= start_prefix
        feature_stop -= end_prefix
        if alg.start >= feature_start and alg.stop < feature_stop :
            start, stop = alg.start, alg.stop + 1
        elif alg.start < feature_start and alg.stop < feature_stop and alg.stop >= feature_start :
            start, stop = feature_start, alg.stop + 1
        elif alg.start >= feature_start and alg.stop >= feature_stop and alg.start < feature_stop :
            start, stop = alg.start, feature_stop
        elif alg.start < feature_start and alg.stop >= feature_stop :
            start, stop = feature_start, feature_stop
        else : # read does not overlap with the feature
            start, stop = 0, 0
        
        if not simulate :
            for i in range(start, stop) :
                array[i - feature_start] += kernel[i - alg.start]
                if is_overlapping and array_overlapping is not None :
                    array_overlapping[i - feature_start] += kernel[i - alg.start]
                if is_ambiguous and array_ambiguous is not None :
                    array_ambiguous[i - feature_start] += kernel[i - alg.start]
        
        for i in range(start, stop) :
            covered += kernel[i - alg.start]
    else :
        feature_stop += start_prefix
        feature_start += end_prefix
        if alg.start < feature_stop and alg.stop >= feature_start :
            start, stop = alg.stop, alg.start + 1
        elif alg.start >= feature_stop and alg.stop >= feature_start and alg.stop < feature_stop :
            start, stop = alg.stop, feature_stop
        elif alg.stop < feature_start and alg.start < feature_stop and alg.stop >= feature_start :
            start, stop = feature_start, alg.start + 1
        elif alg.stop < feature_start and alg.start >= feature_stop :
            start, stop = feature_start, feature_stop
        else : # read does not overlap with the feature
            start, stop = 0, 0
        
        if not simulate :
            for i in range(start, stop) :
                array[i - feature_start + end_prefix] += kernel[alg.start - i]
                if is_overlapping and array_overlapping is not None :
                    array_overlapping[i - feature_start + end_prefix] += kernel[alg.start - i]
                if is_ambiguous and array_ambiguous is not None :
                    array_ambiguous[i - feature_start + end_prefix] += kernel[alg.start - i]
        
        for i in range(start, stop) :
            covered += kernel[alg.start - i]
  
    return covered

def cover_positions(genes, positions, kernel, start_prefix, end_prefix, delta = 35) :
    positions_for, positions_rev = positions
    for gene in genes.values() :
        features = gene.cds
        for feature in features :
            if not positions_for.has_key(feature.reference) :
                continue
            length = len(positions_for[feature.reference])
            start = feature.start 
            stop = feature.stop
            if feature.strand == '+' :
                start -= start_prefix
                pos = positions_for
            else :
                stop += start_prefix
                pos = positions_rev
            start = max(start - delta, 0)
            stop = min(stop + delta, length)

            for i in range(start, stop) :
                for alg in pos[feature.reference][i] :
                    group = alg.group
                    id = alg.alignment_id
                    alignment_coverage = cover_alignment_array(feature, start_prefix, end_prefix, group[2][id], kernel, simulate = True)
                    if alignment_coverage > 0:
                        alg.num_covering_features += 1

    return (positions_for, positions_rev)

def feature_cmp(a, b) :
    if a.strand != b.strand :
        raise ValueError()

    strand = a.strand
    if strand == '+' :
        if a.start < b.start :
            return -1
        elif a.start > b.start :
            return +1
        elif a.stop > b.stop :
            return -1
        elif a.stop < b.stop :
            return +1
    else :
        if a.stop > b.stop : 
            return -1
        elif a.stop < b.stop :
            return +1
        elif a.start < b.start :
            return -1
        elif a.start > b.start :
            return +1
    return 0

def get_feature_seq(seqs, feature, start_prefix = 0) :
    start = feature.start - start_prefix if feature.strand == '+' else feature.start
    stop = feature.stop + start_prefix if feature.strand == '-' else feature.stop
    seq = seqs[feature.reference].seq[start:stop]
    if feature.strand == '-' :
        seq = Bio.Seq.Seq(seq).reverse_complement().tostring()
    return seq

def ribosome_density_kernel(start, stop, weight = 1.0) :
    '''
    Kernel for ribosome occupancy events. Covers only positions 15-17.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    offset_left = 15
    length = abs(stop - start) + 1
    kernel = np.zeros((length,))
    for i in range(offset_left, min(offset_left + 3, length)) :
        kernel[i] = weight

    return kernel

def mrna_coverage_kernel(start, stop, weight = 1.0) :
    '''
    Kernel for read coverage events. Uses full length coverage.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = abs(stop - start) + 1
    kernel = np.ones((length,)) * weight / float(length)
    return kernel

def ribosome_coverage_kernel(start, stop, weight = 1.0) :
    '''
    Kernel for read coverage events. Uses full length coverage.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = abs(stop - start) + 1
    kernel = np.ones((length,)) * weight
    return kernel

def read_start_kernel(start, stop, weight = 1.0) :
    '''
    Kernel for marking starts of reads.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = abs(stop - start) + 1
    kernel = np.zeros((length,))
    kernel[0] = weight

    return kernel

def make_feature_array(positions, feature, start_prefix, end_prefix, kernel, delta, assignment_kernel = None) :
    positions = positions[0] if feature.strand == '+' else positions[1]
    if not positions.has_key(feature.reference) :
        return KeyError()

    if assignment_kernel is None :
        assignment_kernel = kernel

    start, stop = feature.start, feature.stop
    positions = positions[feature.reference]
    if feature.strand == '+' :
        start -= start_prefix
    else :
        stop += start_prefix
    end_prefix = min(end_prefix, feature.stop - feature.start)

    feature_length = stop - start 
    array = np.zeros((feature_length, ))
    array_overlapping = np.zeros((feature_length, ))
    array_ambiguous = np.zeros((feature_length, ))
    total = 0 

    overlapping, ambiguous = 0, 0
    reference_length = len(positions)
    start_adj = max(0, min(reference_length, start - delta))
    stop_adj = max(0, min(reference_length, stop + delta))

    for i in range(start_adj, stop_adj) :
        for alg in positions[i] :
            id = alg.alignment_id
            group = alg.group
            count = group[1]
            n_alignments = len(group[2])
            n_covering_features = alg.num_covering_features

            is_overlapping = n_covering_features > 1
            is_ambiguous = n_alignments > 1
            scale = float(n_alignments)
            
            assigned = cover_alignment_array(feature, start_prefix, end_prefix, group[2][id], assignment_kernel, simulate = True)
            if assigned :
                covered = cover_alignment_array(feature, start_prefix, end_prefix, group[2][id], kernel, is_overlapping, is_ambiguous, array, array_overlapping, array_ambiguous, count, scale = scale)
                if covered > 0 :
                    total += covered
                    if is_overlapping :
                        overlapping += covered
                    if is_ambiguous :
                        ambiguous += covered

    if feature.strand == '-':
        array = array[::-1] 
        array_overlapping = array_overlapping[::-1]
        array_ambiguous = array_ambiguous[::-1]

    return (array, array_overlapping, array_ambiguous, overlapping, ambiguous, total)

def convert_to_mrna_array(seqs, genes, positions, kernel, start_prefix = 16, end_prefix = 14, delta = 35, assignment_kernel = None) :
    if assignment_kernel is None :
        assignment_kernel = kernel
    records = {}
    positions_for, positions_rev = positions
    for gene in genes.values() :
        cds_rec = gene.cds 
        reference = cds_rec[0].reference
        if not seqs.has_key(reference) :
            continue

        cds_rec.sort(feature_cmp)
        feature_length, feature_seq, = 0, ''
        feature_array, feature_array_overlapping, feature_array_ambiguous = np.zeros((0,)), np.zeros((0,)), np.zeros((0,))
        overlapping, ambiguous, total = 0, 0, 0
        name = cds_rec[0].name
        n_recs = len(cds_rec)
        for i in range(n_recs) :
            rec = cds_rec[i]
            rec_array, rec_array_overlapping, rec_array_ambiguous, rec_overlapping, rec_ambiguous, rec_total = make_feature_array(positions, rec, start_prefix, end_prefix, kernel, delta, assignment_kernel = assignment_kernel)
            rec_seq = get_feature_seq(seqs, rec, start_prefix)

            if i > 0 :
                rec_seq = rec_seq[start_prefix:]
                cur_len = feature_array.shape[0]
                feature_array[cur_len - start_prefix:] += rec_array[:start_prefix]
                feature_array_overlapping[cur_len - start_prefix:] += rec_array_overlapping[:start_prefix]
                feature_array_ambiguous[cur_len - start_prefix:] += rec_array_ambiguous[:start_prefix]
                rec_array = rec_array[start_prefix:]
                rec_array_overlapping = rec_array_overlapping[start_prefix:]
                rec_array_ambiguous = rec_array_ambiguous[start_prefix:]

            feature_seq += rec_seq
            feature_array = np.concatenate((feature_array, rec_array))
            feature_array_overlapping = np.concatenate((feature_array_overlapping, rec_array_overlapping))
            feature_array_ambiguous = np.concatenate((feature_array_ambiguous, rec_array_ambiguous))
            
            rec_length = rec.stop - rec.start
            feature_length += rec_length
            overlapping += rec_overlapping
            ambiguous += rec_ambiguous
            total += rec_total
        
        gene_record = SimpleNamespace()
        gene_record.mrna = make_feature_record(feature_seq, feature_array, feature_array_overlapping, feature_array_ambiguous, feature_length, overlapping, ambiguous, total)
        gene_record.start_prefix = start_prefix
        gene_record.end_prefix = end_prefix
        records[name] = gene_record

    return records
