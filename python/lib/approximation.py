import persistent
from tools import ParallelTimeout

import math
import numpy as np
import scipy.optimize
import time

def _line_sse_fit(C, x, y) :
    '''
    Sum-squared-error (SSE) of the line LINE function.
    :param x: initiation rates.
    :param y: measured densities.
    '''
    x, y = np.array(x), np.array(y)
    return np.sum((C[0] * x + C[1] - y ) ** 2) 

def _density_sse_fit(C, x, y) :
    '''
    Sum-squared-error (SSE) of the left function.
    :param C: function parameters.
    :param x: initiation rates.
    :param y: measured densities.
    '''
    x, y = np.array(x), np.array(y)
    return np.sum((C[0] * x / (C[1] + x) - y) ** 2) 

def _select_cut(x) :
    '''
    Returns the middle point of array.
    :param x: array.
    '''
    n = len(x)
    if n == 0 :
        return float('nan')
    x = list(sorted(x))
    return x[n / 2]

def approximate_density(gene, rates, srand, eps = 1e-4, samples = 5, timeout = float('Inf')) :
    '''
    Derive an analytical approximation of the segment density.
    :param gene: name of the gene to be approximated.
    :param rates: rate function parameters.
    :param srand: random seed for the MonteCarlo simulation.
    :param eps: precision to be used in determining the switching point.
    :param samples: number of samples to be taken for the initial parameter guess.
    :param timeout: maximum clock time (in seconds) allowed for the approximation to complete.
    '''
    global _cache_approximation
    global _cache
    if _cache_approximation and _cache.has_key(gene) :
        return _cache[gene]
    
    start_time = time.time()
    simulator, rate_function, tree = persistent.simulators[gene], persistent.rate_functions[gene], persistent.forest[gene]
    rate_function.beta[:] = rates
    simulator.compute_rates()

    # Obtain initial values for the binary-search-fit
    min_rate, max_rate = np.min(simulator._rate), np.max(simulator._rate)
    #print 'Min rate: %g' % min_rate
    lower_rates = np.linspace(1e-6, min_rate / 2.0, samples)
    upper_rates = np.linspace(max_rate, 1.0, samples)
    lower_densities, upper_densities = [], []
    for i in xrange(samples) :
        simulator.initiation = lower_rates[i]
        result = simulator.simulate(srand = srand)
        lower_densities.append(result.density)
        simulator.initiation = upper_rates[i]
        result = simulator.simulate(srand = srand)
        upper_densities.append(result.density)
        # Check timeout
        if time.time() - start_time > timeout :
            raise ParallelTimeout('Gene: %s, timeout %.5f seconds' % (gene, timeout))

    n_segments = len(tree.tree)
    left, right = np.zeros((n_segments,)), np.zeros((n_segments,))
    left_points, right_points = [], []
    left_values, right_values = [], []
    segment_length = np.zeros((n_segments,))
    edge_l = np.zeros((n_segments,))
    edge_r = np.zeros((n_segments,))
    params = np.zeros((n_segments, 4))
    for i in xrange(n_segments) :
        left[i], right[i] = np.max(lower_rates), np.min(upper_rates)
        l, r, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = tree.tree[i]
        segment_length[i] = r - l + 1
        edge_l[i], edge_r[i] = l, r
        lower_estimates, upper_estimates = [], []
        for j in xrange(samples) :
            lower_estimates.append(np.sum(lower_densities[j][l:r + 1]) / segment_length[i])
            upper_estimates.append(np.sum(upper_densities[j][l:r + 1])  / segment_length[i])
        left_points.append(lower_rates.tolist())
        right_points.append(upper_rates.tolist())
        left_values.append(lower_estimates)
        right_values.append(upper_estimates)
        fit = scipy.optimize.minimize(_density_sse_fit, params[i][:2], args = (left_points[i], left_values[i]), method = 'TNC', bounds = [(0, None), (0, None)])
        params[i][:2] = fit.x
        fit = scipy.optimize.minimize(_line_sse_fit, params[i][2:], args = (right_points[i], right_values[i]), method = 'TNC', bounds = [(0, None), (None, None)])
        params[i][2:] = fit.x
    
    # Check timeout
    if time.time() - start_time > timeout :
        raise ParallelTimeout('Gene: %s, timeout %.5f seconds' % (gene, timeout))

    while True :
        proposed = []
        for i in xrange(n_segments) :
            if right[i] - left[i] < eps :
                continue
            m = (left[i] + right[i]) / 2.0
            proposed.append(m)
        m = _select_cut(proposed)
        if math.isnan(m) :
            break
        changed_left = np.zeros((n_segments, ), dtype = np.bool)
        simulator.initiation = m
        result_m = simulator.simulate(srand = srand)
        for i in xrange(n_segments) :
            density = np.sum(result_m.density[edge_l[i]:edge_r[i] + 1]) / segment_length[i]
            if m > left[i] and m < right[i] :
                guess_left = params[i][0] * m / (params[i][1] + m)
                guess_right = params[i][2] * m + params[i][3]
                if (guess_left - density) ** 2 < (guess_right - density) ** 2 and guess_left < guess_right :
                    left_values[i].append(density)
                    left_points[i].append(m)
                    left[i] = m
                    changed_left[i] = True
                else :
                    right_values[i].append(density)
                    right_points[i].append(m)
                    right[i] = m
            elif m < left[i] :
                left_values[i].append(density)
                left_points[i].append(m)
                changed_left[i] = True
            elif m > right[i] :
                right_values[i].append(density)
                right_points[i].append(m)
         
        for i in xrange(n_segments) :
            if changed_left[i] :
                fit = scipy.optimize.minimize(_density_sse_fit, params[i][:2], args = (left_points[i], left_values[i]), method = 'TNC', bounds = [(0, None), (0, None)])
                params[i][:2] = fit.x
            else :
                fit = scipy.optimize.minimize(_line_sse_fit, params[i][2:], args = (right_points[i], right_values[i]), method = 'TNC', bounds = [(0, None), (None, None)])
                params[i][2:] = fit.x
        
        # Check timeout
        if time.time() - start_time > timeout :
            raise ParallelTimeout('Gene: %s, timeout %.5f seconds' % (gene, timeout))

    result = []
    for i in xrange(n_segments) :
        param = np.zeros((5,))
        param[:4] = params[i]
        # A safeguard. In case we did not achieve sufficient precision, move the boundary to the intersection point
        middle = (left[i] + right[i]) / 2.0
        a, b, c, d  = tuple(params[i])
        if c != 0 : 
            D = (b * c + d - a) ** 2 - 4 * b * c * d
            if D >= 0 :
                x1, x2 = (a - b * c - d - math.sqrt(D)) / (2.0 * c), (a - b * c - d + math.sqrt(D)) / (2.0 * c)
                mi, ma = min(x1, x2), max(x1, x2)
                x = mi if mi > 0 else ma
                #print 'x1: %g, x2: %g' % (x1, x2)
                if x > 0 and x < middle :
                    middle = x
        else :
            x = b * d / (a - d)
            if x > 0 and x < middle :
                middle = x
        
        param[4] = middle
        result.append(param)
    
    if _cache_approximation :
        _cache[gene] = result
    return result

def _evaluate_approximation(gene, approximation, init) :
    '''
    Evaluate fit based on density approximation.
    :param gene: name of the gene to be evaluated.
    :param approximation: list of approximation parameter values.
    :param init: initiation rate for which the approximation should be evaluated.
    '''
    tree = persistent.forest[gene]
    n = len(approximation)
    result = np.zeros((3,))
    for i in xrange(n) :
        a, b, c, d, e = tuple(approximation[i])
        segment_occupancy = a * init / (b + init) if init <= e else c * init + d
        node = tree.tree[i]
        left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, sigma = node
        mu = ratio_a / math.log(math.exp(1.0), 2)
        lnx = np.log(segment_occupancy)
        weight = 1.0 / (sigma ** 2)
        result[0] += -0.5 * weight
        result[1] += (mu - lnx) * weight + 1
        result[2] += -0.5 * weight * (lnx - mu) ** 2 + lnx
    return result

def approximate_initiation(gene, rates, scale, srand, eps = 1e-6, timeout = float('Inf')) :
    '''
    Approximates gene initiation rate by finding an optimum rate based on analytical segment density approximations.
    :param gene: name of the gene to be processed.
    :param rates: rate function parameters.
    :param scale: scale based on which the initiation rate should be chosen.
    :param srand: random seed to be used for MonteCarlo simulations.
    :param eps: Precision for determining the initiation rate.
    :param timeout: maximum clock time (in seconds) allowed for the approximation to complete.
    '''
    approximation = approximate_density(gene, rates, srand, timeout = timeout)
    # Using ternary search is not really correct.
    # I cannot prove that the function has only a single maximum.
    # Should switch to a general-purpose optimization procedure perhaps.
    left, right = 1e-6, 1.0 - 1e-6
    while right - left >= eps :
        m1, m2 = (2 * left + right) / 3.0, (left + 2 * right) / 3.0
        f1, f2 = _evaluate_approximation(gene, approximation, m1), _evaluate_approximation(gene, approximation, m2)
        if scale is not None :
            f1, f2 = f1[0] * scale ** 2 + f1[1] * scale + f1[2], f2[0] * scale ** 2 + f2[1] * scale + f2[2]
        else :
            scale_f1, scale_f2 = -f1[1] / (2.0 * f1[0]), -f2[1] / (2.0 * f2[0])
            f1, f2 = f1[0] * scale_f1 ** 2 + f1[1] * scale_f1 + f1[2], f2[0] * scale_f2 ** 2 + f2[1] * scale_f2 + f2[2]
        if f1 < f2 :
            left = m1
        else :
            right = m2
    init = (left + right) / 2.0
    return init

def initialize_cache() :
    global _cache_approximation
    global _cache
    _cache_approximation = False
    _cache = {}

initialize_cache()
