import tools
import tree
import numpy as np

RFM_times = np.array([39.0000,  62.1868,  27.3000,  16.8103,  136.4113,  21.0000,  29.1667,  27.3000,  68.2312,  24.8182,  34.4697,  119.7368,  24.8182,  310.9339,  136.5000,  60.3982,  1.0001,  77.7335,  34.1250,  1.0001,  39.0000,  62.1868,  27.3000,  22.3039,  90.9666,  24.8182,  34.4697,  139.2857,  1.0001,  155.4670,  68.2500,  45.5000,  30.3333,  88.8383,  39.0000,  70.3608,  91.0000,  621.8679,  273.0000,  284.3750,  27.2995,  136.5000,  189.5833,  85.3125,  1365.0000,  45.5000,  63.1944,  273.0000,  19.5000,  38.8667,  17.0625,  42.1296,  136.4045,  19.5000,  27.0833,  103.4091,  54.5880,  24.8182,  34.4697,  170.6250,  91.0000,  38.8667,  17.0625,  92.2297])

class ZhangModel(object) :
    W = 19
    '''
    Window size for smoothing the model output.
    '''

    def __init__(self, times = None) :
        '''
        Class constructor.
        Takes translation times as input.
        '''
        if times is None :
            times = np.array(RFM_times)
        self.times = times

    def simulate(self, record) :
        '''
        Predict local translation times.
        :param record: record for which the prediction has to be done.
        '''
        seq = tree.get_codon_sequence(record)
        length = seq.shape[0]
        rates = np.zeros((length,))
        for i in range(length) :
            codon = tools.codon2int[seq[i]]
            rates[i] = self.times[codon]
        
        smoothed = np.zeros((length,))
        W2 = int(self.W / 2)
        for i in range(length) :
            start, end = max(i - W2, 0), min(i + W2 + 1, length)
            smoothed[i] = np.mean(rates[start:end])
        
        return smoothed
