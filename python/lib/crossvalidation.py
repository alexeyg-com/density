import numpy as np

def create_folds(genes, forest, n_folds = 5, n_bins = 40, max_length = 2000, seed = 9910178) :
    '''
    Create balanced cross-validation folds for fitting.
    :param genes : genes to be split between cross-validation folds.
    :param forest : segment tree forest to be used.
    :param n_folds : number of cross-validation folds to be created.
    :param n_bins : number of bin to split gene into based on measurement (segment) count. Empty bins are removed.
    :param max_length : maximum allowed (inclusive) gene length in codons.
    :param seed : seed of the random number generator to be used.
    '''
    n = len(genes)
    genes = np.array(genes)
    lengths = np.zeros((n, ))
    for i in range(n) :
        lengths[i] = forest[genes[i]].length_codons
    print '[i] Dropping %d genes (too long)\n' % np.sum(lengths > max_length)
    genes = genes[lengths <= max_length]
    lengths = lengths[lengths <= max_length]
    
    n = len(genes)
    np.random.seed(seed)
    meas = np.zeros((n, 2))
    for i in range(n) :
        gene = genes[i]
        meas[i][0] = len(forest[gene].tree)
        meas[i][1] = i
    meas = sorted(meas, key = lambda x : x[0])
    meas = np.array(meas)
    
    bins = []
    per_bin = n / float(n_bins)
    int_pos_prev = 0
    pos = 0
    while pos < n :
        pos = min(per_bin + pos, n)
        int_pos = int(pos)
        if int_pos < n :
            val = meas[int_pos][0]
        else :
            val = meas[n - 1][0] + 1
        prev_val = meas[int_pos_prev][0]
        indices = (meas[:, 0] >= prev_val) * (meas[:, 0] < val)
        int_pos_prev = int_pos
        bin = meas[indices, 1]
        if len(bin) > 0 :
            bins.append(bin.astype(np.int))
            print '[%4d, %4d) : %d genes' % (prev_val, val, len(bin))
    n_bins = len(bins)
    for i in range(n_bins) :
        np.random.shuffle(bins[i])
    
    folds = []
    for i in range(n_folds) :
        fold = []
        for j in range(n_bins) :
            per_fold = len(bins[j]) / float(n_folds)
            start = int(i * per_fold)
            end = int((i + 1) * per_fold)
            selection = bins[j][start:end]
            for ind in selection :
                fold.append(genes[ind])
        folds.append(fold)
    
    print ''
    for i in range(n_folds) :
        cnt = 0
        for gene in folds[i] :
            cnt += len(forest[gene].tree)
        print 'Fold %d : %d genes, %d meas' % (i + 1, len(folds[i]), cnt)
    return folds
