'''
Implements a range of helper functions used throughout the project.

Alexey Gritsenko
31-10-2013
'''

import reader
import profile
import zhang
import scipy.stats
import scipy.optimize
import numpy as np
import copy
import math

from lib.SimpleNamespace import SimpleNamespace

class ParallelTimeout(Exception) :
    '''
    A custom class for parallel timeout exception.
    '''
    pass

def select_entries(density, genes) :
    '''
    Return a subselection of the dictionary.
    :param dict density: the dictionary.
    :param genes: a list of genes to select.
    '''
    out = {}
    for gene in genes :
        if density.has_key(gene) :
            out[gene] = density[gene]
    return out

def select_entries_inverse(density, genes) :
    '''
    Return a list of genes that are in the provided gene list.
    :param dict density: the dictionary.
    :param genes: list of genes to remove from the dictionary.
    '''
    out = {}
    for gene in density.keys() :
        if not gene in genes :
            out[gene] = density[gene]
    return out

def overlap_gene_lists(a, b) :
    '''
    Return overlap of two gene sets.
    :param a: Set a
    :param b: Set b
    '''
    return list(set(a) & set(b))

def chromosome2int(str) :
    '''
    Convert chromosome string representation into a number.
    :param string str: The representation.
    '''
    if str == 'chrI' :
        return 1
    elif str == 'chrII' :
        return 2
    elif str == 'chrIII' :
        return 3
    elif str == 'chrIV' :
        return 4
    elif str == 'chrV' :
        return 5
    elif str == 'chrVI' :
        return 6
    elif str == 'chrVII' :
        return 7
    elif str == 'chrVIII' :
        return 8
    elif str == 'chrIX' :
        return 9
    elif str == 'chrX' :
        return 10
    elif str == 'chrXI' :
        return 11
    elif str == 'chrXII' :
        return 12
    elif str == 'chrXIII' :
        return 13
    elif str == 'chrXIV' :
        return 14
    elif str == 'chrXV' :
        return 15
    elif str == 'chrXVI' :
        return 16
    else :
        return 0

def load_arrays_tree(rib_file  = '../results/rib_density.out', mrna_file = '../results/mrna_density.out') :
    '''
    Load default ribosome and mRNA density arrays.
    :param string rib_file: Ribosome density filename.
    :param string mrna_file: mRNA density filename.
    '''
    # Read
    rib = reader.read_pickle(rib_file)
    mrna = reader.read_pickle(mrna_file)
    
    return (mrna, rib)

def stats_on_alignment_reads(alignment, mismatches = 2, min_length = 22, max_length = 32, max_nucA = 18) :
    '''
    Computes simple statistics on the alignment reads.
    :param alignment: a list of read alignments.
    :param mismatches: maximum number of mismatches allowed.
    :param min_length: minimum allowed alignment length.
    :param max_length: maximum allowed alignment length.
    :param max_nucA: maximum number of A nucleotides allowed at the end of the read.
    '''
    alignment = copy.deepcopy(alignment)
    alignment, removed_groups, removed_alg = reader.filter_alignment(alignment, mismatches, min_length, max_length, max_nucA)
    count, count_ambiguous, count_alignments, min_length, max_length, max_mismatches = 0, 0, 0, 99, -1, -1
    for group in alignment :
        count += group[1]
        cur_alignments = group[2]
        n_alignments = len(cur_alignments)
        count_alignments += n_alignments
        for alg in cur_alignments :
            cur_mismatches = len(group[0]) - alg.score
            if cur_mismatches > max_mismatches :
                max_mismatches = cur_mismatches
            max_length = max(max_length, alg.maximum_length)
            min_length = min(min_length, alg.maximum_length - alg.ambiguity_length)
        if n_alignments > 1 :
            count_ambiguous += group[1]
    print 'Count: %d' % count
    print 'Count ambiguous: %d' % count_ambiguous
    print 'Count unique: %d' % (count - count_ambiguous)
    print 'Count alignments: %d' % count_alignments
    print 'Min. length: %d' % min_length
    print 'Max. length: %d' % max_length
    print 'Max. mismatches: %d' % max_mismatches

def stats_on_cds_assigned_reads(alignment, reference, records, mismatches = 2, min_length = 22, max_length = 32, max_nucA = 18, unique = False) : 
    '''
    :param alignment: a list of read alignments.
    :param reference: genome reference sequence.
    :param records: GFF records of CDSes.
    :param mismatches: maximum number of mismatches allowed.
    :param min_length: minimum allowed alignment length.
    :param max_length: maximum allowed alignment length.
    :param max_nucA: maximum number of A nucleotides allowed at the end of the read.
    :param unique: if true, all ambiguous reads (reads with multiple alignments) are filtered out.
    '''
    genes = reader.group_gff_records_by_name(records)
    genes = reader.unify_gff_record_groups(genes)
    alignment = copy.deepcopy(alignment)
    filtered_alignment, removed_groups, removed_alg = reader.filter_alignment(alignment, mismatches, min_length, max_length, max_nucA, unique)
    positions = reader.map_alignment(reference, filtered_alignment)
    positions = reader.cover_positions(genes, positions, reader.read_start_kernel, start_prefix = 16, end_prefix = 14)
    covered, overlapping = 0.0, 0.0
    for strand in positions :
        chromosomes = strand.keys()
        for chr in chromosomes :
            array = strand[chr]
            for pos in array :
                for entry in pos :
                    id = entry.alignment_id
                    num_covering_features = entry.num_covering_features
                    if num_covering_features == 0 :
                        continue
                    group = entry.group
                    group_size = len(group[2])
                    group_weight = group[1] / float(group_size)
                    covered += group_weight
                    if num_covering_features > 1 :
                        overlapping += group_weight
    print 'Covered: %.2f' % covered
    print 'Overlapping: %.2f' % overlapping

def get_read_count_from_array(array) :
    '''
    Obtain read count by summing over genes in mRNA/Ribosome arrays.
    '''
    acc = 0.0
    genes = array.keys()
    for gene in genes :
        acc += array[gene].mrna.total
    return acc

def get_cds_array(record, which = 'summary') :
    '''
    Returns the CDS regions from the gene array record of the offset mRNA.
    :param record: feature record to obtain array from.
    :param which: type of array to obtain, can be 'summary' (default), 'overlapping' or 'ambiguous'.
    '''
    array = record.mrna.array
    if which == 'overlapping' :
        array = record.mrna.array_overlapping
    elif which == 'ambiguous' :
        array = record.mrna.array_ambiguous
    elif which == 'sequence' :
        array = record.mrna.seq
    return array[record.start_prefix:]

def get_codon_array(record, which = 'summary') :
    '''
    Returns the CDS regions from the gene array record of the offset mRNA at codon resolution.
    :param record: feature record to obtain codon array from.
    :param which: type of array to obtain, can be 'summary' (default), 'overlapping' or 'ambiguous'.
    '''
    array = record.mrna.array
    if which == 'overlapping' :
        array = record.mrna.array_overlapping
    elif which == 'ambiguous' :
        array = record.mrna.array_ambiguous
    elif which == 'sequence' :
        array = record.mrna.seq

    length = len(array) - record.start_prefix
    length_codons = length / 3
    if which != 'sequence' :
        array_codons = np.zeros((length_codons, ))
    else :
        array_codons = np.zeros((length_codons, ), dtype = 'S3')

    if which == 'sequence' :
        for i in range(0, length_codons) :
            array_codons[i] = array[record.start_prefix + 3 * i : record.start_prefix + 3 * (i + 1)]
    else :
        for i in range(0, length_codons) :
            array_codons[i] = np.sum(array[record.start_prefix + 3 * i : record.start_prefix +  3 * (i + 1)])
    return array_codons

def get_cds_count(record) :
    '''
    Return coverage sum for the CDS of an mRNA record.
    '''
    cds = get_cds_array(record)
    return np.sum(cds)

def get_mean_read_length(alignment, should_filter = True) :
    '''
    Compute mean read length from read alignments.
    :param alignment: list of read alignments.
    :param should_filter: a boolean flag specifying whether the alignments should be filtered.
    '''
    if should_filter :
        alignment, removed_groups, removed_alg = reader.filter_alignment(alignment)

    total_count, total_length = 0.0, 0.0
    for group in alignment :
        read_count = float(group[1])
        total_count += read_count
        alignment_count = float(len(group[2]))
        for alg in group[2] :
            read_length = abs(alg.stop - alg.start) + 1
            total_length += read_length * (read_count / alignment_count)

    return total_length / total_count

def correlate_profiles(profiles_a, profiles_b, reliable = [], only_nonzero = True):
    '''
    Compute correlations between gene profiles.
    :param profiles_a: dictionary with a first set profiles.
    :param profiles_b: dictionary with a second set of profiles.
    :param reliable: a list of genes that are measured reliably.
    :param only_nonzero: if true, only non-zero coverage array positions are used to compute correlations.
    '''
    genes_a = profiles_a.keys()
    genes_b = profiles_b.keys()
    genes = overlap_gene_lists(genes_a, genes_b)
    n_genes = len(genes)
    corr = []
    reliable = set(reliable)
    out_genes = []
    for i in range(n_genes) :
        gene = genes[i]
        if len(reliable) > 0 and (not gene in reliable) :
            continue
        array_a, array_b = profiles_a[gene], profiles_b[gene]
        positions = np.logical_and(np.negative(np.isnan(array_a)), np.negative(np.isnan(array_b)))
        if only_nonzero :
            nz_positions = np.logical_and(array_a > 0, array_b > 0)
            positions = np.logical_and(positions, nz_positions)
        if np.sum(positions) <= 1 :
            continue
        res = scipy.stats.pearsonr(array_a[positions], array_b[positions])
        r = res[0]
        if np.isnan(r) or isinstance(r, np.ndarray) :
            continue
        corr.append(r)
        out_genes.append(gene)
    corr = np.array(corr)
    return (out_genes, corr)

def get_sigma_for_length(length, sigmas) :
    '''
    Result 
    :param length: segment length for which the estimate has to be computed.
    :param sigmas: a list with length group biological variance estimates.
    '''
    n_sigmas = len(sigmas)
    result = None
    for i in range(n_sigmas) :
        min_length, max_length, sigma = sigmas[i]
        if length >= min_length and length < max_length:
            result = sigma
            break
    return result

def fit_segment_density(tree) :
    '''
    Given a tree with segment measurements, fit per codon densities that maximize the trees likelihood.
    :param tree : the segment tree to be approximated.
    '''
    n_segments = len(tree.tree)
    parent, is_leaf, length, left, right = {}, {}, {}, {}, {}
    ends = set()
    mean = {}
    for i in range(n_segments) :
        parent[i] = None
        is_leaf[i] = True
        node = tree.tree[i]
        left_i, right_i, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
        mean[i] = math.exp(ratio_m)
        length[i] = right_i - left_i + 1
        left[i], right[i] = left_i, right_i
        ends.add(right[i])
    
    for i in range(n_segments) :
        for j in range(n_segments) :
            if i == j :
                continue
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i
    for i in range(n_segments) :
        is_leaf[parent[i]] = False
    
    n_codons = tree.length_codons
    codons = -np.ones((n_codons,))
    for i in range(n_segments) :
        if is_leaf[i] :
            codons[left[i]:right[i] + 1] = i
    #print codons
    id = 0
    i = 0
    while i < n_codons :
        while i < n_codons and codons[i] >= 0 :
            i += 1
        if i >= n_codons :
            break
        j = i
        while j < n_codons and codons[j] < 0 and not j in ends :
            j += 1
        if j in ends and codons[j] < 0 :
            j += 1
        id -= 1
        left[id], right[id], length[id], parent[id], is_leaf[id] = i, j - 1, j - i, None, True
        mean[id] = 0.5
        i = j
    for i in range(n_segments) :
        for j in range(id, 0) :
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i

#    for i in parent :
#        if parent[i] is not None :
#            print '%d : [%d, %d] : parent %d' % (i, left[i], right[i], parent[i])
#        else :
#            print '%d : [%d, %d] : parent -' % (i, left[i], right[i])

    def objective(x, ids, left, right, n_codons, tree) :
        density = np.zeros((n_codons,))
        for i in range(len(ids)) :
            density[left[ids[i]] : right[ids[i]] + 1] = x[i]
        return -tree.evaluate(0.0, density)
    
    ids = [id for id in is_leaf.keys() if is_leaf[id]]
    n_vars = len(ids)
    start = np.ones((n_vars,))
    for i in range(n_vars) :
        start[i] = mean[ids[i]]
    fit = scipy.optimize.minimize(objective, start, args = (ids, left, right, n_codons, tree), method = 'SLSQP', bounds = [(0.0, 1000.0)] * n_vars, options = {'maxiter' : 1000000, 'maxfun' : 1000000})
    values = fit.x
    density = np.zeros((n_codons,))
    for i in range(len(ids)) :
        density[left[ids[i]] : right[ids[i]] + 1] = values[i]
    return (density, [(left[id], right[id]) for id in ids])

def build_coverage_profiles(mrna, rib, n_mrna, n_rib, type = 'mean') :
    '''
    :param mrna: dictionary of mRNA coverage counts.
    :param rib: dictionary of ribosome coverage counts.
    :param n_mrna: number of exon-mapped mRNA reads.
    :param n_rib: number of exon-mapped ribosome reads.
    :param type: type e to construct, can be 'mean' or 'profile'. 
    '''
    genes_mrna = mrna.keys()
    genes_rib = rib.keys()
    genes = overlap_gene_lists(genes_mrna, genes_rib)
    profiles = {}
    for gene in genes :
        profiles[gene] = profile.make_coverage_profile(mrna[gene], rib[gene], n_mrna, n_rib, type = type)
    return profiles

def get_RFM_rates() :
    '''
    Return RFM translation elongation rates.
    '''
    global codon2int

    result = {}
    rates = 1.0 / zhang.RFM_times
    for codon in codon2int.keys() :
        result[codon] = rates[codon2int[codon]]
    return result

def initialize_codon_dictionary() :
    '''
    Initializes the modules codon dictionary. It can be used to convert codon string representation into numeric representation.
    '''
    global codon2int

    codon2int = {}
    nucleotides = ['A', 'T', 'C', 'G']
    counter = 0
    for nuc1 in nucleotides :
        for nuc2 in nucleotides :
            for nuc3 in nucleotides :
                codon = nuc1 + nuc2 + nuc3
                codon2int[codon] = counter
                counter += 1

def initialize_amino_acid_dictionary() :
    '''
    Initialized the modules amino acid dictionaries.
    Includes:
    a) codon to amino acid dictionary.
    '''
    global codon2int
    global codon2amino
    global amino2codon
    
    codon2amino = {
        'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
        'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
        'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
        'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
        'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
        'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
        'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
        'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
        'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
        'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
        'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
        'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
        'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
        'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
        'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
        'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W'}
   
    items = codon2amino.items()
    for codon, amino in items :
        codon_int = codon2int[codon]
        codon2amino[codon_int] = amino
    
    amino2codon = {}
    for amino in set(codon2amino.values()) :
        amino2codon[amino] = []

    for codon in codon2int :
        amino = codon2amino[codon]
        amino2codon[amino].append(codon)

initialize_codon_dictionary()
initialize_amino_acid_dictionary()
