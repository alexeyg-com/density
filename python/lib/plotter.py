'''
Functions for producing various plots. In most cases the names are self-explanatory.
Uses matplotlib and pylab for plotting.

Alexey Gritsenko
20-06-2013
'''

import pkg_resources
pkg_resources.require("matplotlib") 

import matplotlib
import numpy as np
import pylab
import matplotlib.pyplot as plot
import matplotlib.colors
import pandas
import math
import scipy.stats

import tools
import simulator
from SimpleNamespace import SimpleNamespace

def plot_codon_density(mrna, rib, gene, marker_size = 4, per_codon = True) :
    '''
    Plot mRNA and ribosome densities on top of each other.
    :param mrna: mRNA alignment list.
    :param rib: ribosome alignment list.
    :param string gene: name of the gene to plot densities for.
    :param marker_size: size of the marker to be used for plotting densities (densities are plotted as scatter plots for those codons, where measurements are available)
    '''
    mrna_array = tools.get_cds_array(mrna[gene])
    rib_array = tools.get_cds_array(rib[gene])
    length = len(mrna_array)
    codon_len = length / 3
    if per_codon :
        mrna_array_codon = tools.to_codon_density_array(mrna_array)
        rib_array_codon = tools.to_codon_density_array(rib_array)
    else :
        mrna_array_codon = np.atleast_1d(mrna_array)
        rib_array_codon = np.atleast_1d(rib_array)
        codon_len = length
   
    x = np.array(range(1, codon_len + 1))
    
    fig = pylab.gcf()
    fig.suptitle('Gene: %s' % gene, fontsize = 14)
    pylab.subplot(211)
    #pylab.plot(x, rib_array_codon)
    pylab.scatter(x[rib_array_codon != 0], rib_array_codon[rib_array_codon != 0], s=marker_size)
    pylab.xlim(0, codon_len + 1)
    if per_codon :
        pylab.xlabel('Codon')
    else :
        pylab.xlabel('Nucleotide')
    pylab.ylabel('Ribosome density')
    pylab.grid(True)

    pylab.subplot(212)
    #pylab.plot(x, mrna_array_codon)
    pylab.scatter(x[mrna_array_codon != 0], mrna_array_codon[mrna_array_codon != 0], s=marker_size)
    pylab.xlim(0, codon_len + 1)
    if per_codon :
        pylab.xlabel('Codon')
    else :
        pylab.xlabel('Nucleotide')
    pylab.ylabel('mRNA density')
    pylab.grid(True)
    pylab.show()

def plot_pause(pause, gene, additional = 'none', marker_size = 16, eps = 1e-3) :
    '''
    Plot ribosome pause density for a selected gene.
    :param pause: pause density dictionary.
    :param gene: gene to plot pauses for.
    :param additional: additional density plots to stack. Can be 'mrna', 'ribosome' or 'both'.
    :param marker_size: size of the marker to be used for plotting densities (densities are plotted as scatter plots for those codons, where measurements are available).
    '''
    if additional == 'both' :
        pylab.subplot(311)
    elif additional != 'none' :
        pylab.subplot(211)
    fig = pylab.gcf()
    fig.suptitle('Gene: %s' % gene, fontsize = 14)

    record = pause[gene]
    pause_array = np.atleast_1d(tools.get_codon_array(record))
    codon_len = len(pause_array)
    x = np.array(range(1, codon_len + 1))
    pause_color, mrna_color, rib_color = [], [], []
    for i in range(codon_len) :
        fractions = []
        tools.is_position_trusted(record, i, fraction_out = fractions)
        mrna_color.append(fractions[0])
        rib_color.append(fractions[1])
        pause_color.append(fractions[2])

    mrna_color = np.atleast_1d(mrna_color)
    rib_color = np.atleast_1d(rib_color)
    pause_color = np.atleast_1d(pause_color)

    cmap = pylab.cm.get_cmap('RdYlBu_r')
    im = pylab.scatter(x[pause_array > 0], pause_array[pause_array > 0], s = marker_size, c = pause_color[pause_array > 0], cmap = cmap, vmin = 0, vmax = 1) #, edgecolor = 'none')
    pylab.xlim(0, codon_len + 1)
    pylab.xlabel('Codon')
    pylab.ylabel('Pause')
    pylab.grid(True)

    plot_rib = False
    if additional == 'both' :
        plot_rib = True
        pylab.subplot(312)
    elif additional == 'ribosome' :
        plot_rib = True
        pylab.subplot(212)
    if plot_rib :
        rib_array = np.atleast_1d(tools.get_codon_array(pause[gene].rib))
        im = pylab.scatter(x[rib_array > eps], rib_array[rib_array > eps], s = marker_size, c = rib_color[rib_array > eps], cmap = cmap, vmin = 0, vmax = 1) #, edgecolor = 'none')
        pylab.xlim(0, codon_len + 1)
        pylab.xlabel('Codon')
        pylab.ylabel('Ribosome density')
        pylab.grid(True)

    plot_mrna = False
    if additional == 'both' :
        plot_mrna = True
        pylab.subplot(313)
    elif additional == 'mrna' :
        plot_mrna = True
        pylab.subplot(212)
    if plot_mrna :
        mrna_array = np.atleast_1d(tools.get_codon_array(pause[gene].mrna))
        im = pylab.scatter(x[mrna_array > eps], mrna_array[mrna_array > eps], s = marker_size, c = mrna_color[mrna_array > eps], cmap = cmap, vmin = 0, vmax = 1) #, edgecolor = 'none')
        pylab.xlim(0, codon_len + 1)
        pylab.xlabel('Codon')
        pylab.ylabel('mRNA density')
        pylab.grid(True)

    fig.subplots_adjust(right = 0.85)
    cax = fig.add_axes([0.9, 0.1, 0.03, 0.8])
    fig.colorbar(im, cax = cax)

    pylab.show()

def plot_cummulative_read_count_distribution(mrna, rib, is_density = False) :
    '''
    Plot cummulative read count distribution for mRNA and ribosome reads.
    :param mrna: mRNA density dictionary.
    :param rib: ribosome density dictionary.
    :param boolean is_density: a flag defining whether read count or density distribution is being plotted.
    '''
    totals_mrna = [tools.get_cds_count(gene) for gene in mrna.values()]
    totals_rib = [tools.get_cds_count(gene) for gene in rib.values()]

    totals_mrna.sort(reverse=True)
    totals_rib.sort(reverse=True)

    count = len(totals_mrna)
    cumsum_mrna = np.cumsum(totals_mrna) / np.sum(totals_mrna, dtype=np.float128)
    cumsum_rib = np.cumsum(totals_rib) / np.sum(totals_rib, dtype=np.float128)
    
    handles = pylab.plot(range(1, count + 1), cumsum_mrna, range(1, count + 1), cumsum_rib)
    if not is_density :
        pylab.legend(handles, ['mRNA reads', 'Ribosome reads'], loc = 4)
    else :
        pylab.legend(handles, ['mRNA density', 'Ribosme density'], loc = 4)
    pylab.ylim(0, 1.01)
    pylab.xlim(0, count + 1)
    pylab.xlabel('Gene')
    if not is_density :
        pylab.title('# mRNA reads = %s, # Ribosome reads = %s' % (format(np.sum(totals_mrna), ',.0f'), format(np.sum(totals_rib), ',.0f')))
        pylab.ylabel('Fraction of reads')
    else :
        pylab.title('Total mRNA density = %s, Total ribosome density = %s' % (format(np.sum(totals_mrna), ',.0f'), format(np.sum(totals_rib), ',.0f')))
        pylab.ylabel('Fraction of density')
    pylab.grid(True)
    pylab.show()

def plot_orf_read_count_histogram(mrna, rib, bar_width = 0.35) :
    '''
    Plot ORF read count histogram for mRNA and ribosome reads.
    :param mrna: mRNA ORF read counts.
    :param rib: ribosome ORF read counts.
    '''
    mrna_count = np.sum(mrna.values(), axis=0, dtype=np.float128)
    rib_count = np.sum(rib.values(), axis=0, dtype=np.float128)

    x = np.atleast_1d(range(3))
    mrna_count[:3] /= float(mrna_count[3])
    rib_count[:3] /= float(rib_count[3])
    pylab.hold(True)
    rib_handle = pylab.bar(x, rib_count[:3], bar_width, color='r')
    mrna_handle = pylab.bar(x + bar_width, mrna_count[:3], bar_width, color='b')
    pylab.legend((rib_handle, mrna_handle), ('Ribosome reads', 'mRNA reads'))
    pylab.xlim(0, 2 + 2 * bar_width)
    pylab.xticks(x + bar_width, ('0', '1', '2'))
    pylab.xlabel('ORF')
    pylab.ylabel('Fractions')
    pylab.title('# mRNA = %s, # Ribosome = %s' % (format(int(mrna_count[3]), ',d'), format(int(rib_count[3]), ',d')))
    pylab.show()

def plot_bias_histogram(density, gene, n_bins = 50, ymax = 0.2, xmin = -3.5, xmax = 3.5, drop_zeros = False) :
    '''
    Plots the sequencing bias (log of # times covered / avg. # times covered) histogram
    '''
    array = np.atleast_1d(tools.get_cds_array(density[gene])).astype(np.float128)
    if drop_zeros :
        array = array[array > 0]
    mean = np.mean(array, dtype = np.float128)
    if mean > 0 :
        array /= float(mean)
    log_array = np.log(array)

    n, bins, patches = pylab.hist(log_array, n_bins, histtype='stepfilled')
    plot.clf()
    width = bins[1:] - bins[:-1]
    pylab.bar(bins[:-1],  n / float(log_array.size), width = width) 
    pylab.xlabel('Log bias (count / avg. count)') 
    pylab.ylabel('Fraction of bases')
    pylab.title('Gene: %s' % gene)
    pylab.ylim(0, ymax)
    pylab.xlim(xmin, xmax)
    plot.show()

def plot_autocorrelation(mrna, rib, gene) :
    '''
    Plot autocorrelation plots for mRNA and ribosome density signals on top of each other.
    :param mrna: mRNA alignment list.
    :param rib: ribosome alignment list.
    :param string gene: name of the gene to plot densities for.
    '''
    mrna_array = tools.get_cds_array(mrna[gene])
    rib_array = tools.get_cds_array(rib[gene])

    fig, axes = pylab.subplots(nrows = 2, ncols = 1)
    pandas.tools.plotting.autocorrelation_plot(mrna_array, ax = axes[0])
    pandas.tools.plotting.autocorrelation_plot(rib_array, ax = axes[1])
    axes[0].set_ylim(-1, 1)
    axes[0].set_title('mRNA autocorrelation')
    axes[1].set_ylim(-1, 1)
    axes[1].set_title('Ribosome autocorrelation')
    fig = pylab.gcf()
    fig.suptitle('Gene: %s' % gene, fontsize =  14)

    pylab.show()

def plot_average_density(density, length_cutoff = 0, max_length_cutoff = float('Inf'), density_cutoff = 0.2, cds_only = False, normalize_by_mean = False, xmax = None, marker_size = 4) :
    '''
    Plot average density profile across the genes.
    :param density: density dictionary.
    :param length_cutoff: minimum gene length.
    :param max_length_cutoff: maximum gene length.
    :param density_cutoff: average density cutoff to use for plotting genes.
    :param cds_only: plot only for the CDS part of the array?
    :param normalize_by_mean: a flag defining whether density profiles should be normalized by the mean.
    :param xmax: x-axis limit.
    :param marker_size: size of the marker to use for plotting.
    '''
    expected_length = 0
    selected_genes = []
    for gene in density.keys() :
        gene_array = tools.get_cds_array(density[gene]) if cds_only else density[gene].mrna.array
        length = len(gene_array)
        average = np.sum(gene_array) / float(length)
        if average >= density_cutoff and length >= length_cutoff and length <= max_length_cutoff :
            expected_length = max(expected_length, length)
            selected_genes.append(gene)

    array = [0] * expected_length
    counts = [0] * expected_length

    for gene in selected_genes:
        gene_array = tools.get_cds_array(density[gene]) if cds_only else density[gene].mrna.array
        length = len(gene_array)
        mean = np.sum(gene_array) / float(length)
        for i in range(length) :
            if normalize_by_mean :
                array[i] += density[gene].mrna.array[i] / float(mean)
            else :
                array[i] += density[gene].mrna.array[i]
            counts[i] += 1
        
    array, counts = np.atleast_1d(array), np.atleast_1d(counts)
    array /= counts.astype(np.float)

    x = np.array(range(1, expected_length + 1))

    if xmax is None :
        xmax = expected_length + 1
    fig = pylab.gcf()
    pylab.subplot(211)
    pylab.scatter(x, array, s=marker_size)
    pylab.xlim(0, xmax)
    pylab.xlabel('Position')
    pylab.ylabel('Average density')
    pylab.grid(True)

    pylab.subplot(212)
    pylab.scatter(x, counts, s=marker_size)
    pylab.xlim(0, xmax)
    pylab.xlabel('Position')
    pylab.ylabel('Number of genes')
    pylab.grid(True)
    pylab.show()

def plot_simulation(solution, gene = None, marker_size = 16, plot_data = True, log_space = False) :
    '''
    Plot simulation results.
    :param solution: a single solution or a dictionary of solutions (for gene group fitters).
    :param gene: name of the gene to plot the simulation results for. If None, results are plotted for the first gene.
    :param marker_size: size of the marker to be used for plotting densities (densities are plotted as scatter plots for those codons, where measurements are available)
    :param plot_data: a boolean flag defining whether the data (scatter) should also be plotted.
    :param log_sapce: a boolean flag defining whether the data should be plotter in log space.
    '''
    fig = pylab.gcf()
    if gene is not None :
        record, initiation, simulation, fitness = solution.record[gene], solution.initiation[gene], solution.simulation[gene], solution.fitness[gene]
        scale = solution.scale if isinstance(solution.scale, float) else solution.scale[gene]
    else :
        if not isinstance(solution.record, list) :
            record, initiation, simulation, fitness, scale = solution.record, solution.initiation, solution.simulation, solution.fitness, solution.scale
        else :
            genes = solution.record.keys()
            gene = genes[0]
            record, initiation, simulation, fitness = solution.record[gene], solution.initiation[gene], solution.simulation[gene], solution.fitness[gene]
            scale = solution.scale if isinstance(solution.scale, float) else solution.scale[gene]
    
    initiation = simulator.RateFunction.sigmoid(initiation)

    if gene is not None :
        fig.suptitle('Gene: %s - Init: %.3f - Scale: %.3f - Fitness: %.5f' % (gene, initiation, scale, fitness), fontsize = 14)
    else :
        fig.suptitle('Init: %.3f - Scale: %.3f - Fitness: %.5f' % (initiation, scale, fitness), fontsize = 14)
   
    codon_len = len(simulation)
    x = np.array(range(1, codon_len + 1))
    if plot_data :
        pylab.hold(True)
        if not record.__dict__.has_key('array') :
            for node in record.tree :
                left, right, ratio_average_log2, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log = node
                length = right - left + 1
                mean_occupancy = scale * np.sum(simulation[left:right + 1]) / float(length)
                mean_occupancy_log2 = math.log(mean_occupancy, 2)
                mean_occupancy_log = math.log(mean_occupancy)
                ratio_average = 2 ** ratio_average_log2
                ratio_average_log = math.log(ratio_average)
                sigma = record.sigma

                if not log_space :
                    pylab.plot([left, right], [mean_occupancy, mean_occupancy], 'r', linewidth = 2)
                    pylab.plot([left, right], [ratio_average, ratio_average], 'g', linewidth = 2)
                    diff = mean_occupancy - ratio_average
                    print '[%3d, %3d] len = %3d %.2f -> %.2f (diff: %5.2f)' % (left, right, length, mean_occupancy, ratio_average, diff)
                else :    
                    pylab.plot([left, right], [mean_occupancy_log2, mean_occupancy_log2], 'r', linewidth = 2)
                    pylab.plot([left, right], [ratio_average_log2, ratio_average_log2], 'g', linewidth = 2)
                    diff = mean_occupancy_log2 - ratio_average_log2
                    diff_p = -abs(mean_occupancy_log - ratio_average_log)
                    p_value = scipy.stats.norm(0, sigma).cdf(diff_p) * 2
                    print '[%3d, %3d] len = %3d %.2f -> %.2f (diff: %5.2f, p %.5f)' % (left, right, length, mean_occupancy_log2, ratio_average_log2, diff, p_value)
        else :
            pause_array = np.atleast_1d(tools.get_codon_array(record))
            pause_color = []
            for i in range(codon_len) :
                fractions = []
                tools.is_position_trusted(record, i, fraction_out = fractions)
                pause_color.append(fractions[2])

            pause_color = np.atleast_1d(pause_color)

            cmap = pylab.cm.get_cmap('RdYlBu_r')
            im = pylab.scatter(x[pause_array > 0], pause_array[pause_array > 0], s = marker_size, c = pause_color[pause_array > 0], cmap = cmap, vmin = 0, vmax = 1) 
    if log_space :
        pylab.plot(x, np.log2(scale * simulation))
    else :
        pylab.plot(x, scale * simulation)
    pylab.xlim(0, codon_len + 1)
    pylab.xlabel('Codon')
    pylab.ylabel('Pause')
    pylab.grid(True)

    if plot_data and record.__dict__.has_key('array') :
        fig.subplots_adjust(right = 0.85)
        cax = fig.add_axes([0.9, 0.1, 0.03, 0.8])
        fig.colorbar(im, cax = cax)

    pylab.show()
