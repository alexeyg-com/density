import numpy as np
import math
import sys

import tools

def update_sigma_length_groups(forest, sigmas) :
    '''
    Update forest sigma estimates based on standard deviations estimated per segment length group.
    :param forest: segment tree forest that has to be updated.
    :param sigma: a list with length group biological variance estimates.
    '''
    genes = forest.keys()
    for gene in genes :
        tree = forest[gene]
        n_nodes = len(tree.tree)
        for i in range(n_nodes) :
            node = tree.tree[i]
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            length = right - left + 1
            node_sigma = tools.get_sigma_for_length(length, sigmas)
            node = (left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma)
            tree.tree[i] = node

def get_codon_sequence(tree) :
    '''
    Returns the sequence of the CDS at codon resolution.
    :param tree: tree record to return the sequence for.
    '''
    array = tree.seq
    length = len(array) - tree.start_prefix
    length_codons = length / 3
    array_codons = np.zeros((length_codons, ), dtype = 'S4')
    for i in range(0, length_codons) :
        array_codons[i] = array[tree.start_prefix + 3 * i : tree.start_prefix + 3 * (i + 1)]
    return array_codons

class CumSum(object) :
    '''
    A wrapper for range sum querying.
    '''

    def __init__(self, array, offset_left = 0, offset_right = 0) :
        '''
        Class constructor.
        Compute internal cumulative sum array.
        :param offset_left: left offset for computing sum.
        :param offset_right: right offset for computing sum.
        '''
        self._array = np.cumsum(array)
        self._array = np.insert(self._array, 0, 0)
        self.offset_left = offset_left
        self.offset_right = offset_right

    def __getitem__(self, index) :
        '''
        Return sum from i to j.
        :param i: start index.
        :param j: end index.
        '''
        if isinstance(index, tuple) :
            i, j = index
        else :
            i, j = index, index
        i = max(i - self.offset_left, 0)
        j = max(j - self.offset_right, 0)
        return self._array[j + 1] - self._array[i]

class TreeEvaluator(object) :
    '''
    A class containing methods for profile tree segmentation and evaluation.
    '''

    def __init__(self, mrna_a, mrna_b, rib_a, rib_b, n_mrna_a, n_mrna_b, n_rib_a, n_rib_b, min_length = 20, threshold_mrna = 128, threshold_rib = 64, bad_threshold = 0.2) :
        '''
        Class constructor.
        :param mrna_a: mRNA array gene record for replicate A.
        :param mrna_b: mRNA array gene record for replicate B.
        :param rib_a: ribosome array gene record for replicate A.
        :param rib_b: ribosome array gene record for replicate B.
        :param n_mrna_a: number of mRNA reads in replicate A.
        :param n_mrna_b: number of mRNA reads in replicate B.
        :param n_rib_a: number of ribosome reads in replicate A.
        :param n_rib_b: number of ribosome reads in replicate B.
        :param min_length: minimum allowed tree segment length.
        :param threshold_mrna: between replicates mRNA read count threshold for accepting tree nodes.
        :param threshold_rib: between replicates ribosome read count threshold for accepting tree nodes.
        :param bad_threshold: a cutoff for the allowed fraction of bad data (ambiguous or overlapping reads).
        '''
        self.mrna_a = mrna_a
        self.mrna_b = mrna_b
        self.rib_a = rib_a
        self.rib_b = rib_b
        self.n_mrna_a = float(n_mrna_a)
        self.n_mrna_b = float(n_mrna_b)
        self.n_rib_a = float(n_rib_a)
        self.n_rib_b = float(n_rib_b)

        self._process_arrays(['mrna_a', 'mrna_b', 'rib_a', 'rib_b'])
        self._build_tree(min_length, threshold_mrna, threshold_rib, bad_threshold)
  
    def compute_scale(self, occupancy) :
        n_segments = len(self.tree)
        if not isinstance(occupancy, list) :
            n = CumSum(occupancy)
            segment_occupancy = []
            for i in xrange(n_segments) :
                node = self.tree[i]
                left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
                segment_occupancy.append(n[left, right])
        else :
            segment_occupancy = occupancy
        
        numerator, denominator = 0.0, 0.0
        for i in xrange(n_segments) :
            node = self.tree[i]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            mu = ratio_average_log / math.log(math.exp(1.0), 2)
            length = right - left + 1
            mean_occupancy = segment_occupancy[i] / float(length)
            lnx = np.log(mean_occupancy) 
            weight = 1.0 / sigma ** 2
            numerator += weight * (mu + sigma ** 2 - lnx)
            denominator += weight
        return (numerator, denominator)
    
    def evaluate(self, scale, occupancy) :
        if scale is None :
            numerator, denominator = self.compute_scale(occupancy)
            scale = numerator / float(denominator)

        n_segments = len(self.tree)
        if not isinstance(occupancy, list) :
            n = CumSum(occupancy)
            segment_occupancy = []
            for i in xrange(n_segments) :
                node = self.tree[i]
                left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
                segment_occupancy.append(n[left, right])
        else :
            segment_occupancy = occupancy
        
        likelihood = 0.0
        for i in xrange(n_segments) :
            node = self.tree[i]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            mu = ratio_average_log / math.log(math.exp(1.0), 2)
            length = right - left + 1
            mean_occupancy = segment_occupancy[i] / float(length)
            lnx = np.log(mean_occupancy) + scale
            p =  -1.0 / (2 * sigma ** 2) * (lnx - mu) ** 2 + lnx
            likelihood += p
        return likelihood
    
    def evaluate_scalefree(self, occupancy) :
        n_segments = len(self.tree)
        if not isinstance(occupancy, list) :
            n = CumSum(occupancy)
            segment_occupancy = []
            for i in xrange(n_segments) :
                node = self.tree[i]
                left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
                segment_occupancy.append(n[left, right])
        else :
            segment_occupancy = occupancy
        
        result = np.zeros((3,))
        for i in xrange(n_segments) :
            node = self.tree[i]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            mu = ratio_average_log / math.log(math.exp(1.0), 2)
            length = right - left + 1
            mean_occupancy = segment_occupancy[i] / float(length)
            lnx = np.log(mean_occupancy)
            weight = 1.0 / (sigma ** 2)
            result[0] += -0.5 * weight
            result[1] += (mu - lnx) * weight + 1
            result[2] += -0.5 * weight * (lnx - mu) ** 2 + lnx
        return result
    
    def _find_cut_point(self, left_codon, right_codon, min_length) :
        '''
        Find array cut point.
        :param left_codon: left edge of the array.
        :pram right_codon: right edge of the array (inclusive).
        :param min_length: minimum allowed tree segment length.
        '''
        best_i, best_objective, best_over_threshold = left_codon, sys.maxint, 0
        for j in range(left_codon, right_codon) :
            left = 3 * left_codon
            right = 3 * (right_codon + 1) - 1
            i = 3 * (j + 1) - 1
            i_plus = 3 * (j + 1)
            cur_left, cur_right = i - left + 1, right - i - 1 + 1
            objective_a = abs(self.mrna_a[left, i] + self.rib_a[left, i] - self.mrna_a[i_plus, right] - self.rib_a[i_plus, right])
            objective_b = abs(self.mrna_b[left, i] + self.rib_b[left, i] - self.mrna_b[i_plus, right] - self.rib_b[i_plus, right])
            objective = objective_a + objective_b
            over_threshold = 0
            if cur_left >= min_length :
                over_threshold += 1
            if cur_right >= min_length :
                over_threshold += 1
            if (over_threshold > best_over_threshold) or (over_threshold == best_over_threshold and objective < best_objective) :
                best_i = j
                best_objective = objective
                best_over_threshold = over_threshold
        return (best_i, best_over_threshold)
    
    def _process_segment(self, left_codon, right_codon, min_length, threshold_mrna, threshold_rib, bad_threshold) :
        '''
        Processes array segment [left, right] into a tree node. Returns true is a node has been created.
        :param left_codon: left edge of the array.
        :param right_codon: right edge of the array.
        :param min_length: minimum allowed tree segment length.
        :param threshold_mrna: between replicates mRNA read count threshold for accepting tree nodes.
        :param threshold_rib: between replicates ribosome read count threshold for accepting tree nodes.
        :param bad_threshold: a cutoff for the allowed fraction of bad data (ambiguous or overlapping reads).
        '''
        length = right_codon - left_codon + 1
        if length < min_length :
            return False
        left = left_codon * 3
        right = (right_codon + 1) * 3 - 1
        sum_mrna_a = self.mrna_a[left, right]
        sum_mrna_b = self.mrna_b[left, right]
        mean_mrna_a = sum_mrna_a / float(length)
        mean_mrna_b = sum_mrna_b / float(length)
        sum_rib_a = self.rib_a[left, right] 
        sum_rib_b = self.rib_b[left, right] 
        mean_rib_a = sum_rib_a / float(length)
        mean_rib_b = sum_rib_b / float(length)
        sum_mrna_a_norm = mean_mrna_a / self.n_mrna_a
        sum_mrna_b_norm = mean_mrna_b / self.n_mrna_b
        sum_rib_a_norm = mean_rib_a / self.n_rib_a
        sum_rib_b_norm = mean_rib_b / self.n_rib_b
        
        sum_mrna_a_bad = self.mrna_a_bad[left, right]
        sum_mrna_b_bad = self.mrna_b_bad[left, right]
        sum_rib_a_bad = self.rib_a_bad[left, right] 
        sum_rib_b_bad = self.rib_b_bad[left, right] 

        if sum_mrna_a_norm > 0 and sum_mrna_b_norm > 0 and sum_rib_a_norm > 0 and sum_rib_b_norm > 0 :
            ratio_a = sum_rib_a_norm / sum_mrna_a_norm
            ratio_b = sum_rib_b_norm / sum_mrna_b_norm
            ratio_a_log = math.log(ratio_a, 2)
            ratio_b_log = math.log(ratio_b, 2)
            ratio_average_log = 0.5 * (ratio_a_log + ratio_b_log)
            if sum_mrna_a + sum_mrna_b < threshold_mrna or sum_rib_a + sum_rib_b < threshold_rib :
                print 'FAIL [%3d %3d] len = %3d : ratio %.5f (%.5f, %.5f), mRNA (%4d, %4d), rib (%4d, %4d)' % (left_codon, right_codon, length, ratio_average_log, ratio_a_log, ratio_b_log, sum_mrna_a, sum_mrna_b, sum_rib_a, sum_rib_b)
                return False
            sum_mrna_a_log = math.log(sum_mrna_a_norm, 2)
            sum_mrna_b_log = math.log(sum_mrna_b_norm, 2)
            sum_mrna_average_log = 0.5 * (sum_mrna_a_log + sum_mrna_b_log)
            sum_rib_a_log = math.log(sum_rib_a_norm, 2)
            sum_rib_b_log = math.log(sum_rib_b_norm, 2)
            sum_rib_average_log = 0.5 * (sum_rib_a_log + sum_rib_b_log)
            if sum_mrna_a_bad / float(sum_mrna_a) < bad_threshold and sum_mrna_b_bad / float(sum_mrna_b) < bad_threshold and sum_rib_a_bad / float(sum_rib_a) < bad_threshold and sum_rib_b_bad / float(sum_rib_b) < bad_threshold :
                self.tree.append((left_codon, right_codon, ratio_average_log, ratio_a_log - ratio_b_log, sum_mrna_average_log, sum_mrna_a_log - sum_mrna_b_log, sum_rib_average_log, sum_rib_a_log - sum_rib_b_log, sum_mrna_a + sum_mrna_b, sum_rib_a + sum_rib_b, None))
                print 'ADD  [%3d %3d] len = %3d : ratio %.5f (%.5f, %.5f), mRNA (%4d, %4d), rib (%4d, %4d)' % (left_codon, right_codon, length, ratio_average_log, ratio_a_log, ratio_b_log, sum_mrna_a, sum_mrna_b, sum_rib_a, sum_rib_b)
            else :
                print 'SKIP [%3d %3d] len = %3d : ratio %.5f (%.5f, %.5f), mRNA (%4d, %4d), rib (%4d, %4d)' % (left_codon, right_codon, length, ratio_average_log, ratio_a_log, ratio_b_log, sum_mrna_a, sum_mrna_b, sum_rib_a, sum_rib_b)
            return True

        print 'FAIL [%3d %3d] len = %3d : mRNA (%4d, %4d), rib (%4d, %4d)' % (left_codon, right_codon, length, sum_mrna_a, sum_mrna_b, sum_rib_a, sum_rib_b)
        return False
    
    def _build_tree(self, min_length, threshold_mrna, threshold_rib, bad_threshold) :
        '''
        Build an evaluation tree to be used further.
        :param min_length: minimum allowed tree segment length.
        :param threshold_mrna: between replicates mRNA read count threshold for accepting tree nodes.
        :param threshold_rib: between replicates ribosome read count threshold for accepting tree nodes.
        :param bad_threshold: a cutoff for the allowed fraction of bad data (ambiguous or overlapping reads).
        '''
        self.tree = []
        stack = [(0, self.length_codons - 1)]
        while len(stack) > 0 :
            left, right = stack.pop()
            if self._process_segment(left, right, min_length, threshold_mrna, threshold_rib, bad_threshold) :
                cut, over_threshold = self._find_cut_point(left, right, min_length)
                #print 'Using cut: [%d %d] and [%d %d]' % (left, cut, cut + 1, right)
                if over_threshold > 1 :
                    stack.append((left, cut))
                    stack.append((cut + 1, right))

    def _process_arrays(self, names) :
        '''
        A wrapper for processing record into count arrays. This function works directly with Python objects _dict_ to makes things a little bit easier.
        :param names: a list of field names that need to processed from gene records into count arrays.
        '''
        name = names[0]
        record = self.__dict__[name]
        self.seq = record.mrna.seq
        self.start_prefix = record.start_prefix
        self.end_prefix = record.end_prefix
        self.length_codons = (len(self.seq) - record.start_prefix) / 3

        for name in names :
            record = self.__dict__[name]
            array = record.mrna.array
            array_ambiguous = record.mrna.array_ambiguous
            array_overlapping = record.mrna.array_overlapping
            self.__dict__[name] = CumSum(array, offset_left = self.start_prefix, offset_right = self.end_prefix)
            self.__dict__[name+'_bad'] = CumSum(array_ambiguous + array_overlapping, offset_left = self.start_prefix, offset_right = self.end_prefix)

