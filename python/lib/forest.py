import statsmodels.api as sm
import reader
import tools
import tree as tree_module

import numpy as np
import math

def build_forest(mrna_a, mrna_b, rib_a, rib_b, n_mrna_a = 1335655, n_mrna_b = 3387974, n_rib_a = 1622351, n_rib_b = 2927416, min_length = 20, threshold_mrna = 128, threshold_rib = 64, bad_threshold = 0.2) :
    '''
    Build a forest of gene trees.
    :param mrna_a: mRNA array gene records for replicate A.
    :param mrna_b: mRNA array gene records for replicate B.
    :param rib_a: ribosome array gene records for replicate A.
    :param rib_b: ribosome array gene records for replicate B.
    :param n_mrna_a: number of CDS-mapped reads in mRNA replicate A.
    :param n_mrna_b: number of CDS-mapped reads in mRNA replicate B.
    :param n_rib_a: number of CDS-mapped reads in ribosome replicate A.
    :param n_rib_b: number of CDS-mapped reads in ribosome replicate B.
    :param min_length: minimum allowed tree segment length.
    :param threshold_mrna: between replicates mRNA read count threshold for accepting tree nodes.
    :param threshold_rib: between replicates ribosome read count threshold for accepting tree nodes.
    :param bad_threshold: a cutoff for the allowed fraction of bad data (ambiguous or overlapping reads).
    '''
    genes_a = tools.overlap_gene_lists(mrna_a.keys(), rib_a.keys())
    genes_b = tools.overlap_gene_lists(mrna_b.keys(), rib_b.keys())
    genes = tools.overlap_gene_lists(genes_a, genes_b)
    forest = {}
    n_genes = 0
    n_segments = 0
    for gene in genes :
        tree = tree_module.TreeEvaluator(mrna_a[gene], mrna_b[gene], rib_a[gene], rib_b[gene], n_mrna_a, n_mrna_b, n_rib_a, n_rib_b, min_length = min_length, threshold_mrna = threshold_mrna, threshold_rib = threshold_rib, bad_threshold = bad_threshold)
        if len(tree.tree) == 0 :
            continue
        forest[gene] = tree
        n_genes += 1
        n_segments += len(tree.tree)

    print 'INFO: %d segments in %d genes' % (n_segments, n_genes)
    
    return forest

def compute_length_group_standard_deviations(forest, n_bins = 10) :
    '''
    Split segments into groups and estimate per-groups ratio standard deviations (via the M values).
    '''
    results = []
    genes = forest.keys()
    lengths, M = [], []
    for gene in genes :
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            length = right - left + 1
            lengths.append(length)
            M.append(ratio_m)
    
    pre_sort = np.array([lengths, M]).T
    pre_sort = np.array(sorted(pre_sort, key = lambda x : x[0])).T
    lengths = pre_sort[0]
    M = pre_sort[1] / math.log(math.exp(1.0), 2)
    
    n = lengths.shape[0]
    per_bin = n / float(n_bins)
    int_pos_prev = 0
    pos = 0
    while pos < n :
        pos = min(per_bin + pos, n)
        int_pos = int(pos)
        if int_pos < n :
            val = lengths[int_pos]
        else :
            val = lengths[n - 1] + 1
        prev_val = lengths[int_pos_prev]
        indices = (lengths >= prev_val) * (lengths < val)
        bin_data = M[indices]
        sigma = np.std(bin_data) / math.sqrt(2)
        results.append((prev_val, val, sigma))
        int_pos_prev = int_pos
    return results

def attach_sigma_length_groups(forest) :
    '''
    Estimate standard deviation for segment length groups and attach it to forest node.
    :param forest: segment tree forest that has to be updated.
    '''
    print '[i] Estimating standard deviations for segment length groups'
    sigmas = compute_length_group_standard_deviations(forest)
    n_sigmas = len(sigmas)
    for i in range(n_sigmas) :
        print '\t[i] %2d : [%4d, %4d) : sigmas = %.2f' % (i + 1, sigmas[i][0], sigmas[i][1], sigmas[i][2])
    print '[i] Updating tree with new estimates'
    tree_module.update_sigma_length_groups(forest, sigmas)

def correct_bias_all(forest, frac = 0.33, iter = 3, repeats = 1) :
    '''
    Repeatedly correct mRNA-Rib and ratio biases.
    :param frac: Lowess parameter - fraction of data to use for each point.
    :param iter: Lowess parameter - number of iterations.
    :param repeats: Number of times the correction has to be performed.
    '''
    for r in range(repeats) :
        print '== Bias correction, iteration %d ==' % (r + 1)
        correct_bias(forest, frac = frac, iter = iter)
        correct_bias_ratio(forest, frac = frac, iter = iter)

def correct_bias_ratio(forest, frac = 0.33, iter = 3) :
    '''
    Correct MA-bias in ratios estimated DIRECTLY using lowess regression.
    :param frac: Lowess parameter - fraction of data to use for each point.
    :param iter: Lowess parameter - number of iterations.
    '''
    genes = forest.keys()
    M, A = [], []
    for gene in genes :
        tree = forest[gene]
        for node in tree.tree :
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            M.append(ratio_m_log)
            A.append(ratio_average_log)
    M = np.atleast_1d(M)
    A = np.atleast_1d(A)
    lowess = sm.nonparametric.lowess
    Z = lowess(M, A, frac = frac, it = iter, return_sorted = False)
    counter = 0
    for gene in genes :
        tree = forest[gene]
        n_nodes = len(tree.tree)
        for i in range(n_nodes) :
            node = tree.tree[i]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            correction = Z[counter]
            print 'Correcting: %5.2f - %5.2f / 2 = %5.2f' % (ratio_average_log, correction, ratio_average_log - correction / 2.0)
            node = (left, right, ratio_average_log - correction / 2.0, ratio_m_log - correction, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma)
            tree.tree[i] = node
            counter += 1

def correct_bias(forest, frac = 0.33, iter = 3) :
    '''
    Correct MA-bias using lowess regression.
    :param frac: Lowess parameter - fraction of data to use for each point.
    :param iter: Lowess parameter - number of iterations.
    '''
    genes = forest.keys()
    mrna_M, mrna_A = [], []
    rib_M, rib_A = [], []
    for gene in genes :
        tree = forest[gene]
        for node in tree.tree :
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            mrna_M.append(mrna_m_log)
            mrna_A.append(mrna_average_log)
            rib_M.append(rib_m_log)
            rib_A.append(rib_average_log)
    mrna_M = np.atleast_1d(mrna_M)
    mrna_A = np.atleast_1d(mrna_A)
    rib_M = np.atleast_1d(rib_M)
    rib_A = np.atleast_1d(rib_A)
    lowess = sm.nonparametric.lowess
    mrna_Z = lowess(mrna_M, mrna_A, frac = frac, it = iter, return_sorted = False)
    rib_Z = lowess(rib_M, rib_A, frac = frac, it = iter, return_sorted = False)
    counter = 0
    ratio_corrections = []
    for gene in genes :
        tree = forest[gene]
        n_nodes = len(tree.tree)
        for i in range(n_nodes) :
            node = tree.tree[i]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            mrna_correction = mrna_Z[counter]
            rib_correction = rib_Z[counter]
            print '[mRNA]  Correcting: %.2f - %.2f / 2 = %.2f' % (mrna_average_log, mrna_correction, mrna_average_log - mrna_correction / 2.0)
            print '[Rib]   Correcting: %.2f - %.2f / 2 = %.2f' % (rib_average_log, rib_correction, rib_average_log - rib_correction / 2.0)
            print '[Ratio] Correcting: %.2f - (%.2f - %.2f) / 2 = %.2f - %.2f / 2 = %.2f' % (ratio_average_log, rib_correction, mrna_correction, ratio_average_log, rib_correction - mrna_correction, ratio_average_log - (rib_correction - mrna_correction) / 2.0)
            node = (left, right, ratio_average_log - (rib_correction - mrna_correction) / 2.0, ratio_m_log - rib_correction + mrna_correction, mrna_average_log - mrna_correction / 2.0, mrna_m_log - mrna_correction, rib_average_log - rib_correction / 2.0, rib_m_log - rib_correction, mrna_cnt, rib_cnt, node_sigma)
            tree.tree[i] = node
            ratio_corrections.append((gene, i, ratio_average_log, - (rib_correction - mrna_correction) / 2.0))
            counter += 1
    return ratio_corrections

def estimate_standard_deviation(forest, use_log2 = False) :
    '''
    Estimate standard deviation in forest data.
    :param forest: Forest to estimate standard deviation for.
    :param use_log2: if True then estimates in log2 space are produced.
    '''
    genes = forest.keys()
    M_ratio, M_mrna, M_rib = [], [], []
    for gene in genes :
        tree = forest[gene]
        for node in tree.tree :
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            M_ratio.append(ratio_m_log)
            M_mrna.append(mrna_m_log)
            M_rib.append(rib_m_log)
    M_mrna = np.atleast_1d(M_mrna)
    M_rib = np.atleast_1d(M_rib)
    M_ratio = np.atleast_1d(M_ratio)
    if not use_log2 :
        M_mrna /= math.log(math.exp(1.0), 2)
        M_rib /= math.log(math.exp(1.0), 2)
        M_ratio /= math.log(math.exp(1.0), 2)
    print '[mRNA]'
    print 'Mean: %.5f' % np.mean(M_mrna)
    print 'Variance: %.5f' % np.var(M_mrna)
    print 'Std. dev: %.5f' % np.std(M_mrna)
    print 'Var. of measurement: %.5f' % (np.var(M_mrna) / 2.0)
    print 'Std. of measurement: %.5f\n' % math.sqrt(np.var(M_mrna) / 2.0)
    print '[Rib]'
    print 'Mean: %.5f' % np.mean(M_rib)
    print 'Variance: %.5f' % np.var(M_rib)
    print 'Std. dev: %.5f' % np.std(M_rib)
    print 'Var. of measurement: %.5f' % (np.var(M_rib) / 2.0)
    print 'Std. of measurement: %.5f\n' % math.sqrt(np.var(M_rib) / 2.0)
    print '[Ratio]'
    print 'Mean: %.5f' % np.mean(M_ratio)
    print 'Variance: %.5f' % np.var(M_ratio)
    print 'Std. dev: %.5f' % np.std(M_ratio)
    print 'Var. of measurement: %.5f' % (np.var(M_ratio) / 2.0)
    print 'Std. of measurement: %.5f\n' % math.sqrt(np.var(M_ratio) / 2.0)
    
    var_mrna_meas = np.var(M_mrna) / 2.0
    var_rib_meas = np.var(M_rib) / 2.0
    var_ratio_meas = var_mrna_meas + var_rib_meas
    print '[Ratio from mRNA and Rib]'
    print 'Var. of measurement: %.5f' % var_ratio_meas
    print 'Std. of measurement: %.5f' % math.sqrt(var_ratio_meas)

def estimate_pooled_standard_deviation(forest, type = 'ratio', n_bins = 100, use_log2 = False) :
    '''
    Estimate pooled standard deviation in forest data.
    :param forest: segment forest to estimate standard deviation for.
    :param type: defines data for which the estimate has to be obtained. Can be 'ratio', 'mrna' or 'rib'.
    :param n_bins: number of bins to split data into for the per-group estimates.
    :param use_log2: if True then estimates in log2 space are produced.
    '''
    genes = forest.keys()
    M, A = [], []
    a, b = [], []
    for gene in genes :
        tree = forest[gene]
        for node in tree.tree :
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            if type == 'ratio' :
                _m = ratio_m_log
                _a = ratio_average_log
            elif type == 'mrna' :
                _m = mrna_m_log
                _a = mrna_average_log
            elif type == 'rib' :
                _m = rib_m_log
                _a = rib_average_log
            M.append(_m)
            A.append(_a)

            c = _m
            d = 2 * _a
            one = 0.5 * (c + d)
            two = -0.5 * (c - d)
            a.append(one)
            b.append(two)
    M = np.atleast_1d(M)
    A = np.atleast_1d(A)
    a = np.atleast_1d(a)
    b = np.atleast_1d(b)
    if not use_log2 :
        M /= math.log(math.exp(1.0), 2)
        A /= math.log(math.exp(1.0), 2)
        a /= math.log(math.exp(1.0), 2)
        b /= math.log(math.exp(1.0), 2)

    merge = np.atleast_2d([M, A, a, b])
    merge = merge.transpose()
    merge = sorted(merge, key = lambda x : x[1])
    merge = np.atleast_2d(merge)
    merge = merge.transpose()
    M = merge[0]
    A = merge[1]
    a = merge[2]
    b = merge[3]

    length = M.size
    step = length / float(n_bins)
    start, stop = 0, 0
    bin_var = []
    diff_var = []
    bin_size = []
    for i in range(n_bins) :
        stop += step
        _start = int(start)
        _stop = int(stop)
        current_M = M[_start:_stop]
        current_A = A[_start:_stop]
        current_a = a[_start:_stop]
        current_b = b[_start:_stop]
        estimate = np.concatenate((current_a, current_b))
        cov = np.cov([current_a, current_b])
        print '[%5d %5d] %3d - (%5.2f %5.2f) - %5.2f : %5.2f +/- %5.2f | from diff %5.2f' % (_start, _stop, _stop - _start, np.min(current_A), np.max(current_A), np.mean(current_A), np.mean(estimate), np.std(estimate), math.sqrt(np.var(current_M) / 2.0))
        print 'Mean M: %.2f' % np.mean(current_M)
        start += step
        bin_var.append(np.var(estimate))
        diff_var.append(np.var(current_M) / 2.0)
        bin_size.append(_stop - _start)
    
    bin_size = np.atleast_1d(bin_size)
    bin_var = np.atleast_1d(bin_var)
    print 'Pooled std: %.5f' % math.sqrt(np.dot(bin_size - 1, bin_var) / float(np.sum(bin_size) - bin_size.size))
    print 'Pooled diff. std: %.5f' % math.sqrt(np.dot(bin_size - 1, diff_var) / float(np.sum(bin_size) - bin_size.size))

def filter_dubious(pause, dubious = '../results/dubious.txt') :
    '''
    Filter out dubious genes.
    :param pause: dictionary with computed gene entries.
    :param dubious: a list of dubious genes or a name of a file containing the gene list.
    '''
    if isinstance(dubious, basestring) :
        dubious = reader.read_list(dubious)

    result = {}
    for gene in pause.keys() :
        if not gene in dubious :
            result[gene] = pause[gene]

    return result

def filter_mitochondrial(pause, mitochondrial = '../results/mitochondrial.txt') :
    '''
    Filter out mitochondrial genes.
    :param pause: dictionary with computed gene entries.
    :param dubious: a list of mitochondrial genes or a name of a file containing the gene list.
    '''
    return filter_dubious(pause, mitochondrial)

