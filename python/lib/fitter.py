'''
A module with a collection of fitting functions.

Alexey Gritsenko
29-02-2014
'''

import math
import time
import xmlrpclib
import datetime
import cma
import numpy as np
import sysv_ipc
import sys

import tools
import simulator
import scheduler
import writer
from SimpleNamespace import SimpleNamespace
from tools import ParallelTimeout
from IPython.parallel import Client

_RETRY_SLEEP = 30
_RESTART_SLEEP = 10
_WRITE_FREQUENCY = 1
_MIN_ITERATIONS = 300

_SEMAPHORE_KEY = 691989
_SEMAPHORE_VALUE = 100

_semaphore = None
def _initialize_semaphore() :
    global _semaphore
    try :
        sem = sysv_ipc.Semaphore(_SEMAPHORE_KEY, flags = sysv_ipc.IPC_CREX, initial_value = _SEMAPHORE_VALUE)
    except :
        sem = sysv_ipc.Semaphore(_SEMAPHORE_KEY)
    _semaphore = sem

def _cma_termination_callback(es) :
    '''
    A callback function for stopping CMA optimization if the best solution was not updated for more than X iterations.
    :param es: evolutionary strategy object.
    '''
    if es._cma_best is None or es.best.f < es._cma_best :
        es._cma_best = es.best.f
        es._cma_best_iter = es.countiter
    if es.countiter - es._cma_best_iter > es.cma_max_nochange_iterations and es.countiter > _MIN_ITERATIONS :
        return True
    return False

def report_restart_ready(url) :
    success = False
    result = None
    while not success :
        try :
            server = xmlrpclib.ServerProxy(url)
            result = server.report_restart()
            break
        except :
            success = False
            time.sleep(_RETRY_SLEEP)
    
    if result == 'WAIT' :
        print '[i] Waiting for controller restart (%s)...' % datetime.datetime.now().strftime("%B %d %Y %I:%M%p"),
        sys.stdout.flush()
        while result == 'WAIT' :
            sys.stdout.write('.')
            sys.stdout.flush()
            time.sleep(_RETRY_SLEEP)
            try :
                result = server.report_restart()
            except :
                result = 'WAIT'
        sys.stdout.write('\n')
        sys.stdout.flush()
        return True
    
    return False

def parallel_simulate_gene(params) :
    '''
    An IPython-friendly version of the persistent gene simulator.
    '''
    import persistent
    import approximation
    import time
    start_time = time.time()
    name, vector, scale, cache_approximation, srand, timeout = params
    approximation._cache_approximation = cache_approximation
    initiation = approximation.approximate_initiation(name, vector, scale, srand, timeout = timeout)
    # Check timeout
    if time.time() - start_time > timeout :
        raise ParallelTimeout('Gene: %s, timeout %.5f seconds' % (name, timeout))
    result = persistent.simulate_gene(name, initiation, vector, srand) + (initiation, )
    duration = time.time() - start_time
    return (duration, ) + result

def parallel_evaluate_fit(params) :
    '''
    An IPython-friendly version of the persistent fit evaluator.
    '''
    import persistent
    import approximation
    import time
    start_time = time.time()
    name, vector, scale, cache_approximation, srand, timeout = params
    approximation._cache_approximation = cache_approximation
    initiation = approximation.approximate_initiation(name, vector, scale, srand, timeout = timeout)
    #print '%s, init %.5f' % (name, initiation)
    # Check timeout
    if time.time() - start_time > timeout :
        raise ParallelTimeout('Gene: %s, timeout %.5f seconds' % (name, timeout))
    result = persistent.evaluate_fit(name, initiation, vector, srand)
    duration = time.time() - start_time
    return (duration, result)

class SingleGeneFitter(object) :
    '''
    A class that facilitates fitting a single gene model into the segment tree data.
    Uses the persistent module (i.e. does not store or manage any information within itself).
    '''

    def __init__(self, name, ndims = 64) :
        '''
        Class constructor.
        :param name: systematic name of the gene to fit.
        :param ndims: dimensionality of the rate function search space.
        '''
        self.name = name
        self.ndims = ndims
    
    def _construct_solution(self, fit, vector, srand) :
        '''
        Construct the simulated solution (i.e. obtain the density profile and compute the scale).
        :param fit: a SimpleNamespace in which the results should be saved.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        import persistent
        initiation = simulator.RateFunction.sigmoid(vector[0])
        coef, density, J = persistent.simulate_gene(self.name, initiation, vector[1:], srand)
        scale = -coef[1] / (2 * coef[0])
        fit.scale = math.exp(scale)
        fit.density = density
        fit.J = J

    def get_objective(self, vector, srand, verbose = 0) :
        '''
        Compute objective function value.
        :param vector: the alpha vector (initiation rate + rate function beta) for which the objective function should be evaluated.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        import persistent
        initiation = simulator.RateFunction.sigmoid(vector[0])
        coef = persistent.evaluate_fit(self.name, initiation, vector[1:], srand)
        scale = -coef[1] / (2 * coef[0])
        likelihood = -(coef[0] * scale ** 2 + coef[1] * scale + coef[2])
        if verbose > 1:
            print '   [i] Current fit: %.5f' % (-likelihood)
        return likelihood
   
    def optimize_cma(self, sigma = 2.0, tolerance = 0.001, srand = 101317, cma_srand = 713101, budget = float('Inf'), save_state = None, verbose = 0) :
        '''
        Fit the model into data using the CMA evolutionary strategy.
        :param sigma: sigma parameter of the CMA-ES algorithm.
        :param tolerance : the convergence tolerance.
        :param srand: seed of the random number generator to be used in simulation.
        :param cma_srand: seed of the random number generator to be used in fitting.
        :param budget: the maximum allowed number of function evaluations.
        :param save_state: if not None, CMA optimization object is saved into file every 100 iterations.
        :param verbose : output verbosity level (Default: 0).
        '''
        start_time = time.time()
        start = np.zeros((self.ndims + 1, ))
        options = {'tolfun' : tolerance, 'tolfunhist' : tolerance, 'seed' : cma_srand, 'maxfevals' : budget, 'verb_disp' : 100000, 'verb_log' : 100000, 'bounds' : [-12, 12]}
        es = cma.CMAEvolutionStrategy(start, sigma, options)
        # Evaluate initial solution, mimicking fmin behaviour
        x = es.gp.pheno(es.mean, bounds = es.gp.bounds)
        es.best.update([x], None, [self.get_objective(x, srand, verbose)], 1)
        es.countevals += 1
        it = 0
        next_save = _WRITE_FREQUENCY
        while not es.stop() :
            print '[i] Best: %.5f at %d (%.2f iterations ago)' % (es.best.f, es.countiter, (es.best.evalsall - es.best.evals) / float(es.popsize))
            X = es.ask()
            fit = [self.get_objective(x, srand, verbose) for x in X]
            es.tell(X, fit)
            it += 1
            if save_state is not None and it >= next_save :
                writer.write_pickle(es, save_state)
                next_save += _WRITE_FREQUENCY
        res = es.result()
        vector, value = res[0], res[1]
        time_elapsed = time.time() - start_time
        if verbose  > 0:
            print 'Time elapsed: %.4f sec' % time_elapsed
        if save_state :
            writer.write_pickle(es, save_state)

        data = SimpleNamespace()
        data.function_calls_opt, data.iterations, data.function_calls, data.xmean, data.std, data.stop = res[2], res[4], res[3], res[5], res[6], es.stop().items()
        fit = SimpleNamespace()
        fit.names = self.name
        fit.total_fitness = -value
        fit.fitness = -value
        fit.initiation = vector[0]
        fit.vector = vector[1:]
        fit.srand = srand
        fit.srand_cma = cma_srand
        fit.data = data
        fit.time_elapsed = time_elapsed
        self._construct_solution(fit, vector, srand)
        return fit

class SingleScaleGeneGroupFitter(object) :
    '''
    A gene group fitter that fits genes on a single global scale.
    '''

    def __init__(self, names, ndims = 64, scale_guess = 4.0, scale_guess_sigma = 0.1, cache_approximation = False, client_url = None) :
        '''
        Class constructor.
        :param names: systematic names of the genes to fit.
        :param ndims: dimensionality of the rate function search space.
        :param scale_guess: guess of the scale for initiation rate fitting.
        :param scale_guess_sigma: standard deviation of the guess.
        :param cache_approximation: if True, segment density approximation are NOT recomputed. Should only be used then elongation rates are not optimized.
        :param client_url: URL of the IPython parallel client. Used for parallel execution.
        '''
        self.names = names
        self.ndims = ndims
        self.scale_guess = scale_guess
        self.scale_guess_sigma = scale_guess_sigma
        self.cache_approximation = cache_approximation
        self.client_url = client_url
        self.client = Client(client_url) if client_url is not None else None
        self.view = self.client.load_balanced_view() if self.client is not None else None
        self._n_genes = len(names)
        self.scheduler = scheduler.GeneScheduler(names)

        if self.view is not None and self.view.retries == 0 :
            self.view.retries = 100
        #if self.view is not None :
        #    self.view.timeout = 720
        
    def _construct_solution(self, fit, vector, srand) :
        '''
        Construct the simulated solution (i.e. obtain the density profile and compute the scale).
        :param fit: a SimpleNamespace in which the results should be saved.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        proposed_scale = vector[0]
        coef = np.zeros((3,))
        coefs = []
        density = {}
        initiations = {}
        J = {}
        Q = {}
        if self.view is None :
            import persistent
            import approximation
            for i in xrange(self._n_genes) :
                name = self.names[i]
                current_init = approximation.approximate_initiation(name, vector[1:], proposed_scale, srand)
                current_coef, current_density, current_J = persistent.simulate_gene(name, current_init, vector[1:], srand)
                density[name] = current_density
                J[name] = current_J
                Q[name] = current_Q
                initiations[name] = simulator.RateFunction.logit(current_init)
                coef += current_coef
                coefs.append(current_coef)
        else :
            # Prepare for parallel execution and fire
            params = []
            for i in xrange(self._n_genes) :
                name = self.names[i]
                timeout = self.scheduler.get_timeout(name)
                params.append((name, vector[1:], proposed_scale, self.cache_approximation, srand, timeout))
            # Added reordering for optimal execution
            indices, params = self.scheduler.reorder_genes(params)
            results = self.view.map(parallel_simulate_gene, params, block = True)
            results = self.scheduler.restore_order(indices, results)
            for i in xrange(self._n_genes) :
                name = self.names[i]
                current_duration, current_coef, current_density, current_J, current_Q, current_init = results[i]
                self.scheduler.update_gene(name, current_duration)
                density[name] = current_density
                J[name] = current_J
                Q[name] = current_Q
                initiations[name] = simulator.RateFunction.logit(current_init)
                coef += current_coef
                coefs.append(current_coef)

        scale = -coef[1] / (2 * coef[0])
        fitness = {}
        for i in xrange(self._n_genes) :
            name = self.names[i]
            likelihood = - (coefs[i][0] * scale ** 2 + coefs[i][1] * scale + coefs[i][2])
            fitness[name] = -likelihood
        fit.scale = math.exp(scale)
        fit.proposed_scale = math.exp(proposed_scale)
        fit.density = density
        fit.J = J
        fit.Q = Q
        fit.fitness = fitness
        fit.initiation = initiations

    def get_objective(self, vector, srand, verbose = 0) :
        '''
        Compute objective function value.
        :param vector: the alpha vector (initiation rate + rate function beta) for which the objective function should be evaluated.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        proposed_scale = vector[0]
        #print 'Proposed scale: %g' % proposed_scale
        coef = np.zeros((3,))
        if self.view is None :
            import persistent
            import approximation
            for i in xrange(self._n_genes) :
                name = self.names[i]
                initiation = approximation.approximate_initiation(name, vector[1:], proposed_scale, srand)
                #print "%s, init %.5f" % (name, initiation)
                current_coef = persistent.evaluate_fit(name, initiation, vector[1:], srand)
                coef += current_coef
        else :
            # Prepare for parallel execution and fire
            params = []
            for i in xrange(self._n_genes) :
                name = self.names[i]
                timeout = self.scheduler.get_timeout(name)
                params.append((name, vector[1:], proposed_scale, self.cache_approximation, srand, timeout))
            indices, params = self.scheduler.reorder_genes(params)
            results = self.view.map(parallel_evaluate_fit, params, block = True)
            results = self.scheduler.restore_order(indices, results)
            for i in xrange(self._n_genes) :
                name = self.names[i]
                current_duration, current_coef = results[i]
                self.scheduler.update_gene(name, current_duration)
                coef += current_coef
        scale = -coef[1] / (2 * coef[0])
        likelihood = -(coef[0] * scale ** 2 + coef[1] * scale + coef[2])
        if verbose > 1 :
            print '   [i] Current fit: %.5f' % (-likelihood)
        return likelihood

    def _async_get_objective(self, X, srand) :
        '''
        Compute objective function value. Non-blocking version. Return AsyncResult object.
        :param X: list of vectors - scale + rate function beta for which the objective function should be evaluated.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        n_solutions = len(X)
        params = []
        for j in xrange(n_solutions) :
            vector = X[j]
            proposed_scale = vector[0]
            for i in xrange(self._n_genes) :
                name = self.names[i]
                timeout = self.scheduler.get_timeout(name)
                params.append((name, vector[1:], proposed_scale, self.cache_approximation, srand, timeout))
        indices, params = self.scheduler.reorder_genes(params)
        ar = self.view.map(parallel_evaluate_fit, params, block = False)
        return (ar, indices)

    def _async_process_get_objective(self, ar, indices, verbose = 0) :
        '''
        Compute objective function value. Non-blocking version. Return AsyncResult object.
        :param ar: AsyncResult object
        :param indices: indices for result reconstruction (after scheduler).
        :param verbose: output verbosity level (Default: 0).
        '''
        results = ar.result
        results = self.scheduler.restore_order(indices, results)
        n_solutions = len(indices) / self._n_genes
        likelihoods = []
        k = 0
        for j in xrange(n_solutions) :
            coef = np.zeros((3,))
            for i in xrange(self._n_genes) :
                name = self.names[i]
                current_duration, current_coef = results[k]
                self.scheduler.update_gene(name, current_duration)
                coef += current_coef
                k += 1
            scale = -coef[1] / (2 * coef[0])
            likelihood = -(coef[0] * scale ** 2 + coef[1] * scale + coef[2])
            if verbose > 1:
                print '   [i] Current fit: %.5f' % (-likelihood)
            likelihoods.append(likelihood)
        return likelihoods
    
    def optimize_cma(self, sigma = 2.0, initial_solution = None, tolerance = 0.001, srand = 101317, cma_srand = 713101, budget = float('Inf'), nochange_iterations = 50, fixed_rates = None, save_state = None, restart_url = None, verbose = 0, es = None) :
        '''
        Fit the model into data using the CMA evolutionary strategy.
        :param sigma: sigma parameter of the CMA-ES algorithm.
        :param initial_solution : codon elongation rates for the initial solution.
        :param tolerance : the convergence tolerance.
        :param srand: seed of the random number generator to be used in simulation.
        :param cma_srand: seed of the random number generator to be used in fitting.
        :param budget: the maximum allowed number of function evaluations.
        :param nochange_iterations: CMA will be stopped if no new best solution was found within so nochange_iterations iterations.
        :param fixed_rates : is not None, rates of certain codons are fixed.
        :param save_state: if not None, CMA optimization object is saved into file every 100 iterations.
        :param restart_url: address of the XML-RPC server where the restart readiness should be reported.
        :param verbose : output verbosity level (Default: 0).
        :param es : CMAEvolutionaryStrategy object to be used for continuing the optimization.
        '''
        global _semaphore
        if self.view is None :
            import approximation
            approximation._cache_approximation = self.cache_approximation
        start_time = time.time()
        start = np.zeros((self.ndims + 1, ))
        if initial_solution is not None :
            for codon in initial_solution.keys() :
                start[tools.codon2int[codon] + 1] = simulator.RateFunction.logit(initial_solution[codon])
        start[0] = self.scale_guess
        scaling = np.ones((self.ndims + 1, ))
        scaling[0] = self.scale_guess_sigma / float(sigma)
        options = {'tolfun' : tolerance, 'tolfunhist' : tolerance, 'seed' : cma_srand, 'maxfevals' : budget, 'verb_disp' : 100000, 'verb_log' : 100000, 'bounds' : [-12, 12], 'scaling_of_variables' : scaling, 'termination_callback' : _cma_termination_callback}
        if fixed_rates is not None :
            fixed_variables = {}
            for codon in fixed_rates.keys() :
                fixed_variables[tools.codon2int[codon] + 1] = simulator.RateFunction.logit(fixed_rates[codon])
            options['fixed_variables'] = fixed_variables
        if es is None :
            es = cma.CMAEvolutionStrategy(start, sigma, options)
            # Initialize "no change" stopping criterion variables
            es.cma_max_nochange_iterations = nochange_iterations
            es._cma_best = None
            # Evaluate initial solution, mimicking fmin behaviour
            x = es.gp.pheno(es.mean, bounds = es.gp.bounds)
	    _semaphore.acquire(delta = 1)
	    try :
                es.best.update([x], None, [self.get_objective(x, srand, verbose)], 1)
	    finally :
	        _semaphore.release(delta = 1)
            es.countevals += 1
        it = 0
        next_save = _WRITE_FREQUENCY
        while not es.stop() :
            print '[i] Best: %.5f at %d (%.2f iterations ago)' % (es.best.f, es.countiter, (es.best.evalsall - es.best.evals) / float(es.popsize))
            X = es.ask()
            if self.view is None :
                fit = [self.get_objective(x, srand, verbose) for x in X]
            else :
                _semaphore.acquire(delta = 1)
                try :
                    ar, indices = self._async_get_objective(X, srand)
                    fit = self._async_process_get_objective(ar, indices, verbose)
                    self.client.purge_local_results('all')
                finally :
                    _semaphore.release(delta = 1)
            es.tell(X, fit)
            it += 1
            if save_state is not None and it >= next_save :
                writer.write_pickle(es, save_state)
                next_save += _WRITE_FREQUENCY
            if restart_url :
                if report_restart_ready(restart_url) :
                    if verbose > 0 :
                        print '[i] Controller restarted (%s).' % datetime.datetime.now().strftime("%B %d %Y %I:%M%p")
                    time.sleep(_RESTART_SLEEP)
        res = es.result()
        vector, value = res[0], res[1]
        time_elapsed = time.time() - start_time
        if verbose  > 0:
            print 'Time elapsed: %.4f sec' % time_elapsed
        if save_state :
            writer.write_pickle(es, save_state)
        
        data = SimpleNamespace()
        data.function_calls_opt, data.iterations, data.function_calls, data.xmean, data.std, data.stop = res[2], res[4], res[3], res[5], res[6], es.stop().items()
        fit = SimpleNamespace()
        fit.names = self.names
        fit.total_fitness = -value
        fit.vector = vector[1:]
        fit.srand = srand
        fit.srand_cma = cma_srand
        fit.data = data
        fit.time_elapsed = time_elapsed
        self._construct_solution(fit, vector, srand)
        return fit

class IndividualScaleGeneGroupFitter(object) :
    '''
    A gene group fitter that fits genes on individual (gene-specific) scale.
    '''

    def __init__(self, names, ndims = 64, cache_approximation = False, client_url = None) :
        '''
        Class constructor.
        :param names: systematic names of the genes to fit.
        :param ndims: dimensionality of the rate function search space.
        :param cache_approximation: if True, segment density approximation are NOT recomputed. Should only be used then elongation rates are not optimized.
        :param view: IPython parallel client view. Used for parallel execution.
        '''
        self.names = names
        self.ndims = ndims
        self.cache_approximation = cache_approximation
        self.client_url = client_url
        self.client = Client(client_url) if client_url is not None else None
        self.view = self.client.load_balanced_view() if self.client is not None else None
        self._n_genes = len(names)
        self.scheduler = scheduler.GeneScheduler(names)
        
        if self.view is not None and self.view.retries == 0 :
            self.view.retries = 100
        #if self.view is not None :
        #    self.view.timeout = 720

    def _construct_solution(self, fit, vector, srand) :
        '''
        Construct the simulated solution (i.e. obtain the density profile and compute the scale).
        :param fit: a SimpleNamespace in which the results should be saved.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        density = {}
        initiations = {}
        scales = {}
        fitness = {}
        J = {}
        Q = {}
        if self.view is None :
            import persistent
            import approximation
            for i in xrange(self._n_genes) :
                name = self.names[i]
                init = approximation.approximate_initiation(name, vector, None, srand)
                coef, density_i, J_i, Q_i = persistent.simulate_gene(name, init, vector, srand)
                scale = -coef[1] / (2 * coef[0])
                likelihood = - (coef[0] * scale ** 2 + coef[1] * scale + coef[2])
                density[name] = density_i
                J[name] = J_i
                Q[name] = Q_i
                initiations[name] = simulator.RateFunction.logit(init)
                scales[name] = math.exp(scale)
                fitness[name] = -likelihood
        else :
            # Prepare for parallel execution and fire
            params = []
            for i in xrange(self._n_genes) :
                name = self.names[i]
                timeout = self.scheduler.get_timeout(name)
                params.append((name, vector, None, self.cache_approximation, srand, timeout))
            indices, params = self.scheduler.reorder_genes(params)
            results = self.view.map(parallel_simulate_gene, params, block = True)
            results = self.scheduler.restore_order(indices, results)
            for i in xrange(self._n_genes) :
                name = self.names[i]
                duration, coef, density_i, J_i, Q_i, init = results[i]
                self.scheduler.update_gene(name, duration)
                scale = -coef[1] / (2 * coef[0])
                likelihood = - (coef[0] * scale ** 2 + coef[1] * scale + coef[2])
                density[name] = density_i
                J[name] = J_i
                Q[name] = Q_i
                initiations[name] = simulator.RateFunction.logit(init)
                scales[name] = math.exp(scale)
                fitness[name] = -likelihood
        fit.density = density
        fit.J = J
        fit.Q = Q
        fit.initiation = initiations
        fit.scale = scales
        fit.fitness = fitness

    def get_objective(self, vector, srand, verbose = 0) :
        '''
        Compute objective function value.
        :param vector: the alpha vector (initiation rate + rate function beta) for which the objective function should be evaluated.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        total_likelihood = 0.0
        if self.view is None :
            import persistent
            import approximation
            for i in xrange(self._n_genes) :
                name = self.names[i]
                init = approximation.approximate_initiation(name, vector, None, srand)
                coef = persistent.evaluate_fit(name, init, vector, srand)
                scale = -coef[1] / (2 * coef[0])
                likelihood = -(coef[0] * scale ** 2 + coef[1] * scale + coef[2])
                total_likelihood += likelihood
        else :
            # Prepare for parallel execution and fire
            params = []
            for i in xrange(self._n_genes) :
                name = self.names[i]
                timeout = self.scheduler.get_timeout(name)
                params.append((name, vector, None, self.cache_approximation, srand, timeout))
            indices, params = self.scheduler.reorder_genes(params)
            results = self.view.map(parallel_evaluate_fit, params, block = True)
            results = self.scheduler.restore_order(indices, results)
            for i in xrange(self._n_genes) :
                name = self.names[i]
                duration, coef = results[i]
                self.scheduler.update_gene(name, duration)
                scale = -coef[1] / (2 * coef[0])
                likelihood = -(coef[0] * scale ** 2 + coef[1] * scale + coef[2])
                total_likelihood += likelihood
        if verbose > 1 :
            print '   [i] Current fit: %.5f' % (-total_likelihood)
        return total_likelihood

    def _async_get_objective(self, X, srand) :
        '''
        Compute objective function value. Non-blocking version. Return AsyncResult object.
        :param X: list of vectors - scale + rate function beta for which the objective function should be evaluated.
        :param srand: seed of the random number generator to be used in simulation.
        :param verbose: output verbosity level (Default: 0).
        '''
        params = []
        for j in len(X) :
            vector = X[j]
            for i in xrange(self._n_genes) :
                name = self.names[i]
                timeout = self.scheduler.get_timeout(name)
                params.append((name, vector, None, self.cache_approximation, srand, timeout))
        indices, params = self.scheduler.reorder_genes(params)
        ar = self.view.map(parallel_evaluate_fit, params, block = False)
        return (ar, indices)

    def _async_process_get_objective(self, ar, indices, verbose = 0) :
        '''
        Compute objective function value. Non-blocking version. Return AsyncResult object.
        :param ar: AsyncResult object
        :param indices: indices for result reconstruction (after scheduler).
        :param verbose: output verbosity level (Default: 0).
        '''
        results = ar.result
        results = self.scheduler.restore_order(indices, results)
        n_solutions = len(indices) / self._n_genes
        likelihoods = []
        k = 0
        for j in xrange(n_solutions) :
            total_likelihood = 0.0
            for i in xrange(self._n_genes) :
                name = self.names[i]
                duration, coef = results[k]
                self.scheduler.update_gene(name, duration)
                scale = -coef[1] / (2 * coef[0])
                likelihood = -(coef[0] * scale ** 2 + coef[1] * scale + coef[2])
                total_likelihood += likelihood
                k += 1
            if verbose > 1:
                print '   [i] Current fit: %.5f' % (-total_likelihood)
            likelihoods.append(total_likelihood)
        return likelihoods
    
    def optimize_cma(self, sigma = 2.0, initial_solution = None, tolerance = 0.001, srand = 101317, cma_srand = 713101, budget = float('Inf'), nochange_iterations = 50, fixed_rates = None, save_state = None, restart_url = None, verbose = 0) :
        '''
        Fit the model into data using the CMA evolutionary strategy.
        :param sigma: sigma parameter of the CMA-ES algorithm.
        :param initial_solution : codon elongation rates for the initial solution.
        :param tolerance : the convergence tolerance.
        :param srand: seed of the random number generator to be used in simulation.
        :param cma_srand: seed of the random number generator to be used in fitting.
        :param budget: the maximum allowed number of function evaluations.
        :param nochange_iterations: CMA will be stopped if no new best solution was found within so nochange_iterations iterations.
        :param fixed_rates : is not None, rates of certain codons are fixed.
        :param save_state: if not None, CMA optimization object is saved into file every 100 iterations.
        :param restart_url: address of the XML-RPC server where the restart readiness should be reported.
        :param verbose : output verbosity level (Default: 0).
        '''
        global _semaphore
        if self.view is None :
            import approximation
            approximation._cache_approximation = self.cache_approximation
        start_time = time.time()
        start = np.zeros((self.ndims, ))
        if initial_solution is not None :
            for codon in initial_solution.keys() :
                start[tools.codon2int[codon]] = simulator.RateFunction.logit(initial_solution[codon])
        options = {'tolfun' : tolerance, 'seed' : cma_srand, 'maxfevals' : budget, 'verb_disp' : 100000, 'verb_log' : 100000, 'bounds' : [-12, 12], 'termination_callback' : _cma_termination_callback}
        if fixed_rates is not None :
            fixed_variables = {}
            for codon in fixed_rates.keys() :
                fixed_variables[tools.codon2int[codon]] = simulator.RateFunction.logit(fixed_rates[codon])
            options['fixed_variables'] = fixed_variables
        es = cma.CMAEvolutionStrategy(start, sigma, options)
        # Initialize "no change" stopping criterion variables
        es.cma_max_nochange_iterations = nochange_iterations
        es._cma_best = None
        # Evaluate initial solution, mimicking fmin behaviour
        x = es.gp.pheno(es.mean, bounds = es.gp.bounds)
	_semaphore.acquire(delta = 1)
	try :
            es.best.update([x], None, [self.get_objective(x, srand, verbose)], 1)
	finally :
	    _semaphore.release(delta = 1)
        es.countevals += 1
        it = 0
        next_save = _WRITE_FREQUENCY
        while not es.stop() :
            print '[i] Best: %.5f at %d (%.2f iterations ago)' % (es.best.f, es.countiter, (es.best.evalsall - es.best.evals) / float(es.popsize))
            X = es.ask()
            if self.view is None :
                fit = [self.get_objective(x, srand, verbose) for x in X]
            else :
                _semaphore.acquire(delta = 1)
                try :
                    ar, indices = self._async_get_objective(X, srand)
                    fit = self._async_process_get_objective(ar, indices, verbose)
                    self.client.purge_local_results('all')
                finally :
                    _semaphore.release(delta = 1)
            es.tell(X, fit)
            it += 1
            if save_state is not None and it >= next_save :
                writer.write_pickle(es, save_state)
                next_save += _WRITE_FREQUENCY
            if restart_url :
                if report_restart_ready(restart_url) :
                    if verbose > 0 :
                        print '[i] Controller restarted (%s).' % datetime.datetime.now().strftime("%B %d %Y %I:%M%p")
                    time.sleep(_RESTART_SLEEP)
        res = es.result()
        vector, value = res[0], res[1]
        time_elapsed = time.time() - start_time
        if verbose  > 0:
            print 'Time elapsed: %.4f sec' % time_elapsed
        if save_state :
            writer.write_pickle(es, save_state)
        
        data = SimpleNamespace()
        data.function_calls_opt, data.iterations, data.function_calls, data.xmean, data.std, data.stop = res[2], res[4], res[3], res[5], res[6], es.stop().items()
        fit = SimpleNamespace()
        fit.names = self.names
        fit.total_fitness = -value
        fit.vector = vector
        fit.srand = srand
        fit.srand_cma = cma_srand
        fit.data = data
        fit.time_elapsed = time_elapsed
        self._construct_solution(fit, vector, srand)
        return fit

'''
Execute module initialization
'''
_initialize_semaphore()
