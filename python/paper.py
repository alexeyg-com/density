from lib.SimpleNamespace import SimpleNamespace
import lib.persistent
import lib.zhang
import lib.simulator
import lib.tree

import numpy as np
import pylab
import matplotlib.pyplot as plot
from matplotlib.lines import Line2D
import scipy.stats
import math
import statsmodels.api as sm
import matplotlib.mlab as mlab
import timeit
import json

import lib.approximation
import lib.evaluator
import lib.david

def evaluate_cross_validation(forest, folds, colors = None) :
    codons = lib.tools.codon2int.keys()
    n_folds = len(folds)
    fold_counts = []
    for i in range(n_folds) :
        codon_array = np.zeros((64,), dtype = np.float)
        fold_counts.append(codon_array)
    total_counts = np.zeros((64,), dtype = np.float)
    for i in range(n_folds) :
        for gene in folds[i] :
            seq = lib.tree.get_codon_sequence(forest[gene])
            for codon in codons :
                cmp = seq == codon
                tree = forest[gene]
                for node in tree.tree :
                    left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                    if np.any(cmp[left:right + 1]) :
                        fold_counts[i][lib.tools.codon2int[codon]] += 1
                        total_counts[lib.tools.codon2int[codon]] += 1
    f, ax = plot.subplots(1, 1)
    width = 0.45
    if colors is None :
        colors = ['b', 'r', 'y', 'g', 'c', 'm']
    ind = np.array(range(64)) * 1.7 * width + width
    offset = np.zeros((64,), dtype = np.float)
    bars = []
    for i in range(n_folds) :
        bar = ax.bar(ind, fold_counts[i] / total_counts, width, bottom = offset / total_counts, color = colors[i], alpha = 0.45)
        offset += fold_counts[i]
        bars.append(bar)
    ax.set_xlim((0, np.max(ind) + 2 * width))
    ax.set_xticks(ind)
    codons_permute = [None] * 64
    for codon in codons :
        codons_permute[lib.tools.codon2int[codon]] = codon
    ax.set_xticklabels(codons_permute, rotation = 90)
    ax.set_ylabel('Fraction of segments', fontsize = 16)
    ax.set_xlabel('Codons', fontsize = 16)
    f.set_size_inches(19, 4.5, forward = True)
    f.tight_layout()
    plot.show()

def check_enrichment(names, values, mi, ma, cutoff = 0.05, is_initiation = False) :
    names = np.array(names)
    if is_initiation :
        values2 = []
        for gene in names :
            values2.append(values[gene])
        values = np.array(values2)
        for i in range(values.size) :
            values[i] = lib.simulator.RateFunction.sigmoid(values[i])
    else :
        values = np.copy(np.array(values))
    selected = np.logical_and(values >= mi, values < ma)
    print '[i] Genes selected: %d' % np.sum(selected)
    enrichment = lib.david.enrich(names[selected], names)
    for en in enrichment :
        if en.afdr < cutoff :
            print '%s : %.5f (%d genes)' % (en.term_name, en.afdr, en.list_hits)

def plot_limitation(relative_J, n_bins = 50, cutoff = 0.0, enrichment_text = None) :
    f, ax = plot.subplots(1, 1)
    relative_J = np.array(relative_J)
    mi, ma = np.min(relative_J), np.max(relative_J)
    len_pre, len_post = -mi, ma
    n_pre = int(round(n_bins * len_pre / (len_pre + len_post)))
    n_post = int(round(n_bins * len_post / (len_pre + len_post)))
    bins_pre = np.linspace(mi, 0.0, n_pre, endpoint = False)
    bins_post = np.linspace(0.0, ma, n_post)
    bins = np.concatenate((bins_pre, bins_post))
    n, bins, patches = ax.hist(relative_J, bins, normed = True)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax.set_xlabel('Relative change in $J$', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    plot.setp(ax.get_yticklabels(), visible = False)
    for thisbin, thispatch in zip(bins, patches) :
        if thisbin < cutoff :
            thispatch.set_facecolor('r')
    xmax = ax.get_xlim()
    ax.set_xlim(xmax[0], 0.22)
    if enrichment_text is not None :
        xmax = ax.get_xlim()
        ax.set_xlim(-0.045, xmax[1])
        ymax = ax.get_ylim()
        pos_y = ymax[1] - 0.4
        ann = ax.annotate(enrichment_text,
                  xy = (-0.011, 1.7), xycoords = 'data',
                  xytext = (-0.040, pos_y), textcoords = 'data',
                  size = 12, va = "top", ha = "left",
                  bbox = dict(boxstyle = "square", fc = "w", alpha = 0.45),
                  arrowprops = dict(arrowstyle = "-|>",
                                  connectionstyle = "arc3,rad=0.0",
                                  fc = "w", lw = 1.5), 
                  )

    f.tight_layout()
    plot.show()
    pass

def plot_JQ(result) :
    genes = result.names
    Js, Qs, inits = [], [], []
    for gene in genes :
        Js.append(result.J[gene])
        Qs.append(result.Q[gene])
        inits.append(lib.simulator.RateFunction.sigmoid(result.initiation[gene]))
    f, ax = plot.subplots(1, 1)
    ax.scatter(np.log2(inits), np.log2(Qs))
    ax.set_xlabel('Initiation rate, $\\log_2$', fontsize = 16)
    ax.set_ylabel('Q, $\\log_2$', fontsize = 16)
    f.tight_layout()
    plot.show()

def determine_limitation(result, factor = 1.1, srand = None) :
    genes = result.names
    rates = result.vector
    if srand is None :
        srand = result.srand
    percent_Js, percent_Qs = [], []
    for gene in genes :
        initiation_before = lib.simulator.RateFunction.sigmoid(result.initiation[gene])
        initiation_after = initiation_before * 1.1
        J_before = result.J[gene]
        Q_before = result.Q[gene]
        coef_after, density_after, J_after, Q_after = lib.persistent.simulate_gene(gene, initiation_after, rates, srand)
        percent_J = (J_after - J_before) / J_before
        percent_Q = (Q_after - Q_before) / Q_before
        print '%10s : J_rel = %.3f, Q_rel = %.3f' % (gene, percent_J, percent_Q)
        percent_Js.append(percent_J)
        percent_Qs.append(percent_Q)

    return (np.array(genes[:]), np.array(percent_Js), np.array(percent_Qs))

def check_densities(results, forest) :
    vector = np.copy(results.vector)
    genes = results.names
    srand = results.srand

    for gene in genes :
        print '%s' % gene
        print '---------------'
        init = lib.simulator.RateFunction.sigmoid(results.initiation[gene])
        coef, density, J, Q = lib.persistent.simulate_gene(gene, init, vector, srand)
        for node in forest[gene].tree :
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            before, after = np.mean(results.density[gene][left:right + 1]), np.mean(density[left:right + 1])
            ratio = abs(before - after) / after
            print '%.8f -> %.8f (%.8f)' % (before, after, ratio)

def plot_shah_convergence(abundance_filename, filename_prefix, time_range, folds, forest, means = None, stds = None) :
    if means is None or stds is None :
        means, stds = [], []
        for time in time_range :
            shah = lib.evaluator.read_shah_output(abundance_filename, filename_prefix + str(time), time)
            mean, std = lib.evaluator.evaluate_profiles_cv(shah, folds, forest)
            means.append(mean)
            stds.append(std)
    f, ax = plot.subplots(1, 1)
    time_range, means, stds = np.array(time_range, dtype = np.float), np.array(means), np.array(stds)
    ax.plot(time_range, means, 'r-', linewidth = 1.5)
    ax.fill_between(time_range, means + stds, means - stds, facecolor = 'blue', alpha = 0.45)
    ax.set_ylabel('Objective, $\\psi$', fontsize = 16)
    ax.set_xlabel('Simulation time, sec', fontsize = 16)
    ax.set_xlim(np.min(time_range), np.max(time_range))
    f.tight_layout()
    plot.show()
    return (means, stds)

def attach_production_rates(results) :
    genes = results.names
    results.J = {}
    results.Q = {}
    srand = results.srand
    rates = np.copy(results.vector)
    for gene in genes :
        initiation = lib.simulator.RateFunction.sigmoid(results.initiation[gene])
        coef, density, J, Q = lib.persistent.simulate_gene(gene, initiation, rates, srand)
        results.J[gene] = J
        results.Q[gene] = Q
        print '%10s : J = %.5f, Q = %.5f' % (gene, J, Q)

def plot_initiation_rates(results, n_bins = 50) :
    genes = results.names
    
    initiations = []
    for gene in genes :
        init = lib.simulator.RateFunction.sigmoid(results.initiation[gene])
        initiations.append(init)

    initiations = np.array(initiations)
    initiations = np.log2(initiations)
    f, ax = plot.subplots(1, 1)
    n, bins, patches = ax.hist(initiations, n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax.set_xlabel('Initiation rate, $\\log_2$', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    pylab.show()

def offer_cmp(a, b) :
    if a[1] + a[2] < b[1] + b[2] :
        return 1
    else :
        return -1

def offer_genes(results_a, results_b, results_shah, results_zhang) :
    found = []
    for gene in results_a.names :
        if gene in results_b.names and gene in results_shah.density and gene in results_zhang.density :
            a, b, shah, zhang = results_a.fitness[gene], results_b.fitness[gene], results_shah.fitness[gene], results_zhang.fitness[gene]
            if b > a and a > shah and shah > zhang :
                entry = (gene, b, a, shah, zhang)
                found.append(entry)
    print 'Found: %d entries' % len(found)
    found = list(sorted(found, cmp = offer_cmp))
    return found

def plot_coverage_poster(gene, cov_a = None, cov_b = None, y_max = 150) :
    f, ax = plot.subplots(1, 1, sharey = True)
    if cov_a is not None :
        cov_a = cov_a[gene]
        n_codons = len(cov_a)
    if cov_b is not None :
        cov_b = cov_b[gene]
        n_codons = len(cov_b)
    edges = np.array(range(1, n_codons + 2))
    centers = (edges[1:] + edges[:-1]) / 2.0
    if cov_a is not None and cov_b is not None :
        bad_ind = np.logical_or(np.logical_or(np.isinf(cov_a), np.isinf(cov_b)), np.logical_or(np.isnan(cov_a), np.isnan(cov_b)))
    elif cov_a is not None :
        bad_ind = np.logical_or(np.isinf(cov_a), np.isnan(cov_a))
    elif cov_b is not None :
        bad_ind = np.logical_or(np.isinf(cov_b), np.isnan(cov_b))
    if cov_a is not None :
        cov_a[bad_ind] = 0
    if cov_b is not None :
        cov_b[bad_ind] = 0
    
    if cov_a is not None :
        ax.hist(centers, edges, weights = cov_a, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 1')
    if cov_b is not None :
        ax.hist(centers, edges, weights = cov_b, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 2')
    
    ax.set_xlim(1, n_codons + 1)
    ax.set_ylim(0, y_max)
    ax.set_xlabel('Position', fontsize = 16)
    ax.set_ylabel('Ribosome coverage', fontsize = 16)
    if cov_a is not None and cov_b is not None :
        ax.legend(loc = 'best', ncol = 3)
    
    f.tight_layout()
    plot.show()

def plot_coverage(gene, cov_a, cov_b, results_a = None, results_b = None, results_shah = None, results_zhang = None, forest = None, L = 10, offset = -5, xmax = None, ymax = None, log_scale = False, frac = 0.04, xshift = 40) :
    f, ax = plot.subplots(1, 1, sharey = True)
    cov_a, cov_b = cov_a[gene], cov_b[gene]
    n_codons = len(cov_a)
    edges = np.array(range(1, n_codons + 2))
    centers = (edges[1:] + edges[:-1]) / 2.0
    
    bad_ind = np.logical_or(np.logical_or(np.isinf(cov_a), np.isinf(cov_b)), np.logical_or(np.isnan(cov_a), np.isnan(cov_b)))
    cov_a[bad_ind] = 0
    cov_b[bad_ind] = 0
    if not log_scale :
        ax.hist(centers, edges, weights = cov_a, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 1')
        ax.hist(centers, edges, weights = cov_b, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 2')
    else :
        ax.hist(centers, edges, weights = np.log2(cov_a), histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 1')
        ax.hist(centers, edges, weights = np.log2(cov_b), histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 2')
    
    #cov = np.sqrt(cov_a * cov_b)
    #ax.hist(centers, edges, weights = cov, histtype = 'stepfilled', alpha = 0.45)
    ax.set_xlim(1, n_codons + 1)

    if results_zhang is not None :
        density = np.copy(results_zhang.density[gene])
        scale = results_zhang.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), linewidth = 1.5, color = 'g', label = 'Zhang model')
        else :
            ax.plot(centers, scale * coverage, linewidth = 1.5, color = 'g', label = 'Zhang model')
    
    if results_shah is not None :
        density = np.copy(results_shah.density[gene])
        scale = results_shah.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), linewidth = 1.5, color = 'k', label = 'SMoPT')
        else :
            ax.plot(centers, scale * coverage, linewidth = 1.5, color = 'k', label = 'SMoPT')
    
    if results_a is not None :
        density = np.copy(results_a.density[gene])
        scale = results_a.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), 'b', linewidth = 1.5, label = 'TASEP$^\\mathrm{init}$')
        else :
            ax.plot(centers, scale * coverage, 'b', linewidth = 1.5, label = 'TASEP$^\\mathrm{init}$')
    
    if results_b is not None :
        density = np.copy(results_b.density[gene])
        scale = results_b.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), 'r', linewidth = 1.5, label = 'TASEP$^\\mathrm{elong}$')
        else :
            ax.plot(centers, scale * coverage, 'r', linewidth = 1.5, label = 'TASEP$^\\mathrm{elong}$')
    
    if forest is not None :
        density, segments = lib.tools.fit_segment_density(forest[gene])
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(coverage), 'orange', linewidth = 1.5, label = 'MAP reconstruction')
        else :
            ax.plot(centers, coverage, 'orange', linewidth = 1.5, label = 'MAP reconstruction')
   
    params = { 'legend.fontsize' : 16, 'legend.linewidth' : 1.5, 'xtick.major.pad' : -10}
    plot.rcParams.update(params)
    ax.legend(loc = 'best', ncol = 3)
    if ymax is not None :
        ax.set_ylim(0, ymax)
    if xmax is not None :
        ax.set_xlim(0, xmax)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)
    #plot.setp(ax.get_yticklabels(), visible = False)
    if log_scale :
        ax.set_ylabel('Ribosome coverage, $\\log_2$', fontsize = 16)
    else :
        ax.set_ylabel('Ribosome coverage', fontsize = 16)
    ax.set_xlabel('Position', fontsize = 16)
    
    ymax = ax.set_ylim()
    ymax = ymax[1]
    ymax_original = ymax

    if results_zhang is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_zhang.fitness[gene], fontsize = 18, color = 'g')
    if results_shah is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_shah.fitness[gene], fontsize = 18, color = 'k')
    if results_a is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_a.fitness[gene], fontsize = 18, color = 'b')
    if results_b is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_b.fitness[gene], fontsize = 18, color = 'r')
    
    f.tight_layout()
   # f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    plot.show()

def plot_elongation_rates(results, additional = None, skip_stop = True, highlight = -1, color_codon = 'GAA', threshold = 0.05, threshold_std = 1.0) :
    rates = lib.tools.get_RFM_rates()
    codons = np.array(rates.keys())
    values = np.array([math.log(rates[codon], 2) for codon in codons])
    sortme = np.array([codons, values], dtype = np.dtype(object)).transpose()
    sortme = np.array(sorted(sortme, key = lambda x : -x[1])).transpose()
    codons = sortme[0, :]
    values = sortme[1, :]
    n_results = len(results)
    results_rates = []
    for i in range(n_results) :
        #cur_rates = np.copy(results[i].best.x[1:])
        cur_rates = np.zeros((64,))
        for j in range(len(codons)) :
            codon = codons[j]
            cur_rates[j] = math.log(lib.simulator.RateFunction.sigmoid(results[i].best.x[lib.tools.codon2int[codon] + 1]), 2)
        results_rates.append(cur_rates)

    if additional is not None :
        additional_rates = np.zeros((64,))
        for j in range(len(codons)) :
            codon = codons[j]
            additional_rates[j] = math.log(lib.simulator.RateFunction.sigmoid(additional.best.x[lib.tools.codon2int[codon] + 1]), 2)
        additional = additional_rates
    
    results_rates = np.array(results_rates)
    mean = np.mean(results_rates, axis = 0)
    std = np.std(results_rates, axis = 0)
    if skip_stop :
        good_ind = np.logical_not(np.logical_or(np.logical_or(codons == 'TAG', codons == 'TAA'), codons == 'TGA'))
        mean = mean[good_ind]
        std = std[good_ind]
        values = values[good_ind]
        codons = codons[good_ind]
        if additional is not None :
            additional = additional[good_ind]
        new_results_rates = []
        for i in range(n_results) :
            #results_rates[i] = results_rates[i][good_ind]
            new_results_rates.append(results_rates[i][good_ind])
        results_rates = new_results_rates
    results_rates = np.array(results_rates)
    median = np.median(results_rates, axis = 0)
    
    f, ax = plot.subplots(1, 1)
    width = 0.5
    n = len(mean)
    ind = np.array(range(n))
    ax.bar(ind + width, mean, width, color = 'b', alpha = 0.45, zorder = 5)
    ax.errorbar(ind + 3.0 * width / 2.0, mean, std, 0, capsize = 4, ls = 'none', color = 'black', elinewidth = 2, zorder = 7)
    for i in range(n_results) :
        if i == highlight :
            ax.scatter(ind + width + width / 2.0, results_rates[i], color = 'r', zorder = 15)
#        else :
#            ax.scatter(ind + width + width / 2.0, results_rates[i], color = 'k', zorder = 10)
#    if highlight < 0 :
#        ax.scatter(ind + width + width / 2.0, median, color = 'r', zorder = 15)
    ax.plot(ind + width + width / 2.0, values, color = 'orange', marker = 'x', linewidth = 1.5)
    if additional is not None :
        ax.plot(ind + width + width / 2.0, additional, color = 'red', marker = 'x', linewidth = 1.5)
    ax.set_xlim(0, n +  width)
    ax.set_ylim(-10.7, 0)
    ax.xaxis.set_ticks_position('top')
    ax.set_xticks(ind + width + width / 2.0)
    ax.set_xticklabels(codons, rotation = 90, va = 'bottom')
    xax = ax.get_xaxis()
    xax.set_tick_params(pad = 4)
    ax.set_ylabel('Elongation rates, $\log_2$', fontsize = 16, labelpad = -8)
    #ax.set_xlabel('Codons', fontsize = 16)
    f.tight_layout()
    f.subplots_adjust(top = 0.9)

    pvalues = []
    for i in range(n) :
        codon = codons[i]
        t, p = scipy.stats.ttest_1samp(results_rates[:, i], values[i])
        if p < threshold :
            ax.get_xticklabels()[i].set_color('green')
        elif std[i] < threshold_std :
            ax.get_xticklabels()[i].set_color('blue')
    half_n = int(math.ceil(n / 2.0))
    for i in range(half_n) :
        t, p = scipy.stats.ttest_1samp(results_rates[:, i], values[i])
        color = 'black'
        if p < threshold :
            color = 'green'
        elif std[i] < threshold_std :
            color = 'blue'
        if codons[i] == color_codon :
            color = 'red'
        print '\\textcolor{%s}{%s} & $%.3f$ & $%.3f$ & $%.3f$ & $%.3f$ & $%.2f$' % (color, codons[i], values[i], mean[i], std[i], median[i], p),
        j = half_n + i
        if j < n :
            t, p = scipy.stats.ttest_1samp(results_rates[:, j], values[j])
            color = 'black'
            if p < threshold :
                color = 'green'
            elif std[j] < threshold_std :
                color = 'blue'
            if codons[j] == color_codon :
                color = 'red'
            print '& & \\textcolor{%s}{%s} & $%.3f$ & $%.3f$ & $%.3f$ & $%.3f$ & $%.2f$' % (color, codons[j], values[j], mean[j], std[j], median[j], p) 
        print ' \\tabularnewline'
        #print '%s : obs = %g, expected = %g | p-value %.2f' % (codon, mean[i], values[i], p)
    
    if color_codon is not None :
        color_codon = np.where(codons == color_codon)[0]
        ax.get_xticklabels()[color_codon].set_color('red')
    pylab.show()

def get_compute_time(genes, runtime, scale, number_of_solutions =  1) :
    times = []
    for gene in genes :
        for i in range(number_of_solutions) :
            times.append(runtime[gene])
    times = sorted(times)
    node_time = np.zeros((scale,))
    i = 0
    n = len(times)
    while i < n :
        j = 0
        while j < scale and i < n :
            node_time[j] += times[i]
            i += 1
            j += 1
    return np.max(node_time)

def plot_runtime_scaling(times_approximation, times_simulation, folds, scales = None, number_of_solutions = 1, xmin = 1, xmax = 400, ymax = 30) :
    app = {}
    sim = {}
    for i in range(len(times_approximation[0])) :
        app[times_approximation[0][i]] = times_approximation[1][i]
    for i in range(len(times_simulation[0])) :
        sim[times_simulation[0][i]] = times_simulation[1][i]
    runtime = {}
    genes = []
    for fold in folds :
        genes += fold
    for gene in genes :
         runtime[gene] = app[gene] + sim[gene]
    n_folds = len(folds)
    times = []
    for i in range(n_folds) :
        times.append([])
    if scales is None :
        scales = [1] + range(10, 1000, 50)
    for i in range(n_folds) :
        for scale in scales :
            times[i].append(get_compute_time(folds[i], runtime, scale, number_of_solutions) / 60.0)
    f, ax = plot.subplots(1, 1)
    for i in range(n_folds) :
        ax.plot(scales, times[i], label = 'Fold %d' % (i + 1))
    ax.set_xlabel('Number of engines', fontsize = 16)
    ax.set_ylabel('Runtime per fold, min', fontsize = 16)
    ax.legend(loc = 'upper right')
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(0, ymax)
    plot.show()

def count_codon_presence(forest, genes, color = None) :
    codon_count_genes = np.zeros((64,))
    codon_count_segments = np.zeros((64,))
    codons = lib.tools.codon2int.keys()
    n_genes = len(genes)
    n_segments = 0
    for gene in genes :
        seq = lib.tree.get_codon_sequence(forest[gene])
        n_segments += len(forest[gene].tree)
        for codon in codons :
            cmp = seq == codon
            if np.any(cmp) :
                codon_count_genes[lib.tools.codon2int[codon]] += 1
                tree = forest[gene]
                for node in tree.tree :
                    left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                    if np.any(cmp[left:right + 1]) :
                        codon_count_segments[lib.tools.codon2int[codon]] += 1
    f, ax = plot.subplots(2, 1)
    width = 0.35
    ind = np.array(range(64))
    codon_count_genes = codon_count_genes / float(n_genes)
    codon_count_segments = codon_count_segments / float(n_segments)
    ax[0].bar(ind + width, codon_count_genes, width, color = 'b', alpha = 0.45)
    ax[1].bar(ind + width, codon_count_segments, width, color = 'b', alpha = 0.45)
    ax[0].set_xlim(0, 64 + 2 * width)
    ax[1].set_xlim(0, 64 + 2 * width)
    ax[0].set_xticks(ind + width + width / 2.0)
    ax[1].set_xticks(ind + width + width / 2.0)
    codons_permute = [None] * 64
    for codon in codons :
        codons_permute[lib.tools.codon2int[codon]] = codon
    ax[0].set_xticklabels(codons_permute, rotation = 90)
    ax[1].set_xticklabels(codons_permute, rotation = 90)
    if color is not None :
        color = np.where(np.array(codons_permute) == color)[0]
        ax[0].get_xticklabels()[color].set_color('red')
        ax[1].get_xticklabels()[color].set_color('red')
    ax[0].set_ylabel('Fraction of genes', fontsize = 16)
    ax[1].set_ylabel('Fraction of segments', fontsize = 16)
    ax[1].set_xlabel('Codons', fontsize = 16)
    f.set_size_inches(14, 5.0, forward = True)
    f.tight_layout()
    pylab.show()

def simulate_scaled(gene, srand = 101317) :
    rates = lib.tools.get_RFM_rates()
    rate_vector = np.zeros((64,))
    for codon in rates.keys() :
        rate_vector[lib.tools.codon2int[codon]] = rates[codon]
    initiation = np.percentile(rate_vector, 0.1)
    rate_vector_x1 = np.zeros((64,))
    rate_vector_x025 = np.zeros((64,))
    rate_vector_x05 = np.zeros((64,))
    for i in range(64) :
        rate_vector_x1[i] = lib.simulator.RateFunction.logit(rate_vector[i])
        rate_vector_x025[i] = lib.simulator.RateFunction.logit(rate_vector[i] * 0.25)
        rate_vector_x05[i] = lib.simulator.RateFunction.logit(rate_vector[i] * 0.5)
    coef_x1, density_x1, J_x1 = lib.persistent.simulate_gene(gene, initiation, rate_vector_x1, srand)
    coef_x025, density_x025, J_x025 = lib.persistent.simulate_gene(gene, initiation * 0.25, rate_vector_x025, srand)
    coef_x05, density_x05, J_x05 = lib.persistent.simulate_gene(gene, initiation * 0.5, rate_vector_x05, srand)
    x = range(1, len(density_x1) + 1)
    f, ax = plot.subplots(3, 1, sharex = True)
    ax[0].plot(x, density_x05, linewidth = 1.5)
    ax[1].plot(x, density_x1, linewidth = 1.5)
    ax[2].plot(x, density_x025, linewidth = 1.5)
    ax[0].set_ylabel('$\\times\\,1.00$', fontsize = 16)
    ax[1].set_ylabel('$\\times\\,0.50$', fontsize = 16)
    ax[2].set_ylabel('$\\times\\,0.25$', fontsize = 16)
    ax[0].set_title('Simulation with scaled rates (%s)' % gene, fontsize = 16, fontweight = 'bold')
    ax[2].set_xlabel('Codon position', fontsize=16)
    ax[0].set_xlim(1, len(density_x1) + 1)
    ax[1].set_xlim(1, len(density_x1) + 1)
    ax[2].set_xlim(1, len(density_x1) + 1)
    f.text(0.07, 0.5, 'Ribosome occupancy', va = 'center', rotation = 'vertical', fontsize = 16)
    pylab.show()

def plot_time_distribution(results, n_bins = 40) :
    f, ax = plot.subplots(1, 1)
    n, bins, patches = ax.hist(results, bins = n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax.set_xlabel('Runtime, sec', fontsize = 16)
    #ax.set_xlabel('Fold speedup', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    plot.setp(ax.get_yticklabels(), visible = False)
    pylab.show()

def compare_densities(genes, segments, full, threshold = None) :
    n = len(genes)
    selected_genes = []
    results = []
    for i in xrange(n) :
        gene = genes[i]
        tree = lib.persistent.forest[gene]
        segment_J = segments[i].J
        full_J = full[i].J
        n_nodes = len(tree.tree)
        diff = abs(full_J - segment_J)
        for j in xrange(n_nodes) :
            node = tree.tree[j]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            
            if isinstance(segments[i].density, np.ndarray) :
                segment_density = np.mean(segments[i].density[left:right + 1])
            else :
                segment_density = segments[i].density[j] / (right - left + 1)
            
            if isinstance(full[i].density, np.ndarray) :
                full_density = np.mean(full[i].density[left:right + 1])
            else :
                full_density = full[i].density[j] / (right - left + 1)
            diff = max(diff, abs(segment_density - full_density))
            avg = (segment_density + full_density) * 0.5
        if threshold is None or diff >= threshold :
            selected_genes.append(gene)
            results.append(diff)
            print '%9s : %g (%.2f %%)' % (gene, diff, diff / avg * 100.0)
    return (selected_genes, results)

def compute_densities(genes, eps = None, srand = 101317) :
    results = []
    for gene in genes :
        simulator = lib.persistent.simulators[gene]
        simulator.initiation = 0.2
        results.append(simulator.simulate(eps = eps, srand = srand))
    return (genes, results)

def profile_simulation(genes, n_reps = 3, burn = 1000, eps = 1e-3, srand = 101317) :
    import yep
    yep.start('simulator.prof')
    times = []
    cnt, n = 1, len(genes)
    for gene in genes :
        gene_time = np.mean(timeit.repeat(stmt = 'lib.persistent.simulators["%s"].simulate(srand = %d, eps = %g, burn = %d)' % (gene, srand, eps, burn), setup = 'import lib.persistent', number = 1, repeat = n_reps))
        times.append(gene_time)
        print '[%d/%d] %9s : %.4f' % (cnt, n, gene, gene_time)
        cnt += 1
    yep.stop()
    return (genes, times)

def time_execution(genes, n_reps = 3, burn = 1000, eps = 1e-3, srand = 101317) :
    times = []
    cnt, n = 1, len(genes)
    for gene in genes :
        gene_time = np.mean(timeit.repeat(stmt = 'lib.persistent.simulators["%s"].simulate(srand = %d, eps = %g, burn = %d)' % (gene, srand, eps, burn), setup = 'import lib.persistent', number = 1, repeat = n_reps))
        times.append(gene_time)
        print '[%d/%d] %9s : %.4f' % (cnt, n, gene, gene_time)
        cnt += 1
    return (genes, times)

def time_execution_init(genes, scale = 2.76, n_reps = 3, srand = 101317) :
    import numpy as np
    import timeit
    import lib.tools
    import lib.simulator
    import json
    times = []
    rates = lib.tools.get_RFM_rates()
    rate_vector = np.zeros((64,))
    for codon in rates.keys() :
        rate_vector[lib.tools.codon2int[codon]] = lib.simulator.RateFunction.logit(rates[codon])
    if not isinstance(genes, list) :
        genes = [genes]
    cnt, n = 1, len(genes)
    for gene in genes :
        gene_time = np.mean(timeit.repeat(stmt = "lib.approximation.approximate_initiation('%s', rates, %.15f, srand = %d)" % (gene, scale, srand), setup = "import lib.approximation; import json; import numpy as np; rates = np.array(json.loads('%s'));" % json.dumps(rate_vector.tolist()), number = 1, repeat = n_reps))
        times.append(gene_time)
        print '[%d/%d] %9s : %.4f' % (cnt, n, gene, gene_time)
        cnt += 1
    return (genes, times)

def plot_tree_size_histogram(forest, genes, max_value = 30, n_bins = 35) :
    meas = []
    for gene in genes :
        meas.append(len(forest[gene].tree))
    meas = sorted(meas)
    meas = np.array(meas)
    edges = range(max_value + 1)
    counts = []
    for i in range(max_value - 1) :
        counts.append(np.sum(meas == i + 1))
    counts.append(np.sum(meas >= max_value))
    f, ax = plot.subplots(1, 1)
    edges = np.array(edges)
    counts = np.array(counts)
    x = (edges[1:] + edges[:-1]) / 2.0
    n, bins, patches = ax.hist(x, bins = edges, weights = counts, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    
    n = meas.shape[0]
    per_bin = n / float(n_bins)
    int_pos_prev = 0
    pos = 0
    while pos < n :
        pos = min(per_bin + pos, n)
        int_pos = int(pos)
        if int_pos < n :
            val = meas[int_pos]
        else :
            val = meas[n - 1] + 1
        prev_val = meas[int_pos_prev]
        indices = (meas >= prev_val) * (meas < val)
        bin_data = meas[indices]
        if len(bin_data) > 0 :
            print '[%4d, %4d) : %d genes' % (prev_val, val, len(bin_data))
            if val <= max_value :
                ax.plot([val - 1, val - 1], [0, 1], 'k--')
        int_pos_prev = int_pos
    
    ax.set_ylim(0, 0.26)
    ax.set_xlabel('# of segments', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    pylab.show()
    return meas

def evaluate_approximation(params, x) :
    n = len(x)
    res = np.zeros((n, ))
    a, b, c, d, e = tuple(params)
    for i in range(n) :
        if x[i] > e :
            res[i] = c * x[i] + d
        else :
            res[i] = a  * x[i] / (b + x[i])
    return res

def plot_approximation_schematic_example(app, max_init = 0.0024, n_steps = 200, y_max = 0.1) :
    left, right, mid = app[0]
    guess_left, guess_right, density, params = app[1]
    left_points, right_points, left_values, right_values = app[2]

    init = np.linspace(1e-6, max_init, n_steps)
    f, ax = plot.subplots(2, 1)
    f_minus, f_plus = [], []
    for value in init :
        f_minus.append(params[0] * value / (params[1] + value))
        f_plus.append(params[2] * value + params[3])
    f_plus = np.array(f_plus)
    f_minus = np.array(f_minus)
    ax[1].plot(init[init <= left], f_minus[init <= left], 'g-', linewidth = 1.5)
    ax[1].plot(init[np.logical_and(init <= right, init > left)], f_minus[np.logical_and(init <= right, init > left)], 'g--', linewidth = 1.5)
   
    ax[1].plot(init[np.logical_and(init >= left, init < right)], f_plus[np.logical_and(init >= left, init < right)], 'b--', linewidth = 1.5)
    ax[1].plot(init[init >= right], f_plus[init >= right], 'b-', linewidth = 1.5)

    ax[1].plot([left, left], [0, 1], 'k--')
    ax[1].plot([right, right], [0, 1], 'k--')
    
    pos = params[0] * mid / (params[1] + mid)
    ax[1].plot([mid, mid], [0, pos], 'k--')
    pos = params[2] * mid  + params[3]
    ax[1].plot([mid, mid], [pos, 1.0], 'k--')
    
    ax[1].plot([left, right], [density, density], 'r--', linewidth = 1.5)
    #ax[1].annotate('$k_0 = l$', xy = (left, y_max / 10.0), xytext = (-10, 0), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'vertical', fontsize = 16)
    #ax[1].annotate('$k_0 = r$', xy = (right, y_max / 10.0), xytext = (-10, 0), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'vertical', fontsize = 16)
    #ax[1].annotate('$k_0 = m$', xy = (mid, y_max / 10.0), xytext = (-10, 0), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'vertical', fontsize = 16)
    ax[1].annotate('$A_s(m)$', xy = (left, density), xytext = (-30, 0), textcoords = 'offset points', ha = 'center', va = 'center', rotation = 'horizontal', fontsize = 16)

    pos = params[0] * (left / 2.0) / (params[1] + left / 2.0)
    ax[1].annotate('$f^-_s(k_0)$', xy = (left / 2.0, pos), xytext = (-15, 15), textcoords = 'offset points', ha = 'center', va = 'center', rotation = 10, fontsize = 16)
    x = (max_init + right) / 2.0
    pos = params[2] * x + params[3]
    ax[1].annotate('$f^+_s(k_0)$', xy = (x, pos), xytext = (10, 5), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'horizontal', fontsize = 16)

    pos = params[0] * mid / (params[1] + mid)
    ax[1].annotate('', xy = (mid, density), xycoords = 'data', xytext = (mid, pos), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
    pos = params[2] * mid + params[3]
    ax[1].annotate('', xy = (mid, density), xycoords = 'data', xytext = (mid, pos), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
    
    ax[1].scatter(left_points, left_values, c = 'green', alpha = 1.0, s = 50)
    ax[1].scatter(right_points, right_values, c = 'blue', alpha = 1.0, s = 50)
    ax[1].scatter([mid], [density], c = 'red', alpha = 1.0, s = 50)

    ax[1].set_ylim(0, y_max)
    ax[1].set_xlim(0, max_init)
    ax[1].tick_params(axis = 'both', which = 'major', labelsize = 16)
    ax[1].tick_params(axis = 'both', which = 'minor', labelsize = 16)
    ax[1].set_xlabel('Initiation rate, $k_0$', fontsize = 16)
    ax[1].tick_params(axis = 'y', which = 'both', left = 'off', right = 'off', labelleft = 'off')
    ax[1].set_xticks([left, mid, right])
    ax[1].set_xticklabels(['$l$', '$m$', '$r$'])
    
    smooth = np.array([ 1.0 * 0.03238989,  0.00102625 * 0.4,  0.00041602,  0.0515799,  0.01390564])
    abrupt = np.array([ 0.10130321,  0.00144485 * 0.8,  0.00058738,  0.04684752,  0.0009908])
    jump = np.array([  2.27403866,   2.88557514e-02,   2.70857665e-03, 8.55903307e-02,   5.18817107e-04])

    max_init = 0.002
    init = np.linspace(1e-6, max_init, n_steps)
    smooth_values = evaluate_approximation(smooth, init)
    abrupt_values = evaluate_approximation(abrupt, init)
    jump_values = evaluate_approximation(jump, init)
    
    params = { 'legend.fontsize' : 14, 'legend.linewidth' : 1.5 }
    plot.rcParams.update(params)
    ax[0].plot(init, jump_values, label = 'abrupt, jump', lw = 1.5)
    ax[0].plot(init, abrupt_values, label = 'abrupt', lw = 1.5)
    ax[0].plot(init, smooth_values, label = 'smooth', lw = 1.5)
    ax[0].tick_params(axis = 'x', which = 'both', bottom = 'off', top = 'off', labelbottom = 'off')
    ax[0].tick_params(axis = 'y', which = 'both', left = 'off', right = 'off', labelleft = 'off')
    ax[0].legend(loc = 'center right')
    
    #ax[0].set_xlabel('Initiation rate, $x$', fontsize = 16)
    #ax[0].set_ylabel('Avg. number of ribosomes, $A(x)$', fontsize = 16)
   
    ymax = ax[0].set_ylim()
    ax[0].annotate('(i)', xy = (0, ymax[1]), xytext = (10, -10), textcoords = 'offset points', ha = 'left', va = 'top', rotation = 'horizontal', fontsize = 16, fontweight = 'bold')
    ymax = ax[1].set_ylim()
    ax[1].annotate('(ii)', xy = (0, ymax[1]), xytext = (10, -10), textcoords = 'offset points', ha = 'left', va = 'top', rotation = 'horizontal', fontsize = 16, fontweight = 'bold')
    
    ax = f.add_axes( [0., 0., 1, 1] )
    ax.set_axis_off()
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.text(0.05, 0.5, 'Avg. number of ribosomes, $A_s(k_0)$', rotation = 'vertical', horizontalalignment = 'center', verticalalignment = 'center', fontsize = 16)

    f.tight_layout()
    f.subplots_adjust(left = 0.08, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    plot.show()

def plot_segment_densities(pairs = [('YGL103W', 5), ('YPR017C', 0), ('YGL256W', 0)], n_steps = 100, max_init = 0.005, srand = 101317) :
    rates = 1.0 / lib.zhang.RFM_times
    n_rates = len(rates)
    for i in range(n_rates) :
        rates[i] = lib.simulator.RateFunction.logit(rates[i])
    
    init = np.linspace(1e-6, max_init, n_steps)
    init_app = np.linspace(1e-6, max_init, n_steps * 10)

    f, ax = plot.subplots(1, 1)
    for gene, segment in pairs :
        approximation = lib.approximation.approximate_density(gene, rates, srand = srand)
        tree = lib.persistent.forest[gene]
        n_segments = len(tree.tree)
        data = np.zeros((n_steps,))
        for i in range(n_steps) :
            coef, density, J, Q = lib.persistent.simulate_gene(gene, init[i], rates, srand)
            node = tree.tree[segment]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            data[i] = np.mean(density[left:right + 1])
    
        node = tree.tree[segment]
        left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
        text = '%s : [%d, %d]' % (gene, left + 1, right + 1)
        ax.plot(init, data, label = text, lw = 1.5) 
        text = '%s : [%d, %d], approx' % (gene, left + 1, right + 1)
        values = evaluate_approximation(approximation[segment], init_app)
        ax.plot(init_app, values, '--', label = text, lw = 1.5)
    
    ax.legend(loc = 'lower right')
    ax.set_xlim(np.min(init), np.max(init))
    ax.set_xlabel('Initiation rate, $k_0$', fontsize = 16)
    ax.set_ylabel('Avg. number of ribosomes, $A_s(k_0)$', fontsize = 16)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)
    plot.show()

def plot_initiation_fitness_dependency(genes = ['YLR197W'], scales = [[3.0, 3.5, 4.0]], n_steps = 200, ymin = -100, srand = 101317) :
    rates = 1.0 / lib.zhang.RFM_times
    n_rates = len(rates)
    for i in range(n_rates) :
        rates[i] = lib.simulator.RateFunction.logit(rates[i])
    init = np.linspace(1e-6, 0.004, n_steps)
    n_genes = len(genes)
    fits, fits_approx = [], []
    for i in range(n_genes) :
        gene = genes[i]
        gene_scales = scales[i]
        approximation = lib.approximation.approximate_density(gene, rates, srand)
        print approximation
        n_scales = len(gene_scales)
        gene_fits = []
        gene_approx = []
        for j in range(n_scales) :
            gene_fits.append(np.zeros(n_steps))
            gene_approx.append(np.zeros(n_steps))
        for j in range(n_steps) :
            coef = lib.persistent.evaluate_fit(gene, init[j], rates, srand)
            for k in range(n_scales) :
                scale = gene_scales[k]
                likelihood = coef[0] * scale ** 2 + coef[1] * scale + coef[2]
                gene_fits[k][j] = likelihood
            coef = lib.approximation._evaluate_approximation(gene, approximation, init[j])
            for k in range(n_scales) :
                scale = gene_scales[k]
                likelihood = coef[0] * scale ** 2 + coef[1] * scale + coef[2]
                gene_approx[k][j] = likelihood
        fits.append(gene_fits)
        fits_approx.append(gene_approx)
    params = { 'legend.fontsize' : 16, 'legend.linewidth' : 1.5 }
    plot.rcParams.update(params)
    f, ax = plot.subplots(1, 1)
    for i in range(n_genes) :
        gene_scales = scales[i]
        n_scales = len(gene_scales)
        for j in range(n_scales) :
            text = '$\\ln \\tilde{C}=%.2f$' % (scales[i][j])
            ax.plot(init, fits[i][j], '-', label = text, linewidth = 1.5)
            text = '$\\ln \\tilde{C}=%.2f$, approx' % (scales[i][j])
            ax.plot(init, fits_approx[i][j], '--', label = text, linewidth = 1.5)

    ax.legend(loc = 'lower right')
    ax.set_ylim(ymin, -10)
    ax.set_xlabel('Initiation rate, $k_0$', fontsize = 16)
    ax.set_ylabel('Objective, $\\psi$', fontsize = 16)
    ax.set_title('Gene: %s' % " ".join(genes), fontsize = 16, fontweight = 'bold')
    plot.show()

def plot_correlation_histograms(corr_mn, corr_pn, n_bins = 14) :
    f, ax = plot.subplots(1, 2, sharey = True)
    n, bins, patches = ax[0].hist(corr_mn, n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    median = np.median(corr_mn)
    ax[0].plot([median, median], [0, 10], 'r--', lw = 1.5)
    ax[0].text(median - 0.13, 0.8, 'Median $\\tilde{r}=%.2f$' % median, va = 'center', ha = 'left', rotation = 'vertical', fontsize = 16)
    ax[0].set_xlim(-0.5, 1)
    ax[0].set_ylim(0, 2.1)
    ax[0].set_xlabel('Pearson $r$', fontsize = 16)
    ax[0].set_title('Mean normalization', fontsize = 16, fontweight = 'bold')

    n, bins, patches = ax[1].hist(corr_pn, n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    median = np.median(corr_pn)
    ax[1].plot([median, median], [0, 10], 'r--', lw = 1.5)
    ax[1].text(median - 0.13, 0.8, 'Median $\\tilde{r}=%.2f$' % median, va = 'center', ha = 'left', rotation = 'vertical', fontsize = 16)
    ax[1].set_xlim(-0.5, 1)
    ax[1].set_ylim(0, 2.1)
    ax[1].axes.get_yaxis().set_visible(False)
    ax[1].set_xlabel('Pearson $r$', fontsize = 16)
    ax[1].set_title('Profile normalization', fontsize = 16, fontweight = 'bold')
    ax[0].set_ylabel('Fraction of genes', fontsize = 16)
    plot.setp(ax[0].get_yticklabels(), visible = False)

    f.subplots_adjust(left = None, bottom = 0.13, right = None, top = None, wspace = 0.05, hspace = None)
    pylab.show()

def plot_segment_length_distribution(forest, min_value = 20, max_value = 1024, histogram_bins = 25, std_bins = 5) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 1)

    lengths, M = [], []
    for gene in genes :
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            length = right - left + 1
            lengths.append(length)
            M.append(ratio_m)

    pre_sort = np.array([lengths, M]).T
    pre_sort = np.array(sorted(pre_sort, key = lambda x : x[0])).T
    lengths = pre_sort[0]
    M = pre_sort[1]

    edges, counts = [], []
    step = float(max_value - min_value) / histogram_bins
    current_edge = min_value
    for i in range(histogram_bins) :
        next_edge = current_edge + step
        if i == histogram_bins - 1 :
            next_edge = np.max(lengths)
        lower = lengths < next_edge
        greater = lengths >= current_edge
        lower = lower.astype(np.int)
        greater = greater.astype(np.int)
        cnt = np.dot(lower, greater)
        counts.append(cnt)
        edges.append(current_edge)
        current_edge = next_edge
    edges.append(max_value)
    
    counts = np.array(counts)
    edges = np.array(edges)
    counts = counts / float(np.sum(counts))
    
    x = (edges[1:] + edges[:-1]) / 2.0
    n, bins, patches = ax.hist(x, bins = edges, weights = counts, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)

    ax.set_xscale('log')
    ax.set_xticks([32, 64, 128, 256, 512, 1000])
    labels = ['$2^5$', '$2^6$', '$2^7$', '$2^8$', '$2^9$', '$\\geq2^{10}$']
    ax.set_xticklabels(labels)

    n = lengths.shape[0]
    per_bin = n / float(std_bins)
    int_pos_prev = 0
    pos = 0
    while pos < n :
        pos = min(per_bin + pos, n)
        int_pos = int(pos)
        if int_pos < n :
            val = lengths[int_pos]
        else :
            val = lengths[n - 1] + 1
        prev_val = lengths[int_pos_prev]
        indices = (lengths >= prev_val) * (lengths < val)
        bin_data = M[indices]
        print bin_data.shape

        sigma = np.std(bin_data) / math.sqrt(2)
        print '[%4d, %4d) : %.3f +/- %.3f' % (prev_val, val, np.mean(bin_data), sigma)
        mid = np.median(lengths[indices])
        ax.annotate('$\sigma = %.2f$' % sigma, xy = (mid, 0.20), xytext = (8, 0), textcoords = 'offset points', ha = 'right', va = 'bottom', rotation = 'vertical', fontsize = 30)
        
        int_pos_prev = int_pos
        ax.plot([val, val], [0, 1], 'k--')

    ax.set_ylim(0, 0.30)
    ax.set_xlim(min_value, max_value)
    ax.set_xlabel('Segment length', fontsize = 30)
    ax.set_ylabel('Fraction of segments', fontsize = 30)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 30)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 30)
    f.subplots_adjust(left = None, bottom = 0.13, right = None, top = None, wspace = 0.05, hspace = None)
    
    pylab.show()

def plot_ratio_error_historgram(forest) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 1)
    
    M, M_all = [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                M.append(ratio_m)
            M_all.append(ratio_m)
    M, M_all = np.array(M), np.array(M_all)
    mean, sigma = np.mean(M), np.std(M)
    mean_all, sigma_all = np.mean(M_all), np.std(M_all)
    
    n, bins, patches = ax.hist(M, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    n, bins, patches = ax.hist(M_all, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.45)

    x = np.linspace(-3, 3, 100)
    ax.plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'Full-CDS, $\sigma=%.2f$' % sigma)
    ax.plot(x, mlab.normpdf(x, mean_all, sigma_all), 'g', linewidth = 2, alpha = 0.65, label = 'Segments, $\sigma=%.2f$' % sigma_all)
    ax.set_xlim(-1.5, 1.5)
    ax.set_ylim(0, 2.2)
    ax.axes.get_yaxis().set_visible(False)
    ax.set_xlabel('$\\log_2$ ratio', fontsize = 16)
    #ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
    #ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)
    #ax.set_xticks([-1.5, 0.0, 1.5])
    #params = {'legend.fontsize': 16}
    #plot.rcParams.update(params)
    ax.legend()
    
    pylab.show()
 
def plot_error_historgrams(forest) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 2, sharey = True)
    
    M, M_all = [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                M.append(mrna_m)
            M_all.append(mrna_m)
    M, M_all = np.array(M), np.array(M_all)
    mean, sigma = np.mean(M), np.std(M)
    mean_all, sigma_all = np.mean(M_all), np.std(M_all)
    
    n, bins, patches = ax[0].hist(M, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    n, bins, patches = ax[0].hist(M_all, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.45)

    x = np.linspace(-3, 3, 100)
    ax[0].plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'Full-CDS, $\sigma=%.2f$' % sigma)
    ax[0].plot(x, mlab.normpdf(x, mean_all, sigma_all), 'g', linewidth = 2, alpha = 0.65, label = 'Segments, $\sigma=%.2f$' % sigma_all)
    ax[0].set_xlim(-1.2, 1.2)
    ax[0].set_ylim(0, 2.2)
    ax[0].set_title('mRNA density', fontsize = 16, fontweight = 'bold')
    ax[0].axes.get_yaxis().set_visible(False)
    ax[0].set_xlabel('$\\log_2$ ratio', fontsize = 16)
    ax[0].legend()
 
    print 'mRNA full-gene: %.5f +/- %.5f' % (mean, sigma)
    print 'mRNA segments: %.5f +/- %.5f' % (mean_all, sigma_all)  

    M, M_all = [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                M.append(rib_m)
            M_all.append(rib_m)
    M, M_all = np.array(M), np.array(M_all)
    mean, sigma = np.mean(M), np.std(M)
    mean_all, sigma_all = np.mean(M_all), np.std(M_all)
    
    n, bins, patches = ax[1].hist(M, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    n, bins, patches = ax[1].hist(M_all, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.45)

    x = np.linspace(-3, 3, 100)
    ax[1].plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'Full-CDS, $\sigma=%.2f$' % sigma)
    ax[1].plot(x, mlab.normpdf(x, mean_all, sigma_all), 'g', linewidth = 2, alpha = 0.65, label = 'Segments, $\sigma=%.2f$' % sigma_all)
    ax[1].set_xlim(-1.2, 1.2)
    ax[1].set_ylim(0, 2.2)
    ax[1].set_title('Ribosome density', fontsize = 16, fontweight = 'bold')
    ax[1].axes.get_yaxis().set_visible(False)
    ax[1].set_xlabel('$\\log_2$ ratio', fontsize = 16)
    ax[1].legend()

    print 'Rib full-gene: %.5f +/- %.5f' % (mean, sigma)
    print 'Rib segments: %.5f +/- %.5f' % (mean_all, sigma_all)  
    
    pylab.show()

def regress_ma_lowess(m, a, color, frac = 0.33, it = 3, sorted = False) :
    a = np.atleast_1d(a) 
    m = np.atleast_1d(m) 
    color = np.atleast_1d(color)
    a = a[color == 0]
    m = m[color == 0]

    lowess = sm.nonparametric.lowess
    z = lowess(m, a, frac = frac, it = it, return_sorted = sorted)
    return z
    
def ma_plot_forest_original(forest, mrna_threshold = 128, rib_threshold = 64, output = None) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 3, sharey = True)
    cmap = pylab.cm.get_cmap('RdYlBu_r')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                color = 1 if mrna_cnt < mrna_threshold else 0
                M.append(mrna_m)
                A.append(mrna_a)
                colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    ax[0].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0].transAxes, linewidth = 1, color = 'k'))
    ax[0].set_xlim(-30, -13)
    ax[0].set_ylim(-3, 3)
    ax[0].set_title('mRNA density', fontsize = 16, fontweight = 'bold')
    ax[0].set_ylabel('M, $\\log_2$', fontsize = 16)

    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                color = 1 if rib_cnt < rib_threshold else 0
                M.append(rib_m)
                A.append(rib_a)
                colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    ax[1].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[1].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1].transAxes, linewidth = 1, color = 'k'))
    ax[1].set_xlim(-30, -13)
    ax[1].set_ylim(-3, 3)
    ax[1].set_title('Ribosome density', fontsize = 16, fontweight = 'bold')
    ax[1].set_xlabel('A, $\\log_2$', fontsize = 16)

    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                color = 1 if mrna_cnt < mrna_threshold or rib_cnt < rib_threshold else 0
                M.append(ratio_m)
                A.append(ratio_a)
                colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    ax[2].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[2].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[2].transAxes, linewidth = 1, color = 'k'))
    ax[2].set_xlim(-7, 4)
    ax[2].set_ylim(-3, 3)
    ax[2].set_title('Density ratio', fontsize = 16, fontweight = 'bold')

    f.tight_layout()
    f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)

    if output is not None :
        plot.savefig(output)

    pylab.show()

def ma_plot_ratio_bias_correction(forest) :
    genes = forest.keys()
    cmap = pylab.cm.get_cmap('RdYlBu_r')
    f, ax = plot.subplots(1, 1)
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 0
            M.append(ratio_m)
            A.append(ratio_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax.scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax.add_line(Line2D([0, 1], [0.5, 0.5], transform = ax.transAxes, linewidth = 1, color = 'k'))
    ax.plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax.set_xlim(-5, 5)
    ax.set_ylim(-2, 2)
    ax.set_xlabel('A, $\\log_2$', fontsize = 30)
    ax.set_ylabel('M, $\\log_2$', fontsize = 30)
   
    plot.tick_params(axis = 'both', which = 'major', labelsize = 30)
    plot.tick_params(axis = 'both', which = 'minor', labelsize = 30)
    f.tight_layout()
    f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    pylab.show()

def ma_plot_bias_correction(forest, forest_corrected, mrna_threshold = 128, rib_threshold = 64) :
    genes = forest.keys()
    f, ax = plot.subplots(2, 3, sharey = 'row')
    cmap = pylab.cm.get_cmap('RdYlBu_r')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if mrna_cnt < mrna_threshold else 0
            M.append(mrna_m)
            A.append(mrna_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[0][0].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0][0].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0][0].transAxes, linewidth = 1, color = 'k'))
    ax[0][0].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[0][0].set_title('mRNA density', fontsize = 16, fontweight = 'bold')
    ax[0][0].set_ylabel('(i)', fontsize = 16, fontweight = 'bold', rotation = 'horizontal')
    ax[0][0].set_ylim(-2, 2)
    ax[0][0].set_xlim(-27, -13)
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold else 0
            M.append(rib_m)
            A.append(rib_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[0][1].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0][1].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0][1].transAxes, linewidth = 1, color = 'k'))
    ax[0][1].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[0][1].set_ylim(-2, 2)
    ax[0][1].set_xlim(-27, -13)
    ax[0][1].set_title('Ribosome density', fontsize = 16, fontweight = 'bold')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold or mrna_cnt < mrna_threshold else 0
            M.append(ratio_m)
            A.append(ratio_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[0][2].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0][2].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0][2].transAxes, linewidth = 1, color = 'k'))
    ax[0][2].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[0][2].set_ylim(-2, 2)
    ax[0][2].set_xlim(-5, 5)
    ax[0][2].set_title('Density ratio', fontsize = 16, fontweight = 'bold')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest_corrected[gene].length_codons
        for node in forest_corrected[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if mrna_cnt < mrna_threshold else 0
            M.append(mrna_m)
            A.append(mrna_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[1][0].scatter(A[colors == 0], M[colors == 0], c = colors[colors == 0], cmap = cmap, alpha = 0.2, s = 30)
    ax[1][0].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1][0].transAxes, linewidth = 1, color = 'k'))
    ax[1][0].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[1][0].set_ylim(-2, 2)
    ax[1][0].set_xlim(-27, -13)
    ax[1][0].set_ylabel('(ii)', fontsize = 16, fontweight = 'bold', rotation = 'horizontal')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest_corrected[gene].length_codons
        for node in forest_corrected[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold else 0
            M.append(rib_m)
            A.append(rib_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[1][1].scatter(A[colors == 0], M[colors == 0], c = colors[colors == 0], cmap = cmap, alpha = 0.2, s = 30)
    ax[1][1].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1][1].transAxes, linewidth = 1, color = 'k'))
    ax[1][1].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[1][1].set_xlabel('A, $\\log_2$', fontsize = 16)
    ax[1][1].set_ylim(-2, 2)
    ax[1][1].set_xlim(-27, -13)

    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest_corrected[gene].length_codons
        for node in forest_corrected[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold or mrna_cnt < mrna_threshold else 0
            M.append(ratio_m)
            A.append(ratio_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[1][2].scatter(A[colors == 0], M[colors == 0], c = colors[colors == 0], cmap = cmap, alpha = 0.2, s = 30)
    ax[1][2].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1][2].transAxes, linewidth = 1, color = 'k'))
    ax[1][2].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[1][2].set_ylim(-2, 2)
    ax[1][2].set_xlim(-5, 5)

    f.tight_layout()
    f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    
    f.text(0.07, 0.5, 'M, $\\log_2$', va = 'center', rotation = 'vertical', fontsize = 16)
    
    pylab.show()
